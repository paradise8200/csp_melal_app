import 'package:intl/intl.dart';

DateTime getNowDateTime() {
  return DateTime.now();
}

final formatter = new NumberFormat("#,###");
