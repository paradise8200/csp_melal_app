import 'package:flutter_language_app/pages/bas/home/home_page.dart';
import 'package:flutter_language_app/pages/bas/login_page/login_page.dart';
import 'package:flutter_language_app/pages/bas/splash_page/splash_page.dart';
import 'package:flutter_language_app/pages/bas/verify_page/verify_page.dart';
import 'package:flutter_language_app/pages/create_user/create_account_page.dart';
import 'package:flutter_language_app/pages/create_user/create_user_page.dart';
import 'package:flutter_language_app/pages/edit_profile_page/edit_profile_page.dart';
import 'package:flutter_language_app/pages/main_page.dart';
import 'package:stacked/stacked_annotations.dart';


@StackedApp(
  routes: [
    MaterialRoute(page: SplashPage , initial: false),
    MaterialRoute(page: LoginPage,initial: true),
    MaterialRoute(page: HomePage,initial: false),
    MaterialRoute(page: CreateUserPage),
    MaterialRoute(page: EditProfilePage),
    MaterialRoute(page: VerifyPage),
  ],
)
class AppSetup {
  /** Serves no purpose besides having an annotation attached to it */
}
