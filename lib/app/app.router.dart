// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedRouterGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../pages/bas/home/home_page.dart';
import '../pages/bas/login_page/login_page.dart';
import '../pages/bas/splash_page/splash_page.dart';
import '../pages/bas/verify_page/verify_page.dart';
import '../pages/create_user/create_user_page.dart';
import '../pages/edit_profile_page/edit_profile_page.dart';

class Routes {
  static const String splashPage = '/splash-page';
  static const String loginPage = '/';
  static const String homePage = '/home-page';
  static const String createUserPage = '/create-user-page';
  static const String editProfilePage = '/edit-profile-page';
  static const String verifyPage = '/verify-page';
  static const all = <String>{
    splashPage,
    loginPage,
    homePage,
    createUserPage,
    editProfilePage,
    verifyPage,
  };
}

class StackedRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.splashPage, page: SplashPage),
    RouteDef(Routes.loginPage, page: LoginPage),
    RouteDef(Routes.homePage, page: HomePage),
    RouteDef(Routes.createUserPage, page: CreateUserPage),
    RouteDef(Routes.editProfilePage, page: EditProfilePage),
    RouteDef(Routes.verifyPage, page: VerifyPage),
  ];
  @override
  Map<Type, StackedRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, StackedRouteFactory>{
    SplashPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SplashPage(),
        settings: data,
      );
    },
    LoginPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => LoginPage(),
        settings: data,
      );
    },
    HomePage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => HomePage(),
        settings: data,
      );
    },
    CreateUserPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => CreateUserPage(),
        settings: data,
      );
    },
    EditProfilePage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => EditProfilePage(),
        settings: data,
      );
    },
    VerifyPage: (data) {
      var args = data.getArgs<VerifyPageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => VerifyPage(args.phoneNumber),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// VerifyPage arguments holder class
class VerifyPageArguments {
  final String phoneNumber;
  VerifyPageArguments({required this.phoneNumber});
}
