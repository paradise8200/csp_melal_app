import 'package:flutter/material.dart';
import 'package:flutter_language_app/lessons/base_page.dart';
import 'package:flutter_language_app/models/cart_box.dart';
import 'package:flutter_language_app/models/category_model.dart';
import 'package:flutter_language_app/models/choice_pic_quiz_model.dart';
import 'package:flutter_language_app/models/matching_quiz_model.dart';
import 'package:flutter_language_app/pages/bas/home/home_page.dart';
import 'lessons/intro_lesson_page.dart';
import 'models/lesson_model.dart';
import 'models/MultiQuestion.dart';

List categoryList() {
  List<CategoryModel> categoryCard = [];
  categoryCard.add(CategoryModel("assets/bg_category_card_yellow.jpg",Color(0xfff8d56d),
      'دوره مـبتدی', 'assets/pic_rocket.png'));
  categoryCard.add(CategoryModel("assets/bg_category_card_blue.jpg",Color(0xff12c5fd),
      'دوره متوسـط', 'assets/pic_rocket.png'));
  categoryCard.add(CategoryModel("assets/bg_category_card_pink.jpg",Color(0xfffc56dc),
      'دوره پیشرفتـه', 'assets/pic_rocket.png'));
  return categoryCard;
}

List<MatchingQuizModel> matchingQuiz() {
  List<MatchingQuizModel> matchingQuizFakeData = [];
  matchingQuizFakeData.add(MatchingQuizModel(name: "Family",image: "https://s4.uupload.ir/files/family_juv2.jpg"));
  matchingQuizFakeData.add(MatchingQuizModel(name: "Happy",image: "https://s4.uupload.ir/files/happy_7lrl.jpg"));
  matchingQuizFakeData.add(MatchingQuizModel(name: "Mother",image: "https://s4.uupload.ir/files/mother_m86y.jpg"));
  matchingQuizFakeData.add(MatchingQuizModel(name: "Thanks",image: "https://s4.uupload.ir/files/thankes_ktlq.jpg"));
  matchingQuizFakeData.add(MatchingQuizModel(name: "Sister",image: "https://s4.uupload.ir/files/sister_t4ao.jpg"));
  return matchingQuizFakeData;
}

List<WordModel> wordModel() {
  List<WordModel> wordModelFake = [];
  wordModelFake.add(WordModel(wordAnswer: 'H'));
  wordModelFake.add(WordModel(wordAnswer: 'E'));
  wordModelFake.add(WordModel(wordAnswer: 'L'));
  wordModelFake.add(WordModel(wordAnswer: 'L'));
  wordModelFake.add(WordModel(wordAnswer: 'O'));
  return wordModelFake;
}

List<ChoicePicModel> choicePicQuiz() {
  List<ChoicePicModel> choicePicQuizFakeData = [];
  choicePicQuizFakeData.add(ChoicePicModel(name: "Family",image: "https://s4.uupload.ir/files/family_juv2.jpg"));
  choicePicQuizFakeData.add(ChoicePicModel(name: "Happy",image: "https://s4.uupload.ir/files/happy_7lrl.jpg"));
  choicePicQuizFakeData.add(ChoicePicModel(name: "Mother",image: "https://s4.uupload.ir/files/mother_m86y.jpg"));
  choicePicQuizFakeData.add(ChoicePicModel(name: "Thanks",image: "https://s4.uupload.ir/files/thankes_ktlq.jpg"));
  return choicePicQuizFakeData;
}



List lessonList(BuildContext context) {
  List<Lesson> lessonList = [];
  var theme = Theme.of(context);
  lessonList.add(Lesson("https://s16.picofile.com/file/8417349934/teacher.jpg",
      "آموزش گرامر", lessonType.video, true, theme.primaryColor, () {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => LessonDetailPage()));
  },false));
  lessonList.add(Lesson("https://s17.picofile.com/file/8417351634/Book3.jpg",
      "کلمات رو حفظ کن!", lessonType.words, true, Colors.indigoAccent, () {},false));
  lessonList.add(Lesson("https://s16.picofile.com/file/8417352300/quiz5.jpg",
      "وقتشه خودتو محک بزنی!", lessonType.Exercises,true, Colors.red, () {},false));

  lessonList.add(Lesson("https://s16.picofile.com/file/8417350600/Write.jpg",
      "بنویس تا یادت بمونه !", lessonType.writing, false, Colors.pink, () {},false));
  lessonList.add(Lesson("https://s17.picofile.com/file/8417350200/talk.jpg",
      "صحبت کن !", lessonType.speaking, false, theme.accentColor, () {
        Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) => HomePage()));
      },true));

  return lessonList;
}

List homeLessonList() {
  List<CartBoxModel> homeLessonList = [];
  homeLessonList.add(CartBoxModel(
      "https://s16.picofile.com/file/8417344384/Family.jpg",
      "خانواده و فامیل",
      "The Family",
      "طلایی"));
  homeLessonList.add(CartBoxModel(
      "https://s16.picofile.com/file/8417343834/Countries.jpg",
      "کشور ها و زبانها",
      "Country and Nationality",
      "طلایی"));
  homeLessonList.add(CartBoxModel(
      "https://s16.picofile.com/file/8417344242/Romance.jpg",
      "عشق و علاقه",
      "Lovely",
      "طلایی"));
  homeLessonList.add(CartBoxModel(
      "https://s16.picofile.com/file/8417344742/Weather.jpg",
      "فصل ها و اقلیم ها",
      "Seasons and climates",
      "طلایی"));

  homeLessonList.add(CartBoxModel(
      "https://s16.picofile.com/file/8417344250/travel.jpg",
      "سفر کردن",
      "Travel",
      "طلایی"));

  homeLessonList.add(CartBoxModel(
      "https://s16.picofile.com/file/8417348918/activities.jpg",
      "فعالیت ها",
      "Activities",
      "طلایی"));

  homeLessonList.add(CartBoxModel(
      "https://s16.picofile.com/file/8417346692/travel9.jpg",
      "تدارک سفر",
      "Travel preparation",
      "طلایی"));

  homeLessonList.add(CartBoxModel(
      "https://s16.picofile.com/file/8417344468/public.jpg",
      "حمل و نقل عمومی",
      "Public transportation",
      "طلایی"));

  return homeLessonList;
}

MultiQuestion multiQuestion() {
  return MultiQuestion([
    "Boy",
    "Girl",
    "Father",
    "Mother"
  ], [
    "https://s18.picofile.com/file/8432327818/boy.png",
    "https://s18.picofile.com/file/8432327850/girl.png",
    "https://s18.picofile.com/file/8432327884/man.png",
    "https://s18.picofile.com/file/8432327926/woman.png"
  ], "Girl","",false);
}

