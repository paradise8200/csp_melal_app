import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/pages/bas/home/home_page.dart';
import 'package:flutter_language_app/pages/bas/verify_page/verify_page.dart';
import 'package:flutter_language_app/pages/main_page.dart';
import 'package:flutter_language_app/theme/colors.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/progress_button.dart';
import 'package:lottie/lottie.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';




class NotificationPage extends StatefulWidget {


  @override
  State<StatefulWidget> createState() => NotificationPageState();
}

class NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: theme.backgroundColor,
        body: Stack(
          fit: StackFit.expand,
          children: [
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              child: Container(
                padding: EdgeInsets.all(standardSize(context)),
                width: fullWidth(context),
                height: fullHeight(context) / 2.2,
                child: FlareActor(
                  "assets/Notifications Dark (2).flr",
                  isPaused: false,
                  animation: "Notifications",
                ),
              ),
            ),
            Align(
              alignment: Alignment(0, 0.0),
              child: Text(
                "!در مسیر خود باشید",
                style: theme.textTheme.bodyText1!.copyWith(
                    color: theme.accentColor,
                    fontSize: mediumSize(context) / 1.2),
              ),
            ),
            Align(
              alignment: Alignment(0, 0.15),
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: Text(
                  "یادآوری های یادگیری را دریافت کنید تا ضربان خود را از دست ندهید!\nهمیشه می توانید آنها را در حالت تنظیم خاموش کنید.",
                  style: theme.textTheme.caption!.copyWith(
                      color: theme.accentColor, fontSize: smallSize(context)),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: standardSize(context)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {

                    },
                    borderRadius: BorderRadius.circular(mediumSize(context)),
                    splashColor: Colors.grey.shade200,
                    child: Directionality(
                      textDirection: TextDirection.rtl,
                      child: Container(
                        width: fullWidth(context) / 2.6,
                        height: fullHeight(context) / 13,
                        child: OutlineGradientButton(
                          radius: Radius.circular(mediumSize(context)),
                          child: Container(alignment: Alignment.center,child: Text("بعدا!",textAlign: TextAlign.center,style: TextStyle(
                              fontSize: fullWidth(context) > 600
                                  ? fullWidth(context) / 35
                                  : fullWidth(context) / 25,
                              color: Color(0xff3428ea),
                              fontWeight: FontWeight.w600),)),
                          gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            stops: [0.0, 1.0],
                            colors: [
                              Color(0xff3428ea),
                              Color(0xffa573ff),
                            ],
                          ),
                          strokeWidth: 2,
                        ),
                      ),
                    ),
                  ),
                  Directionality(
                    textDirection: TextDirection.rtl,
                    child: Container(
                      width: fullWidth(context) / 2.6,
                      height: fullHeight(context) / 13,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(mediumSize(context)),
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          stops: [0.0, 1.0],
                          colors: [
                            Color(0xff3428ea),
                            Color(0xffa573ff),
                          ],
                        ),
                      ),
                      child: ElevatedButton(
                        onPressed: () {

                        },
                        style: ElevatedButton.styleFrom(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context))),
                            primary: Colors.transparent,
                            padding: EdgeInsets.symmetric(
                                vertical: mediumSize(context))),
                        child: Text(
                          "یادآوری کن!",
                          style: TextStyle(
                              fontSize: fullWidth(context) > 600
                                  ? fullWidth(context) / 35
                                  : fullWidth(context) / 25,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
