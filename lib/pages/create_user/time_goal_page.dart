import 'package:flutter/material.dart';
import 'package:flutter_language_app/pages/bas/deposit_notifier.dart';
import 'package:flutter_language_app/pages/bas/verify_page/verify_page.dart';
import 'package:flutter_language_app/pages/main_page.dart';
import 'package:flutter_language_app/theme/colors.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/progress_button.dart';
import 'package:stacked/stacked.dart';
import 'level_english_page.dart';

class TimeGoalPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => TimeGoalPageState();
}

class TimeGoalPageState extends State<TimeGoalPage> {
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<DepositVM>.reactive(
    viewModelBuilder: () => DepositVM(),
    builder: (context, model, child) => Directionality(
        textDirection: TextDirection.ltr,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: theme.backgroundColor,
          body: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: largeSize(context)),
                      child: Text("!چـقـدر وقـت مـیذاری",
                          textAlign: TextAlign.center,
                          style: theme.textTheme.bodyText1!.copyWith(
                              color: theme.accentColor,
                              fontSize: mediumSize(context) / 1.2),),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: mediumSize(context)),
                      child: Text(
                        "مسیر موفقیت و مسیر شکست تقریبا شبیه هم هستند\n!ما به شما کمک خواهیم کرد تا مکان مناسب را برای شروع درست کنید",
                        style: theme.textTheme.caption!.copyWith(
                            color: theme.accentColor,
                            fontSize: smallSize(context)
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: timeCard(context, "1  تا  2  ساعت", "assets/clock.png",(){},model),
                      margin: EdgeInsets.only(top: standardSize(context)),
                    ),
                    timeCard(context, "2  تا  4  ساعت", "assets/clock.png",(){},model),
                    timeCard(context, "4  تا  6  ساعت", "assets/clock.png",(){},model),
                    timeCard(context, "مطمئن نیستم!", "assets/clock.png",(){},model),
                  ],
                ),
              ),
              // Positioned(
              //   left: 0,
              //   bottom: 0,
              //   right: 0,
              //   child: Container(
              //     margin: EdgeInsets.only(
              //       top: standardSize(context),
              //       right: standardSize(context),
              //       left: standardSize(context),
              //       bottom: standardSize(context),
              //     ),
              //     child: progressButton(context, "ادامه", false, () {
              //
              //     }, color: theme.accentColor),
              //   ),
              // )
            ],
          ),
        )));
  }
  Widget timeCard(BuildContext context, String text, String image,
      VoidCallback onTap, DepositVM model) {
    var theme = Theme.of(context);
    return Container(
      decoration: BoxDecoration(
          color: Color(0xfff3f4f9),
          // border: Border.all(
          //   color: model.isSelectIdPay || model.isSelectPay || model.isSelectZarinPal ? theme.accentColor : Colors.transparent
          // ),
          borderRadius: BorderRadius.circular(mediumSize(context))),
      padding: EdgeInsets.symmetric(
          vertical: mediumSize(context), horizontal: mediumSize(context)),
      margin: EdgeInsets.only(
          left: standardSize(context),
          right: standardSize(context),
          top: xSmallSize(context),
          bottom: mediumSize(context)),
      child: InkWell(
        splashColor: Colors.grey.shade300,
        borderRadius: BorderRadius.circular(mediumSize(context)),
        onTap: onTap,
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                text,
                style: theme.textTheme.caption!.copyWith(
                    color: Color(0xff474747),
                    fontSize: mediumSize(context) / 1.2
                ),
                textAlign: TextAlign.center,
              ),
              Container(
                width: standardSize(context),
                height: standardSize(context),
                child: Image.asset(
                  image,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


