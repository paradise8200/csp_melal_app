import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/pages/bas/home/home_page.dart';
import 'package:flutter_language_app/pages/bas/login_page/login_page.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/action_widgets.dart';

class CreateAccount extends StatefulWidget{

  @override
  State<StatefulWidget> createState()=>CreateAccountState();

}
class CreateAccountState extends State<CreateAccount>{
  @override
  Widget build(BuildContext context) {
    var theme=Theme.of(context);
      return Directionality(
        textDirection: TextDirection.rtl,
        child: Container(
          height: fullHeight(context),
          width: fullWidth(context),
          child: SingleChildScrollView(
            child: Stack(children: [
              Align(
                alignment: Alignment.topRight,
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: largeSize(context)),
                      child: Text(
                        "آماده ی سفر و یادگیری هستی؟",
                        style: theme.textTheme.bodyText1!.copyWith(
                            color: theme.accentColor,
                            fontSize: mediumSize(context) / 1.2),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: smallSize(context),right: smallSize(context),top: mediumSize(context)),
                      child: Text(
                        "سفر به سرزمین های مختلف دنیا و ایران برای یادگیری زبان برای شروع\nیکی از موارد زیر را انتخاب کنید",
                        style: theme.textTheme.caption!.copyWith(
                            color: theme.accentColor,
                            fontSize: smallSize(context)),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          top: xlargeSize(context)/1.2,
                          left: standardSize(context),
                          right: standardSize(context)),
                      width: fullWidth(context),
                      height: fullWidth(context)/2.5,
                      decoration: BoxDecoration(
                          boxShadow: [BoxShadow(color: Color(0xffD974C5),blurRadius:7,spreadRadius: 2,offset: Offset(0,0))],
                          gradient: LinearGradient(colors: [Color(0xffD885E6),Color(0xffD91D1D)],
                              tileMode: TileMode.clamp

                          ),

                          borderRadius: BorderRadius.circular(mediumSize(context))

                      ),
                      child: Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: smallSize(context),bottom: mediumSize(context)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                Text("دوره تخصصی\nصفر تا صد زبان انگلیسی",
                                    style: theme.textTheme.bodyText1!.copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w800,
                                        fontSize: mediumSize(context) / 1.2)
                                ),

                              ],),
                          ),

                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: xlargeSize(context)/1.3,
                          bottom: xlargeSize(context)/1.3,
                          left: standardSize(context),
                          right: standardSize(context)),
                      width: fullWidth(context),
                      height: fullWidth(context)/2.5,
                      decoration: BoxDecoration(
                          boxShadow: [BoxShadow(color: Color(0xffA573FF),blurRadius:4,spreadRadius: 2,offset: Offset(0,0))],
                          gradient: LinearGradient(colors: [Color(0xffA573FF),Color(0xff3428EA)],
                              tileMode: TileMode.clamp

                          ),

                          borderRadius: BorderRadius.circular(mediumSize(context))

                      ),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(),));
                        },
                        child: Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: smallSize(context),bottom: mediumSize(context)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  Text("دوره تخصصی\nصفر تا صد زبان انگلیسی",
                                      style: theme.textTheme.bodyText1!.copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w800,
                                          fontSize: mediumSize(context) / 1.2)
                                  ),
                                  // Text("",
                                  //     style: theme.textTheme.bodyText1!.copyWith(
                                  //         color: Colors.white,
                                  //         fontWeight: FontWeight.w800,
                                  //
                                  //         fontSize: mediumSize(context) / 1.2)),

                                ],),
                            ),

                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(),));
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            bottom: mediumSize(context),
                            // top: xlargeSize(context),
                            // bottom: mediumSize(context),
                            left: standardSize(context),
                            right: standardSize(context)),
                        width: fullWidth(context),
                        height: fullWidth(context)/2.5,
                        decoration: BoxDecoration(
                            boxShadow: [BoxShadow(color: Color(0xffFFB646),blurRadius:4,spreadRadius: 2,offset: Offset(0,0))],
                            gradient: LinearGradient(colors: [Color(0xffFFCC26),Color(0xffFF611C)],
                                tileMode: TileMode.clamp

                            ),

                            borderRadius: BorderRadius.circular(mediumSize(context))

                        ),
                        child: Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: smallSize(context),bottom: mediumSize(context)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  Text("دوره تخصصی\nصفر تا صد زبان انگلیسی",
                                      style: theme.textTheme.bodyText1!.copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w800,
                                          fontSize: mediumSize(context) / 1.2)
                                  ),


                                ],),
                            ),

                          ],
                        ),
                      ),
                    ),




                  ],
                ),
              ),
              Positioned(
                left: fullWidth(context)/9,
                top:mediumSize(context)*2.2,
                child: Container(
                  width: fullWidth(context)/3,
                  height: fullWidth(context),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/image_first.png")
                      )
                  ),

                ),
              ),
              Positioned(
                left: xxLargeSize(context)-smallSize(context),
                right: 0,
                top:0,
                bottom: xxLargeSize(context)*1.4,
                child: Container(
                  width: iconSize(context),
                  height:iconSize(context),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/image_send.png"),scale: iconSize(context)*1.2
                      )
                  ),

                ),
              ),
              Positioned(
                left:standardSize(context),
                top:fullWidth(context)/1.4,
                child: Container(
                  width: fullWidth(context)/2.2,
                  height: fullWidth(context),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/image_second.png"),scale:1
                      )
                  ),

                ),
              ),
              Positioned(
                left: xlargeSize(context),
                right: 0,
                top:xxLargeSize(context)*1.1,
                bottom:0,
                child: Container(
                  width: iconSize(context),
                  height:iconSize(context),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/image_send_second.png"),scale: iconSize(context)*1.2
                      )
                  ),

                ),
              ),
              Positioned(
                top: xxLargeSize(context)*3.45,
                left:xlargeSize(context)/1.8,
                child: Container(
                  width: fullWidth(context)/1.8,
                  height: fullWidth(context),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/image_third.png"),scale: 2
                      )
                  ),

                ),
              ),
              Positioned(
                right: 0,
                left:xlargeSize(context),
                bottom: 0,
                top: xxLargeSize(context)*4.95,
                child: Container(
                  width: iconSize(context)*1.2,
                  height:iconSize(context),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/image_send.png"),scale: iconSize(context)*1.2
                      )
                  ),

                ),
              ),




/*
              Container(
                width: fullWidth(context),
                height: fullHeight(context) / 3.5,
                child: Stack(
                  children: [
                    Positioned.fill(
                      child: Container(
                        margin: EdgeInsets.only(
                          left: mediumSize(context),
                          right: mediumSize(context),
                          top: smallSize(context),
                          bottom: smallSize(context),
                        ),
                        decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.circular(
                              mediumSize(context)),
                            gradient: LinearGradient(colors: [Color(0xffD885E6),Color(0xffD91D1D)]),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xfff8d56d),
                              spreadRadius: 1,
                              blurRadius: 8,
                              offset: Offset(0,
                                  2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Stack(
                          children: [
                            Positioned(
                              right:
                              standardSize(context) /
                                  0.9,
                              top: standardSize(context),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment
                                    .start,
                                children: [
                                  Container(
                                    child: Text(
                                        "دوره تخصصی\nصفر تا صد زبان انگلیسی",
                                        style: theme
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(
                                            color: Colors
                                                .white,
                                            shadows: [
                                              BoxShadow(
                                                color: Colors
                                                    .grey
                                                    .withOpacity(1),
                                                spreadRadius:
                                                5,
                                                blurRadius:
                                                7,
                                                offset: Offset(
                                                    0,
                                                    3), // changes position of shadow
                                              ),
                                            ],
                                            fontSize:
                                            standardSize(context) /
                                                1.5,
                                            fontWeight:
                                            FontWeight
                                                .w900)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: standardSize(
                                            context),
                                        right: xSmallSize(
                                            context)),
                                    decoration:
                                    BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(
                                          xSmallSize(
                                              context)),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(
                                                0xff4c456f),
                                            blurRadius: 0,
                                            spreadRadius:
                                            1.2,
                                            offset:
                                            Offset(
                                                4, 5))
                                      ],
                                      color: theme
                                          .backgroundColor,
                                    ),

                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(top: 0,
                        left: standardSize(context) / 0.7,
                        child: Container(

                          child: Image.asset(
                            "assets/image_first.png",
                          ),
                          width:
                          fullWidth(context) / 1.95,
                          height:
                          fullWidth(context) / 1.95,
                        ))
                  ],
                ),
              ),
*/
            ],),
          ),
        ),
      );
  }

}
