import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/enums/level_type.dart';
import 'package:flutter_language_app/pages/bas/deposit_notifier.dart';
import 'package:flutter_language_app/pages/bas/verify_page/verify_page.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/progress_button.dart';
import 'package:stacked/stacked.dart';

class LevelEnglish extends StatefulWidget {


  @override
  State<StatefulWidget> createState() => LevelEnglishState();
}

class LevelEnglishState extends State<LevelEnglish> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<DepositVM>.reactive(
        viewModelBuilder: () => DepositVM(),
        builder: (context, model, child) => Directionality(
              textDirection: TextDirection.ltr,
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                backgroundColor: theme.backgroundColor,
                body: Container(
                  height: fullHeight(context),
                  width: fullWidth(context),
                  child: Stack(
                    children: [
                      Positioned(
                        right: 0,
                        left: 0,
                        top: 0,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: largeSize(context)),
                              child: Text(
                                "آیا از قبـل انگـلیسی بلـدید؟",
                                style: theme.textTheme.bodyText1!.copyWith(
                                    color: theme.accentColor,
                                    fontSize: mediumSize(context) / 1.2),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: smallSize(context)),
                              child: Text(
                                "مسیر موفقیت و مسیر شکست تقریبا شبیه هم هستند\n!ما به شما کمک خواهیم کرد تا مکان مناسب را برای شروع درست کنید",
                                style: theme.textTheme.caption!.copyWith(
                                    color: theme.accentColor,
                                    fontSize: smallSize(context)),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              child: levelEnglish(
                                  context, "بسیار کم!", "assets/easy_level.png",
                                  () {
                                setState(() {
                                  model.setSelectedEasyLevel(
                                      LevelType.easy.index);
                                });
                              }),
                              margin: EdgeInsets.only(
                                top: standardSize(context),
                              ),
                            ),
                            levelEnglish(
                              context,
                              "مقداری!",
                              "assets/medium_level.png",
                              () {
                                setState(() {
                                  model.setSelectedEasyLevel(
                                      LevelType.medium.index);
                                });
                              },
                            ),
                            levelEnglish(
                              context,
                              "زیاد!",
                              "assets/hard_level.png",
                              () {
                                setState(() {
                                  model.setSelectedEasyLevel(
                                      LevelType.hard.index);
                                });
                              },
                            ),
                            levelEnglish(
                              context,
                              "بسیار زیاد!",
                              "assets/soHard_level.png",
                              () {
                                setState(() {
                                  model.setSelectedEasyLevel(
                                      LevelType.soHard.index);
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                      // Positioned(
                      //   left: 0,
                      //   bottom: 0,
                      //   right: 0,
                      //   child: Container(
                      //     margin: EdgeInsets.only(
                      //       top: standardSize(context),
                      //       right: standardSize(context),
                      //       left: standardSize(context),
                      //       bottom: standardSize(context),
                      //     ),
                      //     child: progressButton(context, "ادامه", false, () {
                      //
                      //     }, color: theme.accentColor),
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
            ));
  }

  Widget levelEnglish(
      BuildContext context, String text, String image, VoidCallback onTap) {
    var theme = Theme.of(context);
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
            color: Color(0xfff3f4f9),
            // border:  GradientBorder.uniform(
            //     gradient: LinearGradient(
            //       begin: Alignment.topLeft,
            //       end: Alignment.bottomRight,
            //       stops: [0.0, 1.0],
            //       colors: [
            //         Color(0xff3428ea),
            //         Color(0xffa573ff),
            //       ],
            //     ),
            //     width: 1) ,
            borderRadius: BorderRadius.circular(mediumSize(context))),
        padding: EdgeInsets.symmetric(
            vertical: mediumSize(context), horizontal: mediumSize(context)),
        margin: EdgeInsets.only(
            left: standardSize(context),
            right: standardSize(context),
            top: xSmallSize(context),
            bottom: mediumSize(context)),
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                text,
                style: theme.textTheme.caption!.copyWith(
                    color: Color(0xff474747),
                    fontSize: mediumSize(context) / 1.2),
                textAlign: TextAlign.center,
              ),
              Container(
                width: standardSize(context),
                height: standardSize(context),
                child: Image.asset(image),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
