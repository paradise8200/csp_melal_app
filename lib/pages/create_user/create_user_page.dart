import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/pages/bas/verify_page/verify_page.dart';
import 'package:flutter_language_app/pages/bas/verify_page/widgets/create_user_sheet_notifier.dart';
import 'package:flutter_language_app/theme/colors.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/progress_button.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stacked/stacked.dart';

class CreateUserPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => CreateUserPageState();
}

class CreateUserPageState extends State<CreateUserPage> {
  late TextEditingController nameController;
  late TextEditingController yearController;
  late TextEditingController monthController;
  late TextEditingController dayController;
  bool isOpenKeyboard = false;
  late double profileWidth;
  late double profileHeight;
  late double titleNameWidth;
  late double titleNameHeight;
  late double formPositioned;
  late FocusNode _focusNodeYear;
  late FocusNode _focusNodeMonth;
  late FocusNode _focusNodeDay;

  @override
  void initState() {
    nameController = TextEditingController();
    yearController = TextEditingController();
    monthController = TextEditingController();
    dayController = TextEditingController();
    _focusNodeYear = FocusNode();
    _focusNodeMonth = FocusNode();
    _focusNodeDay = FocusNode();

    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    nameController.dispose();
    yearController.dispose();
    monthController.dispose();
    _focusNodeYear.dispose();
    _focusNodeMonth.dispose();
    _focusNodeDay.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    isOpenKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;

    if (!isOpenKeyboard) {
      profileWidth = fullWidth(context) / 2.5;
      profileHeight = fullWidth(context) / 2.5;
      titleNameWidth = 1;
      titleNameHeight = -0.2;
      formPositioned = fullWidth(context) / 1.4;
    } else {
      profileHeight = fullWidth(context) / 4.5;
      profileWidth = fullWidth(context) / 4.5;
      titleNameWidth = 0;
      titleNameHeight = -0.4;
      formPositioned = fullWidth(context) / 2.3;
    }
    var theme = Theme.of(context);
    return ViewModelBuilder<CreateUserSheetVM>.reactive(
        viewModelBuilder: () => CreateUserSheetVM(context),
        builder: (context, model, child) => Directionality(
            textDirection: TextDirection.rtl,
            child: Scaffold(
                backgroundColor: theme.backgroundColor,
                resizeToAvoidBottomInset: true,
                body: ListView(
                  padding: EdgeInsets.symmetric(horizontal: standardSize(context),vertical: standardSize(context)),
                    children: [
                  GestureDetector(
                    onTap: () {
                      showModalBottomSheet(
                          context: context,
                          backgroundColor: Colors.transparent,
                          builder: (context) => Container(
                              padding: EdgeInsets.only(
                                right: largeSize(context),
                                left: largeSize(context),
                                bottom: smallSize(context),
                              ),
                              decoration: BoxDecoration(
                                  color: theme.backgroundColor,
                                  borderRadius: BorderRadius.only(
                                      topRight:
                                          Radius.circular(largeSize(context)),
                                      topLeft:
                                          Radius.circular(largeSize(context)))),
                              child: Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                            top: standardSize(context),
                                            bottom: smallSize(context),
                                          ),
                                          height: xxSmallSize(context),
                                          width: largeSize(context),
                                          decoration: BoxDecoration(
                                              color: Colors.grey.shade300,
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      largeSize(context))),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Material(
                                              color: Colors.transparent,
                                              child: InkWell(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        mediumSize(context)),
                                                onTap: () {
                                                  model.pickImage(
                                                      ImageSource.gallery);
                                                  Navigator.pop(context);
                                                },
                                                splashColor:
                                                    Colors.grey.shade300,
                                                child: Container(
                                                  padding: EdgeInsets.all(
                                                      xSmallSize(context)),
                                                  child: Column(
                                                    children: [
                                                      Icon(
                                                          CupertinoIcons
                                                              .photo_fill,
                                                          color:
                                                              theme.accentColor,
                                                          size: largeSize(
                                                              context)),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: smallSize(
                                                                context)),
                                                        child: Text(
                                                          "انتخاب از گالری",
                                                          style: theme.textTheme
                                                              .bodyText1!
                                                              .copyWith(
                                                                  color: theme
                                                                      .accentColor,
                                                                  fontSize:
                                                                      mediumSize(
                                                                              context) /
                                                                          1.2),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Material(
                                              color: Colors.transparent,
                                              child: InkWell(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        mediumSize(context)),
                                                onTap: () {
                                                  model.pickImage(
                                                      ImageSource.camera);
                                                  Navigator.pop(context);
                                                },
                                                splashColor:
                                                    Colors.grey.shade300,
                                                child: Container(
                                                  padding: EdgeInsets.all(
                                                      xSmallSize(context)),
                                                  child: Column(
                                                    children: [
                                                      Icon(
                                                        CupertinoIcons
                                                            .camera_fill,
                                                        color:
                                                            theme.accentColor,
                                                        size:
                                                            largeSize(context),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: smallSize(
                                                                context)),
                                                        child: Text(
                                                          "انتخاب از دوربین",
                                                          style: theme.textTheme
                                                              .bodyText1!
                                                              .copyWith(
                                                                  color: theme
                                                                      .accentColor,
                                                                  fontSize:
                                                                      mediumSize(
                                                                              context) /
                                                                          1.2),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ]))));
                    },
                    child: model.imageFile != null
                        ? Container(
                            width: fullWidth(context) / 2.5,
                            height: fullWidth(context) / 2.5,
                            child: ClipRRect(
                              child: Image.file(model.imageFile!,
                                  fit: BoxFit.cover),
                              borderRadius:
                                  BorderRadius.circular(xxLargeSize(context)),
                            ),
                          )
                        : Container(
                            width: fullWidth(context) / 2.5,
                            height: fullWidth(context) / 2.5,
                            child: ClipRRect(
                              borderRadius:
                                  BorderRadius.circular(xxLargeSize(context)),
                              child: Image(
                                  image: AssetImage("assets/pic_profile.png")),
                            ),
                          ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: fullHeight(context) / 9,bottom: smallSize(context)),
                    child: Text(
                      "نام و نام خانوادگی خود را وارد کنید",
                      style: theme.textTheme.bodyText1!.copyWith(
                          color: theme.accentColor,
                          fontSize: mediumSize(context) / 1.2),
                    ),
                  ),
                  Container(
                    child: TextFormField(
                      cursorColor: theme.accentColor,
                      controller: nameController,
                      style: theme.textTheme.caption!.copyWith(
                          color: Color(0xff474747),
                          fontSize: mediumSize(context) / 1.2),
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        fillColor: Color(0xfff3f4f9),
                        filled: true,
                        contentPadding: EdgeInsets.symmetric(
                            vertical: mediumSize(context),
                            horizontal: mediumSize(context)),
                        hintText: "نام و نام خانوادگی",
                        hintStyle: theme.textTheme.caption!.copyWith(
                            color: Color(0xff474747).withOpacity(0.40),
                            fontSize: mediumSize(context) / 1.2),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(mediumSize(context)),
                            borderSide: BorderSide(
                                color: theme.accentColor,
                                width: 1,
                                style: BorderStyle.none)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(mediumSize(context)),
                            borderSide: BorderSide(
                                color: theme.accentColor,
                                width: 1,
                                style: BorderStyle.none)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.circular(mediumSize(context)),
                            borderSide: BorderSide(
                                color: theme.accentColor,
                                width: 1,
                                style: BorderStyle.none)),
                      ),
                      maxLength: 22,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: smallSize(context)),
                    child: Text(
                      "سن خود را وارد کنید",
                      style: theme.textTheme.bodyText1!.copyWith(
                          color: theme.accentColor,
                          fontSize: mediumSize(context) / 1.2),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: fullWidth(context) / 4,
                        child: TextFormField(
                          controller: dayController,
                          cursorColor: theme.accentColor,
                          focusNode: _focusNodeDay,
                          textAlign: TextAlign.center,
                          style: theme.textTheme.caption!.copyWith(
                              color: Color(0xff474747),
                              fontSize: mediumSize(context) / 1.2),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            fillColor: Color(0xfff3f4f9),
                            filled: true,
                            contentPadding: EdgeInsets.symmetric(
                                vertical: mediumSize(context),
                                horizontal: mediumSize(context)),
                            hintText: "روز",
                            hintStyle: theme.textTheme.caption!.copyWith(
                                color: Color(0xff474747).withOpacity(0.40),
                                fontSize: mediumSize(context) / 1.2),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context)),
                                borderSide: BorderSide(
                                    color: theme.accentColor,
                                    width: 1,
                                    style: BorderStyle.none)),
                            enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context)),
                                borderSide: BorderSide(
                                    color: theme.accentColor,
                                    width: 1,
                                    style: BorderStyle.none)),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context)),
                                borderSide: BorderSide(
                                    color: theme.accentColor,
                                    width: 1,
                                    style: BorderStyle.none)),
                          ),
                          maxLength: 2,
                        ),
                      ),
                      Container(
                        width: fullWidth(context) / 4,
                        child: TextFormField(
                          controller: monthController,
                          focusNode: _focusNodeMonth,
                          onChanged: (val) {
                            if (monthController.text.length == 2) {
                              setState(() {
                                _focusNodeDay.requestFocus();
                              });
                            }
                          },
                          cursorColor: theme.accentColor,
                          textAlign: TextAlign.center,
                          style: theme.textTheme.caption!.copyWith(
                              color: Color(0xff474747),
                              fontSize: mediumSize(context) / 1.2),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            fillColor: Color(0xfff3f4f9),
                            filled: true,
                            contentPadding: EdgeInsets.symmetric(
                                vertical: mediumSize(context),
                                horizontal: mediumSize(context)),
                            hintText: "ماه",
                            hintStyle: theme.textTheme.caption!.copyWith(
                                color: Color(0xff474747).withOpacity(0.40),
                                fontSize: mediumSize(context) / 1.2),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context)),
                                borderSide: BorderSide(
                                    color: theme.accentColor,
                                    width: 1,
                                    style: BorderStyle.none)),
                            enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context)),
                                borderSide: BorderSide(
                                    color: theme.accentColor,
                                    width: 1,
                                    style: BorderStyle.none)),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context)),
                                borderSide: BorderSide(
                                    color: theme.accentColor,
                                    width: 1,
                                    style: BorderStyle.none)),
                          ),
                          maxLength: 2,
                        ),
                      ),
                      Container(
                        width: fullWidth(context) / 4,
                        child: TextFormField(
                          controller: yearController,
                          focusNode: _focusNodeYear,
                          onChanged: (val) {
                            if (yearController.text.length == 4) {
                              setState(() {
                                _focusNodeMonth.requestFocus();
                              });
                            }
                          },
                          cursorColor: theme.accentColor,
                          textAlign: TextAlign.center,
                          style: theme.textTheme.caption!.copyWith(
                              color: Color(0xff474747),
                              fontSize: mediumSize(context) / 1.2),
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            fillColor: Color(0xfff3f4f9),
                            filled: true,
                            contentPadding: EdgeInsets.symmetric(
                                vertical: mediumSize(context),
                                horizontal: mediumSize(context)),
                            hintText: "سال",
                            hintStyle: theme.textTheme.caption!.copyWith(
                                color: Color(0xff474747).withOpacity(0.40),
                                fontSize: mediumSize(context) / 1.2),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context)),
                                borderSide: BorderSide(
                                    color: theme.accentColor,
                                    width: 1,
                                    style: BorderStyle.none)),
                            enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context)),
                                borderSide: BorderSide(
                                    color: theme.accentColor,
                                    width: 1,
                                    style: BorderStyle.none)),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(mediumSize(context)),
                                borderSide: BorderSide(
                                    color: theme.accentColor,
                                    width: 1,
                                    style: BorderStyle.none)),
                          ),
                          maxLength: 4,
                        ),
                      ),
                    ],
                  ),
                ]))));
  }
}
