// import 'package:flutter/material.dart';
// import 'package:flutter_language_app/pages/create_user/level_english_page.dart';
// import 'package:flutter_language_app/pages/create_user/time_goal_page.dart';
// import 'package:flutter_language_app/theme/colors.dart';
// import 'package:flutter_language_app/theme/dimens.dart';
//
// import '../main_page.dart';
//
// class AgesPage extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => AgesPageState();
// }
//
// class AgesPageState extends State<AgesPage> {
//   @override
//   Widget build(BuildContext context) {
//     var theme = Theme.of(context);
//     return Directionality(
//         textDirection: TextDirection.ltr,
//         child: Scaffold(
//           backgroundColor: theme.backgroundColor,
//           body: SingleChildScrollView(
//             physics: BouncingScrollPhysics(),
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Container(
//                   alignment: Alignment.center,
//                   margin: EdgeInsets.only(top: xxLargeSize(context)),
//                   child: Text("شمـا چنـد سـال داریـد؟",
//                       textAlign: TextAlign.center,
//                       style: theme.textTheme.headline4!
//                           .copyWith(color: AppColors.textColorLight)),
//                 ),
//                 Container(
//                   alignment: Alignment.center,
//                   margin: EdgeInsets.only(top: mediumSize(context)),
//                   child: Text(
//                     "مسیر موفقیت و مسیر شکست تقریبا شبیه هم هستند\n!ما به شما کمک خواهیم کرد تا مکان مناسب را برای شروع درست کنید",
//                     style: theme.textTheme.caption!.copyWith(
//                       color: AppColors.textColorDark,
//                     ),
//                     textAlign: TextAlign.center,
//                   ),
//                 ),
//                 Container(
//                   child: level(context, "زیر 18", "image"),
//                   margin: EdgeInsets.only(top: smallSize(context)),
//                 ),
//                 level(context, "18-24", "image"),
//                 level(context, "25-34", "image"),
//                 level(context, "35-44", "image"),
//                 level(context, "45-54", "image"),
//                 level(context, "55-64", "image"),
//                 level(context, "65-74", "image"),
//               ],
//             ),
//           ),
//         ));
//   }
//
//   Widget level(
//       BuildContext context,
//       String text,
//       String image,
//       ) {
//     var theme = Theme.of(context);
//     return Container(
//       decoration: BoxDecoration(
//           color: Color(0xfff3f4f9),
//           borderRadius: BorderRadius.circular(mediumSize(context))),
//       padding: EdgeInsets.symmetric(
//           vertical: mediumSize(context), horizontal: mediumSize(context)),
//       margin: EdgeInsets.only(
//           left: standardSize(context),
//           right: standardSize(context),
//           top: xSmallSize(context),
//           bottom: standardSize(context)),
//       child: InkWell(
//         splashColor: Colors.grey.shade300,
//         borderRadius: BorderRadius.circular(mediumSize(context)),
//         onTap: () {
//           Navigator.pushReplacement(
//               context, MaterialPageRoute(builder: (context) => MainPage()));
//         },
//         child: Directionality(
//           textDirection: TextDirection.rtl,
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Text(
//                 text,
//                 style: theme.textTheme.headline4!.copyWith(
//                   fontSize: caption1Size(context),
//                   color: AppColors.textColorLight,
//                 ),
//                 textAlign: TextAlign.center,
//               ),
//               Container(
//                 width: standardSize(context),
//                 height: standardSize(context),
//                 child: Image.asset(
//                   image,
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
