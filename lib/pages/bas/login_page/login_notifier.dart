import 'package:flutter_language_app/app/app.router.dart';
import 'package:flutter_language_app/app/locator.dart';
import 'package:flutter_language_app/services/preferences_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class LogInVM extends BaseViewModel{
  // final AppPreferences pref = locator<AppPreferences>();
  final NavigationService navigationService = locator<NavigationService>();






  void saveDataAndNavigation(String  phoneNumber) async {
    try {

      // var user = result.user;
      //
      // pref.user.phone = phoneNumber;
      // pref.phoneNumber = phoneNumber;
      navigationService.navigateTo(Routes.verifyPage,arguments: VerifyPageArguments(phoneNumber: phoneNumber));

      // print("pref.user.phone: ${pref.user?.phone}");
      // print("pref.phoneNumber: ${ pref.phoneNumber}");

      // pref.token = result.token;

      // if (user == null || user.firstName.isEmpty || user?.firstName == null || user?.firstName == "") {
      //   await navigateToUserInfo(user);
      // } else {
      //   pref.user = result.user;
      //
      //   _navigationService.replaceWith(Routes.mainPage,
      //       arguments: MainPageArguments(selectedIndex: 0));
      // }
    } catch (e) {
      print(e);
    }
  }

}