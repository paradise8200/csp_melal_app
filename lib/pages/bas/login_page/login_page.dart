import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_language_app/app/locator.dart';
import 'package:flutter_language_app/enums/snackbar_type.dart';
import 'package:flutter_language_app/pages/bas/verify_page/verify_page.dart';
import 'package:flutter_language_app/theme/colors.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/utilities/close_keybored.dart';
import 'package:flutter_language_app/widgets/progress_button.dart';
import 'package:lottie/lottie.dart';
import 'package:page_transition/page_transition.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import 'login_notifier.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  late TextEditingController controller;
  final _formKey = GlobalKey<FormState>();
  bool isOpenKeyboard = false;
  bool isCheckPhoneNumber = false;
  late double lottieHeight;
  late double formHeight;

  @override
  void initState() {
    controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    isOpenKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;


    if (!isOpenKeyboard) {
      lottieHeight = fullHeight(context) / 2.5  ;
      formHeight = fullHeight(context) < 700 ? fullHeight(context) / 2.2 : fullHeight(context) / 2.1;
    } else {
      lottieHeight = fullHeight(context) / 5;
      formHeight = fullHeight(context) < 700 ? fullHeight(context) / 5.1 : fullHeight(context) / 4.3;
    }

    return ViewModelBuilder<LogInVM>.reactive(
        viewModelBuilder: () => LogInVM(),
        builder: (context, model, child) => GestureDetector(
          onTap: () => closeKeyboard(context),
          child: Directionality(
                textDirection: TextDirection.rtl,
                child: Scaffold(
                    resizeToAvoidBottomInset: true,
                    backgroundColor: theme.backgroundColor,
                    appBar: AppBar(
                      brightness: Brightness.light,
                      elevation: 0,
                      toolbarHeight: 0,
                    ),
                    body: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: standardSize(context)),
                      height: fullHeight(context),
                      child: Stack(
                        children: [
                          AnimatedContainer(
                            duration: Duration(milliseconds: 300),
                            child: Lottie.asset(
                              "assets/lottie_welcome.json",
                              fit: BoxFit.cover,
                            ),
                            height: lottieHeight,
                            width: fullWidth(context),
                            alignment: Alignment.center,
                          ),
                          AnimatedPositioned(
                            top: formHeight,
                            left: 0,
                            right: 0,
                            duration: Duration(milliseconds: 300),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "به ملل خوش اومدی!",
                                    textAlign: TextAlign.center,
                                    style: theme.textTheme.bodyText1!.copyWith(
                                        color: theme.accentColor,
                                        fontWeight: FontWeight.w700,
                                        fontSize: standardSize(context)),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical: smallSize(context)),
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "لطفا شماره موبایل تو وارد کن تا کد رو واست بفرستیم!",
                                    style: theme.textTheme.bodyText1!.copyWith(
                                        color: theme.accentColor,
                                        fontSize: mediumSize(context) / 1.2),
                                  ),
                                ),
                                Form(
                                  key: _formKey,
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "شماره موبایل تو وارد کن تا بریم مرحله بعد!";
                                      } else if (!value.startsWith("09") ||
                                          value.length < 11) {
                                        return "شماره موبایلت درست نیس!";
                                      }
                                      return null;
                                    },
                                    cursorColor: theme.accentColor,
                                    controller: controller,
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(
                                          RegExp(r'[0-9]')),
                                    ],
                                    style: theme.textTheme.caption!.copyWith(
                                        color: Color(0xff474747),
                                        fontSize: mediumSize(context) / 1.2),
                                    keyboardType: TextInputType.number,
                                    onChanged: (value) {
                                      if(controller.text.length == 11){
                                        setState(() {
                                          isCheckPhoneNumber = true;
                                        });
                                      }
                                    },
                                    decoration: InputDecoration(
                                      helperStyle: TextStyle(
                                          fontSize: 0,
                                          color: Colors.transparent,
                                          height: 0),
                                      errorStyle: theme.textTheme.caption!
                                          .copyWith(
                                              color: Color(0xFFF54848),
                                              fontSize: smallSize(context)),
                                      errorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(
                                              mediumSize(context)),
                                          borderSide: BorderSide(
                                              color: Color(0xFFF54848),
                                              width: 1)),
                                      focusedErrorBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(
                                              mediumSize(context)),
                                          borderSide: BorderSide(
                                              color: Color(0xFFF54848),
                                              width: 1)),
                                      fillColor: Color(0xfff3f4f9),
                                      filled: true,
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: mediumSize(context),
                                          horizontal: mediumSize(context)),
                                      hintText: "شماره موبایل",
                                      hintStyle: theme.textTheme.caption!
                                          .copyWith(
                                              color: Color(0xff474747)
                                                  .withOpacity(0.40),
                                              fontSize:
                                                  mediumSize(context) / 1.2),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(
                                              mediumSize(context)),
                                          borderSide: BorderSide(
                                              color: theme.accentColor,
                                              width: 1,
                                              style: BorderStyle.none)),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(
                                              mediumSize(context)),
                                          borderSide: BorderSide(
                                              color: theme.accentColor,
                                              width: 1,
                                              style: BorderStyle.none)),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(
                                              mediumSize(context)),
                                          borderSide: BorderSide(
                                              color: theme.accentColor,
                                              width: 1,
                                              style: BorderStyle.none)),
                                    ),
                                    maxLength: 11,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              margin:
                                  EdgeInsets.only(bottom: standardSize(context)),
                              child: Opacity(
                                opacity: isCheckPhoneNumber ? 1 : 0.4,
                                child: progressButton(
                                    context,
                                    "بزن بریم!",
                                    false,
                                    isCheckPhoneNumber
                                        ? () {
                                            if (_formKey.currentState!
                                                .validate()) {
                                              model.saveDataAndNavigation(controller.text);
                                            }
                                          }
                                        : () {
                                            !_formKey.currentState!.validate();
                                          }),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )),
              ),
        )
    );
  }
}
