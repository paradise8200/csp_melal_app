// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_language_app/fakeData.dart';
// import 'package:flutter_language_app/lessons/intro_lesson_page.dart';
// import 'package:flutter_language_app/models/lesson_model.dart';
// import 'package:flutter_language_app/pages/bas/lesson_page/lesson_notifier.dart';
// import 'package:flutter_language_app/theme/colors.dart';
// import 'package:flutter_language_app/theme/dimens.dart';
// import 'package:flutter_language_app/theme/text_widgets.dart';
// import 'package:flutter_language_app/widgets/action_widgets.dart';
// import 'package:flutter_language_app/widgets/appbar.dart';
// import 'package:flutter_language_app/widgets/image_loading.dart';
// import 'package:flutter_language_app/widgets/image_widget.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:lottie/lottie.dart';
// import 'package:shimmer/shimmer.dart';
// import 'package:stacked/stacked.dart';
//
// class LessonPageTest extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => LessonPageState();
// }
//
// class LessonPageState extends State<LessonPageTest>
//     with TickerProviderStateMixin {
//   late AnimationController _controllerBannerHeader = AnimationController(
//     duration: const Duration(milliseconds: 1500),
//     vsync: this,
//   )..forward();
//   late Animation<Offset> _offsetAnimationBannerHeader = Tween<Offset>(
//     begin: const Offset(1.5, 0.0),
//     end: Offset.zero,
//   ).animate(CurvedAnimation(
//     parent: _controllerBannerHeader,
//     curve: Curves.decelerate,
//   ));
//
//   bool _animateListStep = false;
//   static bool _isStartListStep = true;
//
//   late AnimationController _controllerBannerFooter = AnimationController(
//     duration: const Duration(milliseconds: 2000),
//     vsync: this,
//   )..forward();
//   late Animation<Offset> _offsetAnimationBannerFooter = Tween<Offset>(
//     begin: const Offset(-1.5, 0.0),
//     end: Offset.zero,
//   ).animate(CurvedAnimation(
//     parent: _controllerBannerHeader,
//     curve: Curves.decelerate,
//   ));
//
//   @override
//   void initState() {
//     super.initState();
//
//     _isStartListStep
//         ? Future.delayed(Duration(milliseconds: 3500), () {
//             setState(() {
//               _animateListStep = true;
//               _isStartListStep = false;
//             });
//           })
//         : _animateListStep = true;
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     _controllerBannerHeader.dispose();
//     _controllerBannerFooter.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     var theme = Theme.of(context);
//     return ViewModelBuilder<LessonVM>.reactive(
//         viewModelBuilder: () => LessonVM(),
//         builder: (context, model, child) => AnnotatedRegion(
//             value: SystemUiOverlayStyle(
//               statusBarColor: Colors.transparent,
//               statusBarBrightness: Brightness.light,
//             ),
//             child: Directionality(
//               child: ,
//               textDirection: TextDirection.rtl,
//             )));
//   }
//
//   // Widget shimmerLessonPage(bool isBusy) {
//   //   var theme = Theme.of(context);
//   //   return Container(
//   //     height: fullHeight(context),
//   //     child: SingleChildScrollView(
//   //         primary: true,
//   //         physics: BouncingScrollPhysics(),
//   //         child: Column(mainAxisSize: MainAxisSize.max, children: [
//   //           Shimmer.fromColors(
//   //               baseColor: Colors.grey.shade400,
//   //               highlightColor: Colors.grey.shade300,
//   //               child: Container(
//   //                 margin: EdgeInsets.only(
//   //                     left: standardSize(context),
//   //                     right: standardSize(context),
//   //                     top: fullHeight(context) / 18,
//   //                     bottom: standardSize(context)),
//   //                 width: fullWidth(context),
//   //                 height: fullHeight(context) / 3.3,
//   //                 decoration: BoxDecoration(
//   //                   borderRadius: BorderRadius.circular(standardSize(context)),
//   //                   color: Colors.grey.shade300,
//   //                 ),
//   //               )),
//   //           ListView.builder(
//   //             primary: false,
//   //             padding: EdgeInsets.all(mediumSize(context)),
//   //             physics: NeverScrollableScrollPhysics(),
//   //             itemCount: lessonList(context).length,
//   //             shrinkWrap: true,
//   //             itemBuilder: (context, index) =>
//   //                 lessonItemWidget(lessonList(context)[index], isBusy, index),
//   //           ),
//   //           Shimmer.fromColors(
//   //               baseColor: Colors.grey.shade400,
//   //               highlightColor: Colors.grey.shade300,
//   //               child: Container(
//   //                 margin: EdgeInsets.only(
//   //                     left: standardSize(context),
//   //                     right: standardSize(context),
//   //                     top: fullHeight(context) / 18,
//   //                     bottom: standardSize(context)),
//   //                 width: fullWidth(context),
//   //                 height: fullHeight(context) / 3.7,
//   //                 decoration: BoxDecoration(
//   //                   borderRadius: BorderRadius.circular(standardSize(context)),
//   //                   color: Colors.grey.shade300,
//   //                 ),
//   //               )),
//   //         ])),
//   //   );
//   // }
//
//   Widget lessonItemWidget(Lesson lesson, bool isBusy, int index) {
//     return isBusy == true
//         ? Container(
//             width: fullWidth(context),
//             child: Row(
//               children: [
//                 Shimmer.fromColors(
//                   baseColor: Colors.grey.shade300,
//                   highlightColor: Colors.grey.shade200,
//                   child: Container(
//                     margin: EdgeInsets.symmetric(vertical: smallSize(context)),
//                     width: xxLargeSize(context) / 1.3,
//                     height: xxLargeSize(context) / 1.3,
//                     decoration: BoxDecoration(
//                         shape: BoxShape.circle, color: Colors.grey.shade400),
//                   ),
//                 ),
//                 Expanded(
//                   child: Container(
//                     margin:
//                         EdgeInsets.symmetric(horizontal: smallSize(context)),
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Shimmer.fromColors(
//                             baseColor: Colors.grey.shade300,
//                             highlightColor: Colors.grey.shade200,
//                             child: Container(
//                               decoration: BoxDecoration(
//                                   color: Colors.grey.shade300,
//                                   borderRadius: BorderRadius.circular(
//                                       xxSmallSize(context))),
//                               width: xxLargeSize(context),
//                               height: mediumSize(context),
//                             )),
//                         Container(
//                           margin: EdgeInsets.only(top: mediumSize(context)),
//                           child: Shimmer.fromColors(
//                               baseColor: Colors.grey.shade300,
//                               highlightColor: Colors.grey.shade200,
//                               child: Container(
//                                 decoration: BoxDecoration(
//                                     color: Colors.grey.shade300,
//                                     borderRadius: BorderRadius.circular(
//                                         xxSmallSize(context))),
//                                 width: xxLargeSize(context),
//                                 height: mediumSize(context),
//                               )),
//                         )
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           )
//         : GestureDetector(
//             onTap: lesson.opPress,
//             child: AnimatedOpacity(
//               duration: Duration(milliseconds: 2000),
//               opacity: _animateListStep ? 1 : 0,
//               curve: Curves.easeInOutQuart,
//               child: AnimatedPadding(
//                 duration: Duration(milliseconds: 2500),
//                 padding: _animateListStep
//                     ? const EdgeInsets.all(4.0)
//                     : const EdgeInsets.only(top: 10),
//                 child: Container(
//                   width: fullWidth(context),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Row(
//                         children: [
//                           Container(
//                             height: fullWidth(context) / 3.5,
//                             child: Stack(
//                               children: [
//                                 Positioned.fill(
//                                   child: Container(
//                                     decoration: BoxDecoration(
//                                         shape: BoxShape.circle,
//                                         boxShadow: [
//                                           BoxShadow(
//                                             color: Colors.grey.shade300,
//                                             blurRadius: 10,
//                                             spreadRadius: 2,
//                                           )
//                                         ],
//                                         border: Border.all(
//                                             width: 9,
//                                             color: lesson.isDone
//                                                 ? lesson.color
//                                                 : Colors.grey.shade200)),
//                                   ),
//                                 ),
//                                 Positioned(
//                                   top: mediumSize(context),
//                                   left: mediumSize(context),
//                                   right: mediumSize(context),
//                                   bottom: mediumSize(context),
//                                   child: ClipRRect(
//                                     borderRadius: BorderRadius.circular(
//                                         xxLargeSize(context)),
//                                     child: imageWidget(lesson.image,
//                                         fit: BoxFit.cover),
//                                   ),
//                                 ),
//                                 lesson.isDone
//                                     ? Align(
//                                         alignment: Alignment(0, 1.3),
//                                         child: Container(
//                                           padding: EdgeInsets.all(
//                                               xSmallSize(context)),
//                                           child: Container(
//                                               width: fullWidth(context) / 26,
//                                               height: fullWidth(context) / 26,
//                                               child: SvgPicture.asset(
//                                                 'assets/ic_tick.svg',
//                                                 color: Colors.white,
//                                               )),
//                                           decoration: BoxDecoration(
//                                               color: lesson.color,
//                                               shape: BoxShape.circle),
//                                         ),
//                                       )
//                                     : Container(),
//                               ],
//                             ),
//                             width: fullWidth(context) / 3.5,
//                           ),
//                           Expanded(
//                             child: Container(
//                               margin: EdgeInsets.symmetric(
//                                   horizontal: smallSize(context)),
//                               child: Column(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: [
//                                   Text(lesson.title,
//                                       style: Theme.of(context)
//                                           .textTheme
//                                           .bodyText1!
//                                           .copyWith(
//                                               color: Color(0xff4c456f),
//                                               fontSize:
//                                                   standardSize(context) / 1.5,
//                                               fontWeight: FontWeight.w900)),
//                                   Container(
//                                     margin: EdgeInsets.only(
//                                         top: xSmallSize(context)),
//                                     child: Row(
//                                       children: [
//                                         icon((() {
//                                           switch (lesson.type) {
//                                             case lessonType.video:
//                                               return Icons.video_library_sharp;
//                                             case lessonType.speaking:
//                                               return Icons.chat_rounded;
//                                             case lessonType.words:
//                                               return Icons.menu_book_outlined;
//                                             case lessonType.writing:
//                                               return Icons.text_format_sharp;
//                                             case lessonType.Exercises:
//                                               return Icons.assignment;
//                                           }
//                                         })()),
//                                         text(context, (() {
//                                           switch (lesson.type) {
//                                             case lessonType.video:
//                                               return "Video";
//                                             case lessonType.speaking:
//                                               return "Speaking";
//                                             case lessonType.words:
//                                               return "Vocabulary";
//                                             case lessonType.writing:
//                                               return "Writing";
//                                             case lessonType.Exercises:
//                                               return "Exercises";
//                                           }
//                                         })()),
//                                       ],
//                                     ),
//                                   )
//                                 ],
//                               ),
//                             ),
//                           ),
//                         ],
//                       ),
//                       Align(
//                         alignment: Alignment(0.71, -0.5),
//                         child: lesson.isLast == true
//                             ? Container()
//                             : Container(
//                                 width: xSmallSize(context),
//                                 height: fullWidth(context) / 6,
//                                 decoration: BoxDecoration(
//                                     borderRadius: BorderRadius.circular(16),
//                                     color: lesson.isDone
//                                         ? lesson.color
//                                         : Colors.grey.shade200),
//                               ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           );
//   }
// }
//
// Widget text(BuildContext context, text) {
//   return Container(
//     margin: EdgeInsets.only(right: xSmallSize(context)),
//     child: bodyText2(context, text, color: Color(0xff4c456f)),
//   );
// }
//
// Widget icon(IconData iconData) {
//   return Icon(
//     iconData,
//     color: Color(0xff4c456f),
//   );
// }
// /*
//               child: CustomScrollView(slivers: [
//                 SliverAppBar(
//                   expandedHeight: fullWidth(context)/2,
//                   flexibleSpace:
//                       Container(color: Colors.blue,)
//                   ,
//                   centerTitle: true,
//                   elevation: 0,
//                   brightness: Brightness.light,
//                   backgroundColor: Colors.white,
//                   leading: Container(
//                     margin: EdgeInsets.only(right: mediumSize(context)),
//                     child: backIcon(context),
//                   ),
//                   title: Text(
//                     "خانواده و فامیل",
//                     style: Theme.of(context).textTheme.bodyText1!.copyWith(
//                           color: Color(0xff474747),
//                         ),
//                   ),
//                 ),
//                 SliverToBoxAdapter(
//                   child: model.isBusy
//                       ? Center(child: CupertinoActivityIndicator())
//                       : SingleChildScrollView(
//                         child: Container(
//                           decoration: BoxDecoration(
//                             color: Colors.white,
//                             borderRadius: BorderRadius.circular(24)
//                           ),
//                             child: Column(
//                               mainAxisSize: MainAxisSize.max,
//                               children: [
//                                 ListView.builder(
//                                   primary: false,
//                                   padding: EdgeInsets.all(mediumSize(context)),
//                                   physics: NeverScrollableScrollPhysics(),
//                                   itemCount: lessonList(context).length,
//                                   shrinkWrap: true,
//                                   itemBuilder: (context, index) =>
//                                       lessonItemWidget(lessonList(context)[index],
//                                           model.isBusy, index),
//                                 ),
//                                 SlideTransition(
//                                   position: _offsetAnimationBannerFooter,
//                                   child: Container(
//                                     margin: EdgeInsets.only(
//                                         left: standardSize(context),
//                                         right: standardSize(context),
//                                         bottom: standardSize(context)),
//                                     width: fullWidth(context),
//                                     height: fullWidth(context) / 1.9,
//                                     decoration: BoxDecoration(boxShadow: [
//                                       BoxShadow(
//                                         color:
//                                             Colors.blueAccent.withOpacity(0.20),
//                                         spreadRadius: 1,
//                                         blurRadius: 12,
//                                         offset: Offset(
//                                             0, 16), // changes position of shadow
//                                       ),
//                                     ]),
//                                     child: Stack(
//                                       children: [
//                                         Positioned(
//                                           left: 0,
//                                           right: 0,
//                                           top: standardSize(context),
//                                           bottom: 0,
//                                           child: Container(
//                                             decoration: BoxDecoration(
//                                                 borderRadius:
//                                                     BorderRadius.circular(16),
//                                                 image: DecorationImage(
//                                                     fit: BoxFit.cover,
//                                                     image: AssetImage(
//                                                         "assets/bg_finish_lesson.jpg"))),
//                                           ),
//                                         ),
//                                         Positioned(
//                                           top: 0,
//                                           right: 0,
//                                           child: Lottie.asset(
//                                             "assets/lottie_nice_job.json",
//                                             width: fullWidth(context) / 2,
//                                             height: fullWidth(context) / 2,
//                                           ),
//                                         ),
//                                         Positioned(
//                                           top: xlargeSize(context),
//                                           child: Column(
//                                             children: [
//                                               Container(
//                                                 margin: EdgeInsets.only(
//                                                     top: mediumSize(context)),
//                                                 child: headline3(
//                                                     context, "Great Job",
//                                                     color: Colors.white),
//                                               ),
//                                               Container(
//                                                 margin: EdgeInsets.only(
//                                                     top: xxSmallSize(context)),
//                                                 child: bodyText2(
//                                                   context,
//                                                   "Next lesson",
//                                                   color: Colors.white,
//                                                 ),
//                                               ),
//                                               Container(
//                                                 alignment: Alignment.center,
//                                                 margin: EdgeInsets.only(
//                                                     top: smallSize(context)),
//                                                 height: fullWidth(context) / 12,
//                                                 width: fullWidth(context) / 4.6,
//                                                 decoration: BoxDecoration(
//                                                     color: theme.accentColor,
//                                                     borderRadius:
//                                                         BorderRadius.circular(16),
//                                                     boxShadow: [BoxShadow()]),
//                                                 child: bodyText2(
//                                                   context,
//                                                   "Start",
//                                                   color: Colors.white,
//                                                 ),
//                                               ),
//                                             ],
//                                           ),
//                                           left: mediumSize(context),
//                                           bottom: mediumSize(context),
//                                         )
//                                       ],
//                                     ),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                       ),
//                 ),
//               ]),
// *//*
//
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_language_app/models/lesson_model.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/theme/text_widgets.dart';
import 'package:flutter_language_app/widgets/action_widgets.dart';
import 'package:flutter_language_app/widgets/image_widget.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:shimmer/shimmer.dart';
import 'package:stacked/stacked.dart';

import '../../../fakeData.dart';
import 'lesson_notifier.dart';
import 'lesson_page.dart';

class LessonPageTest extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeState();
}

class HomeState extends State<LessonPageTest> with TickerProviderStateMixin {
  bool _animateListStep = false;
  static bool _isStartListStep = true;
  late AnimationController _controllerBannerHeader = AnimationController(
    duration: const Duration(milliseconds: 1500),
    vsync: this,
  )..forward();
  late Animation<Offset> _offsetAnimationBannerHeader = Tween<Offset>(
    begin: const Offset(1.5, 0.0),
    end: Offset.zero,
  ).animate(CurvedAnimation(
    parent: _controllerBannerHeader,
    curve: Curves.decelerate,
  ));
  GlobalKey key = GlobalKey();
  double contentBoxTop = 250;
  late Animation<Offset> _offsetAnimationBannerFooter = Tween<Offset>(
    begin: const Offset(-1.5, 0.0),
    end: Offset.zero,
  ).animate(CurvedAnimation(
    parent: _controllerBannerHeader,
    curve: Curves.decelerate,
  ));

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback(_afterLayout);
    _isStartListStep
        ? Future.delayed(Duration(milliseconds: 3500), () {
            setState(() {
              _animateListStep = true;
              _isStartListStep = false;
            });
          })
        : _animateListStep = true;
  }

  _afterLayout(_) {
    _getPosition();
  }

  void _getPosition() {
    RenderBox box = key.currentContext!.findRenderObject() as RenderBox;
    setState(() {
      contentBoxTop = box.size.height;
    });
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<LessonVM>.reactive(
        viewModelBuilder: () => LessonVM(),
        builder: (context, model, child) =>Directionality (
          textDirection: TextDirection.rtl,
          child: Scaffold(

              body: NestedScrollView(
                  headerSliverBuilder: (context, innerBoxIsScrolled) {
                    return <Widget>[
                      SliverAppBar(
                        leading:    IconButton(
                    splashRadius: standardSize(context) / 1.2,
                    splashColor: Colors.grey.shade300,
                    icon: SvgPicture.asset(
                    "assets/back.svg",
                    color: Colors.white,
                    height: standardSize(context) / 1.2,
                    width: standardSize(context) / 1.2,
                    ),
                    onPressed: () {
                    Navigator.pop(context);
                    },
                    ),

                    title: Text(
                          "خانواده و فامیل",
                          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: Colors.white,
                          ),
                        ),
                        centerTitle: true,


                        flexibleSpace:
                        Stack(children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                  'assets/nathan-dumlao-4_mJ1TbMK8A-unsplash.jpg',
                                ),
                              ),
                            ),
                            height: 350.0,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                gradient: LinearGradient(
                                  tileMode: TileMode.clamp,

                                    colors: [
                                      Color(0xffD8364C).withOpacity(0.50),
                                      Color(0xffD883E1).withOpacity(0.50),
                                    ],
                                    stops: [
                                      0.0,
                                      1.0
                                    ])),
                          )
                        ]),


                        bottom:

                        PreferredSize(
                          child: Container(
                            height: standardSize(context),
                            decoration: BoxDecoration(

                                boxShadow: [BoxShadow(color: Colors.white)],
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(100),
                                    topRight: Radius.circular(100)),
                                color: Colors.white),
                          ),
                          preferredSize: Size(double.infinity, 1),
                        ),
                        expandedHeight: fullWidth(context)/1.6,
                        backgroundColor: Colors.white,
                        elevation: 0,
                        foregroundColor: Colors.white,

                        toolbarHeight: kToolbarHeight,
                        floating: false,


                        pinned: true,
                        shadowColor: Colors.white,
                      ),
                    ];
                  },
                  body:
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: SingleChildScrollView(
                      child: Column(children: [
                        ListView.builder(
                          primary: false,
                          padding: EdgeInsets.all(mediumSize(context)),
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: lessonList(context).length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) => lessonItemWidget(
                              lessonList(context)[index], model.isBusy, index),
                        ),
                        SlideTransition(
                          position: _offsetAnimationBannerFooter,
                          child: Container(
                            margin: EdgeInsets.only(
                                left: standardSize(context),
                                right: standardSize(context),
                                bottom: standardSize(context)),
                            width: fullWidth(context),
                            height: fullWidth(context) / 1.9,
                            decoration: BoxDecoration(boxShadow: [
                              BoxShadow(
                                color: Colors.blueAccent.withOpacity(0.20),
                                spreadRadius: 1,
                                blurRadius: 12,
                                offset: Offset(0, 16), // changes position of shadow
                              ),
                            ]),
                            child: Stack(
                              children: [
                                Positioned(
                                  left: 0,
                                  right: 0,
                                  top: standardSize(context),
                                  bottom: 0,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: AssetImage(
                                                "assets/bg_finish_lesson.jpg"))),
                                  ),
                                ),
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Lottie.asset(
                                    "assets/lottie_nice_job.json",
                                    width: fullWidth(context) / 2,
                                    height: fullWidth(context) / 2,
                                  ),
                                ),
                                Positioned(
                                  top: xlargeSize(context),
                                  child: Column(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: mediumSize(context)),
                                        child: headline3(context, "Great Job",
                                            color: Colors.white),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: xxSmallSize(context)),
                                        child: bodyText2(
                                          context,
                                          "Next lesson",
                                          color: Colors.white,
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(
                                            top: smallSize(context)),
                                        height: fullWidth(context) / 12,
                                        width: fullWidth(context) / 4.6,
                                        decoration: BoxDecoration(
                                            color: theme.accentColor,
                                            borderRadius: BorderRadius.circular(16),
                                            boxShadow: [BoxShadow()]),
                                        child: bodyText2(
                                          context,
                                          "Start",
                                          color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                  left: mediumSize(context),
                                  bottom: mediumSize(context),
                                )
                              ],
                            ),
                          ),
                        ),
                      ]),
                    ),
                  )
              )
          ),
        ));
  }

  Widget lessonItemWidget(Lesson lesson, bool isBusy, int index) {
    return isBusy == true
        ? Container(
            width: fullWidth(context),
            child: Row(
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey.shade300,
                  highlightColor: Colors.grey.shade200,
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: smallSize(context)),
                    width: xxLargeSize(context) / 1.3,
                    height: xxLargeSize(context) / 1.3,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.grey.shade400),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: smallSize(context)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Shimmer.fromColors(
                            baseColor: Colors.grey.shade300,
                            highlightColor: Colors.grey.shade200,
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey.shade300,
                                  borderRadius: BorderRadius.circular(
                                      xxSmallSize(context))),
                              width: xxLargeSize(context),
                              height: mediumSize(context),
                            )),
                        Container(
                          margin: EdgeInsets.only(top: mediumSize(context)),
                          child: Shimmer.fromColors(
                              baseColor: Colors.grey.shade300,
                              highlightColor: Colors.grey.shade200,
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey.shade300,
                                    borderRadius: BorderRadius.circular(
                                        xxSmallSize(context))),
                                width: xxLargeSize(context),
                                height: mediumSize(context),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        : GestureDetector(
            onTap: lesson.opPress,
            child: AnimatedOpacity(
              duration: Duration(milliseconds: 2000),
              opacity: _animateListStep ? 1 : 0,
              curve: Curves.easeInOutQuart,
              child: AnimatedPadding(
                duration: Duration(milliseconds: 2500),
                padding: _animateListStep
                    ? const EdgeInsets.all(4.0)
                    : const EdgeInsets.only(top: 10),
                child: Container(
                  width: fullWidth(context),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: fullWidth(context) / 3.5,
                            child: Stack(
                              children: [
                                Positioned.fill(
                                  child: Container(
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.shade300,
                                            blurRadius: 10,
                                            spreadRadius: 2,
                                          )
                                        ],
                                        border: Border.all(
                                            width: 9,
                                            color: lesson.isDone
                                                ? lesson.color
                                                : Colors.grey.shade200)),
                                  ),
                                ),
                                Positioned(
                                  top: mediumSize(context),
                                  left: mediumSize(context),
                                  right: mediumSize(context),
                                  bottom: mediumSize(context),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(
                                        xxLargeSize(context)),
                                    child: imageWidget(lesson.image,
                                        fit: BoxFit.cover),
                                  ),
                                ),
                                lesson.isDone
                                    ? Align(
                                        alignment: Alignment(0, 1.3),
                                        child: Container(
                                          padding: EdgeInsets.all(
                                              xSmallSize(context)),
                                          child: Container(
                                              width: fullWidth(context) / 26,
                                              height: fullWidth(context) / 26,
                                              child: SvgPicture.asset(
                                                'assets/ic_tick.svg',
                                                color: Colors.white,
                                              )),
                                          decoration: BoxDecoration(
                                              color: lesson.color,
                                              shape: BoxShape.circle),
                                        ),
                                      )
                                    : Container(),
                              ],
                            ),
                            width: fullWidth(context) / 3.5,
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: smallSize(context)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(lesson.title,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                              color: Color(0xff4c456f),
                                              fontSize:
                                                  standardSize(context) / 1.5,
                                              fontWeight: FontWeight.w900)),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: xSmallSize(context)),
                                    child: Row(
                                      children: [
                                        icon((() {
                                          switch (lesson.type) {
                                            case lessonType.video:
                                              return Icons.video_library_sharp;
                                            case lessonType.speaking:
                                              return Icons.chat_rounded;
                                            case lessonType.words:
                                              return Icons.menu_book_outlined;
                                            case lessonType.writing:
                                              return Icons.text_format_sharp;
                                            case lessonType.Exercises:
                                              return Icons.assignment;
                                          }
                                        })()),
                                        text(context, (() {
                                          switch (lesson.type) {
                                            case lessonType.video:
                                              return "Video";
                                            case lessonType.speaking:
                                              return "Speaking";
                                            case lessonType.words:
                                              return "Vocabulary";
                                            case lessonType.writing:
                                              return "Writing";
                                            case lessonType.Exercises:
                                              return "Exercises";
                                          }
                                        })()),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment(0.71, -0.5),
                        child: lesson.isLast == true
                            ? Container()
                            : Container(
                                width: xSmallSize(context),
                                height: fullWidth(context) / 6,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: lesson.isDone
                                        ? lesson.color
                                        : Colors.grey.shade200),
                              ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}

class _SliverAppBarTabDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  _SliverAppBarTabDelegate({required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  double get maxExtent => child.preferredSize.height;

  @override
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
