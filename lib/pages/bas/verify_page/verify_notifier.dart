import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter_language_app/app/locator.dart';
import 'package:flutter_language_app/enums/timer_status.dart';
import 'package:flutter_language_app/pages/bas/verify_page/verify_page.dart';
import 'package:flutter_language_app/services/preferences_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class VerifyVM extends FutureViewModel {
  BuildContext context;
  String phone;

  VerifyVM(this.context, this.phone);

  late int number ;


  // final AppPreferences pref = locator<AppPreferences>();
  final NavigationService navigationService = locator<NavigationService>();

  Timer? timerCode;
  TimerStatus timerStatue = TimerStatus.set;
  Duration _timerDuration = const Duration(minutes: 2, seconds: 0);
  int _restTimerDurationInSeconds = 3 * 60;
  String timerDurationAsString = '2:00';

  void _startTimer({int? seconds}) {
    const oneSecondPeriod = const Duration(seconds: 1);

    if ((timerCode != null) && (timerCode!.isActive)) {
      timerCode!.cancel();
    }

    timerCode = new Timer.periodic(
      oneSecondPeriod,
      (Timer timer) {
        if (timerStatue == TimerStatus.started) _restTimerDurationInSeconds--;
        timerDurationAsString = _durationAsString(
            duration: Duration(seconds: _restTimerDurationInSeconds));
        notifyListeners();

        if (_restTimerDurationInSeconds == 0) {
          timerStatue = TimerStatus.finished;
          timer.cancel();
          notifyListeners();
        }
      },
    );
  }


  String _durationAsString({required Duration duration}) {
    String twoDigitMinutes = duration.inMinutes.remainder(60).toString().padLeft(2,"0");
    String twoDigitSeconds =  duration.inSeconds.remainder(60).toString().padLeft(2,"0");
    return '$twoDigitMinutes : $twoDigitSeconds';
  }



  void controlButtonTimer() {
    switch (timerStatue) {
      case TimerStatus.set:
        _restTimerDurationInSeconds = _timerDuration.inSeconds;
        _startTimer(seconds: _restTimerDurationInSeconds);
        timerStatue = TimerStatus.started;
        timerDurationAsString = _durationAsString(
            duration: Duration(seconds: _restTimerDurationInSeconds));
        notifyListeners();
        break;
      case TimerStatus.finished:

        _restTimerDurationInSeconds = _timerDuration.inSeconds;
        _startTimer(seconds: _restTimerDurationInSeconds);
        timerStatue = TimerStatus.started;
        notifyListeners();
        break;
    }
  }

  Future getDataFromServer() async {
    setBusy(true);
    await Future.delayed(Duration(seconds: 2));
    initUser();
  }

  void initUser() {
    // pref.isFirstTimeLaunch = false;
    // pref.phoneNumber = phone;
    print(phone);
    setBusy(false);
    createUserSheet(context);
  }

  @override
  Future futureToRun() async {
    return controlButtonTimer();
  }
}
