import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/pages/bas/verify_page/verify_notifier.dart';
import 'package:flutter_language_app/pages/bas/verify_page/widgets/create_user_sheet.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/utilities/close_keybored.dart';
import 'package:flutter_language_app/widgets/progress_button.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter_language_app/enums/timer_status.dart';


class VerifyPage extends StatefulWidget {
  final String phoneNumber;

  VerifyPage(this.phoneNumber);

  @override
  State<StatefulWidget> createState() => VerifyPageState();
}

class VerifyPageState extends State<VerifyPage> with TickerProviderStateMixin {
  bool isOpenKeyboard = false;
  bool isCheckCodeNumber = false;
  late double lottieHeight;
  late double formHeight;
  late TextEditingController codeController;
  final _formKey = GlobalKey<FormState>();



  @override
  void initState() {
    super.initState();
    codeController = TextEditingController(text: "");
  }

  @override
  void dispose() {
    super.dispose();
    codeController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    isOpenKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;

    if (!isOpenKeyboard) {
      lottieHeight = fullHeight(context) / 2.5;
      formHeight = fullHeight(context) < 700 ? fullHeight(context) / 2.1 : fullHeight(context) / 2;
    } else {
      lottieHeight = fullHeight(context) / 5;
      formHeight = fullHeight(context) < 700 ? fullHeight(context) / 4.9 : fullHeight(context) / 4.1;
    }
    return ViewModelBuilder<VerifyVM>.reactive(
        viewModelBuilder: () => VerifyVM(context, widget.phoneNumber),
        builder: (context, model, child) => GestureDetector(
              onTap: () => closeKeyboard(context),
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: Scaffold(
                  resizeToAvoidBottomInset: true,
                  backgroundColor: theme.backgroundColor,
                  appBar: AppBar(
                    brightness: Brightness.light,
                    elevation: 0,
                    toolbarHeight: 0,
                  ),
                  body: Container(
                    padding: EdgeInsets.symmetric(horizontal: mediumSize(context)),
                    height: fullHeight(context),
                    child: Stack(children: [
                      AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        child: Lottie.asset(
                          "assets/lottie_confused.json",
                          fit: BoxFit.cover,
                        ),
                        height: lottieHeight,
                        width: fullWidth(context),
                        alignment: Alignment.center,
                      ),
                      AnimatedPositioned(
                        top: formHeight,
                        left: 0,
                        right: 0,
                        duration: Duration(milliseconds: 300),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: smallSize(context)),
                              child: Text(
                                "یه کد چهار رقمی رو برای شماره ی  " + widget.phoneNumber + "  فرستادیم، اونو اینجا وارد کن!",
                                textAlign: TextAlign.start,
                                style: theme.textTheme.bodyText1!.copyWith(
                                    color: theme.accentColor,
                                    fontSize: mediumSize(context) / 1.2),
                              ),
                            ),
                            Form(
                              key: _formKey,
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: largeSize(context)),
                                  child: Directionality(
                                textDirection: TextDirection.ltr,
                                child: PinCodeTextField(
                                  controller: codeController,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return "";
                                    } else if (value.length < 4) {
                                      return "";
                                    }
                                    return null;
                                  },
                                  cursorColor: theme.accentColor,
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.number,
                                  textStyle: theme.textTheme.caption!.copyWith(
                                      color: Color(0xff474747), fontSize: mediumSize(context) / 1.2),
                                  length: 4,
                                  pinTheme: PinTheme(
                                    shape: PinCodeFieldShape.box,
                                    borderRadius: BorderRadius.circular(smallSize(context)),
                                    fieldHeight: fullWidth(context) / 7.8,
                                    fieldWidth: fullWidth(context) / 7.8,
                                    selectedColor: Color(0xff3428ea),
                                    inactiveFillColor: Color(0xfff3f4f9),
                                    selectedFillColor: Color(0xfff3f4f9),
                                    inactiveColor: Colors.transparent,
                                    activeColor: Colors.transparent,
                                    activeFillColor: Color(0xfff3f4f9),
                                  ),
                                  autoDismissKeyboard: true,
                                  enablePinAutofill: true,
                                  showCursor: false,
                                  animationDuration: Duration(milliseconds: 300),
                                  backgroundColor: Colors.transparent,
                                  autovalidateMode: AutovalidateMode.always,
                                  enableActiveFill: true,
                                  appContext: context,
                                  onChanged: (value) {
                                    if(codeController.text.length == 4){
                                      setState(() {
                                        isCheckCodeNumber = true;
                                      });
                                    }
                                  },
                                ),
                              )),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: largeSize(context)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Opacity(
                                    opacity:
                                        model.timerCode!.isActive ? 0.4 : 1,
                                    child: GestureDetector(
                                      onTap: model.timerCode!.isActive
                                          ? null
                                          : () {
                                              if (model.timerStatue ==
                                                  TimerStatus.finished) {
                                                setState(() {
                                                  model.controlButtonTimer();
                                                });
                                              }
                                            },
                                      child: Text(
                                        "ارسال مجدد کد",
                                        style:
                                            theme.textTheme.caption!.copyWith(
                                          color: theme.accentColor,
                                          fontSize: mediumSize(context) / 1.2,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Directionality(
                                    textDirection: TextDirection.ltr,
                                    child: Text(
                                      "${model.timerDurationAsString}",
                                      style: theme.textTheme.caption!
                                          .copyWith(
                                              color: theme.accentColor,
                                              fontSize:
                                                  mediumSize(context) /
                                                      1.2),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          margin:
                              EdgeInsets.only(bottom: standardSize(context)),
                          child: Opacity(
                            opacity: isCheckCodeNumber ? 1 : 0.4,
                            child: progressButton(
                                context, "تایید کد!", model.isBusy,  isCheckCodeNumber ? () {if (_formKey.currentState!.validate()) {model.getDataFromServer();}} : () {}),
                          ),
                        ),
                      ),

                    ]),
                  ),
                ),
              ),
            ));
  }
}


void createUserSheet(BuildContext context) {
  showModalBottomSheet(
    backgroundColor: Colors.white,
    isScrollControlled: true,
    enableDrag: false,
    isDismissible: false,
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(standardSize(context)),
            topRight: Radius.circular(standardSize(context)))),
    builder: (BuildContext context) => CreateUserSheet(),
    context: context,
  );
}
