import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/pages/bas/home/home_page.dart';
import 'package:flutter_language_app/pages/bas/verify_page/verify_page.dart';
import 'package:flutter_language_app/pages/bas/verify_page/widgets/create_user_sheet_notifier.dart';
import 'package:flutter_language_app/pages/create_user/create_account_page.dart';
import 'package:flutter_language_app/pages/create_user/create_user_page.dart';
import 'package:flutter_language_app/pages/create_user/level_english_page.dart';
import 'package:flutter_language_app/pages/create_user/notification_page.dart';
import 'package:flutter_language_app/pages/create_user/time_goal_page.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/progress_button.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stacked/stacked.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class CreateUserSheet extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => CreateUserSheetState();
}

class CreateUserSheetState extends State<CreateUserSheet> {
  late TextEditingController nameController;
  late PageController pageController;

  int nbSteps = 5;

  int selectedStep = 0;

  @override
  void initState() {
    nameController = TextEditingController();
    pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    nameController = TextEditingController();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CreateUserSheetVM>.reactive(
        viewModelBuilder: () => CreateUserSheetVM(context),
        builder: (context, model, child) => FractionallySizedBox(
              heightFactor: 0.9,
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: Container(
                  height: fullHeight(context),
                  width: fullWidth(context),
                  child: Column(
                    children: [
                      AppBar(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(standardSize(context)),
                                topRight:
                                    Radius.circular(standardSize(context)))),
                        backgroundColor: Colors.white,
                        centerTitle: true,
                        title: GestureDetector(
                          onTap: () =>  Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(),)),
                          child: Text(
                            "ایجاد حساب",
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      color: Color(0xff474747),
                                    ),
                          ),
                        ),
                        leading:
                            selectedStep == 0 ? SizedBox() :
                            IconButton(
                          splashRadius: standardSize(context) / 1.2,
                          splashColor: Colors.grey.shade300,
                          icon: SvgPicture.asset(
                            "assets/back.svg",
                            color: Color(0xff474747),
                            height: standardSize(context) / 1.2,
                            width: standardSize(context) / 1.2,
                          ),
                          onPressed: () {
                            pageController.previousPage(
                                duration: Duration(milliseconds: 200),
                                curve: Curves.bounceIn);
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: xSmallSize(context)),
                        child: StepProgressIndicator(
                          totalSteps: nbSteps,
                          currentStep: selectedStep,
                          unselectedColor: Color(0xffEAEAEA),
                          roundedEdges: Radius.circular(smallSize(context)),
                          selectedGradientColor: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              Color(0xff3428ea),
                              Color(0xffa573ff),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: PageView(
                          controller: pageController,
                          physics: NeverScrollableScrollPhysics(),
                          onPageChanged: (value) {
                            setState(() {
                              selectedStep = value;
                            });
                          },
                          children: [
                            CreateUserPage(),
                            LevelEnglish(),
                            TimeGoalPage(),
                            NotificationPage(),
                            CreateAccount(),
                          ],
                        ),
                      ),
                      PreferredSize(
                          child: Container(
                            margin: EdgeInsets.only(bottom: standardSize(context),left: standardSize(context),right: standardSize(context)),
                            child: progressButton(context, "بعدی!", false, () {
                              pageController.animateToPage(selectedStep + 1, duration: Duration(milliseconds: 200), curve: Curves.bounceIn);


                            }),
                          ),
                          preferredSize: Size(
                              fullWidth(context), kBottomNavigationBarHeight))
                    ],
                  ),
                ),
              ),
            ));
  }
}
