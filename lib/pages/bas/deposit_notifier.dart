import 'package:stacked/stacked.dart';

class DepositVM extends BaseViewModel {

  int _isSelectEasyLevel = 0;

  int get isSelectEasyLevel => _isSelectEasyLevel;

  // bool _isSelectMediumLevel = false;
  //
  // bool get isSelectMediumLevel => _isSelectMediumLevel;
  //
  // bool _isSelectHardLevel = false;
  //
  // bool get isSelectHardLevel => _isSelectHardLevel;
  //
  // bool _isSelectSoHardLevel = false;
  //
  // bool get isSelectSoHardLevel => _isSelectSoHardLevel;


  void setSelectedEasyLevel(int selected) {
    _isSelectEasyLevel = selected;
    notifyListeners();
  }

  // void setSelectedMediumLevel(bool selected) {
  //   _isSelectMediumLevel = selected;
  //   notifyListeners();
  // }
  //
  // void setSelectedHardLevel(bool selected) {
  //   _isSelectHardLevel = selected;
  //   notifyListeners();
  // }
  //
  // void setSelectedSoHardLevel(bool selected) {
  //   _isSelectSoHardLevel = selected;
  //   notifyListeners();
  // }

}