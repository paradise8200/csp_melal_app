import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_language_app/entities/language.dart';
import 'package:flutter_language_app/fakeData.dart';
import 'package:flutter_language_app/models/cart_box.dart';
import 'package:flutter_language_app/models/category_model.dart';
import 'package:flutter_language_app/pages/bas/home/home_notifier.dart';
import 'package:flutter_language_app/pages/bas/lesson_page/lesson_page.dart';
import 'package:flutter_language_app/pages/bas/lesson_page/lesson_page_part_first.dart';
import 'package:flutter_language_app/pages/create_user/create_account_page.dart';
import 'package:flutter_language_app/pages/listening_page/homee.dart';
import 'package:flutter_language_app/pages/listening_page/listening_page.dart';
import 'package:flutter_language_app/pages/profile_page/profile_page.dart';
import 'package:flutter_language_app/pages/quiz/new_word_quiz_page.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/theme/text_widgets.dart';
import 'package:flutter_language_app/widgets/action_widgets.dart';
import 'package:flutter_language_app/widgets/cupertinoContext.dart';
import 'package:flutter_language_app/widgets/empty_app_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';
import 'package:ndialog/ndialog.dart';
import 'package:shimmer/shimmer.dart';
import 'package:stacked/stacked.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomePage> with TickerProviderStateMixin {
  late AnimationController _animationControllerHeader;
  late Animation<double> _animationHeader;
  late CurvedAnimation _curveHeader;

  late AnimationController _animationControllerTitleCourse;
  late Animation<double> _animationTitleCourse;
  late CurvedAnimation _curveTitleCourse;

  late AnimationController _animationControllerTitleLesson;
  late Animation<double> _animationTitleLesson;
  late CurvedAnimation _curveTitleLesson;

  late AnimationController _controllerBanner = AnimationController(
    duration: const Duration(milliseconds: 2000),
    vsync: this,
  )..forward();
  late Animation<Offset> _offsetAnimationBanner = Tween<Offset>(
    begin: const Offset(1.5, 0.0),
    end: Offset.zero,
  ).animate(CurvedAnimation(
    parent: _controllerBanner,
    curve: Curves.decelerate,
  ));

  bool _animateCourseCard = false;
  static bool _isStartCourseCard = true;

  bool _animateLessonCard = false;
  static bool _isStartLessonCard = true;

  @override
  void dispose() {
    super.dispose();
    _controllerBanner.dispose();
  }

  String avatar = "assets/character_gril.png";

  void configLoading() {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 2000)
      ..indicatorType = EasyLoadingIndicatorType.circle
      ..loadingStyle = EasyLoadingStyle.light
      ..indicatorSize = 45.0
      ..radius = 10.0
      ..progressColor = Colors.yellow
      ..backgroundColor = Colors.white
      ..indicatorColor = Colors.yellow
      ..textColor = Colors.yellow
      ..maskColor = Colors.blue.withOpacity(0.5)
      ..userInteractions = true
      ..dismissOnTap = false;
  }

  @override
  void initState() {
    configLoading();
    _animationControllerHeader = AnimationController(
      duration: Duration(milliseconds: 3000),
      vsync: this,
    );

    _animationControllerTitleCourse = AnimationController(
      duration: Duration(milliseconds: 3000),
      vsync: this,
    );

    _animationControllerTitleLesson = AnimationController(
      duration: Duration(milliseconds: 3000),
      vsync: this,
    );

    _curveHeader = CurvedAnimation(
      parent: _animationControllerHeader,
      curve: Interval(
        0.5,
        1.0,
        curve: Curves.fastOutSlowIn,
      ),
    );

    _curveTitleCourse = CurvedAnimation(
      parent: _animationControllerTitleCourse,
      curve: Interval(
        0.5,
        1.0,
        curve: Curves.fastOutSlowIn,
      ),
    );

    _curveTitleLesson = CurvedAnimation(
      parent: _animationControllerTitleLesson,
      curve: Interval(
        0.5,
        1.0,
        curve: Curves.fastOutSlowIn,
      ),
    );

    _animationHeader = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_curveHeader);

    _animationTitleCourse = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_curveTitleCourse);

    _animationTitleLesson = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_curveTitleLesson);

    Future.delayed(
      Duration(milliseconds: 1500),
      () => _animationControllerHeader.forward(),
    );

    Future.delayed(
      Duration(milliseconds: 1500),
      () => _animationControllerTitleCourse.forward(),
    );

    Future.delayed(
      Duration(milliseconds: 1500),
      () => _animationControllerTitleLesson.forward(),
    );

    _isStartCourseCard
        ? Future.delayed(Duration(milliseconds: 3500), () {
            setState(() {
              _animateCourseCard = true;
              _isStartCourseCard = false;
            });
          })
        : _animateCourseCard = true;

    _isStartLessonCard
        ? Future.delayed(Duration(milliseconds: 4000), () {
            setState(() {
              _animateLessonCard = true;
              _isStartLessonCard = false;
            });
          })
        : _animateLessonCard = true;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return  Directionality(
            textDirection: TextDirection.rtl,
            child: Scaffold(
                backgroundColor: theme.backgroundColor,
                appBar: emptyAppBar(),
                body: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        showModalBottomSheet(
                                            context: context,
                                            isScrollControlled: true,
                                            backgroundColor: Colors.transparent,
                                            builder: (BuildContext bc) {
                                              return Container(
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      topRight: Radius.circular(
                                                          standardSize(context)),
                                                      topLeft: Radius.circular(
                                                          standardSize(context)),
                                                    ),
                                                    color: Color(0xffFAFAFA)),
                                                child: Column(
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: [
                                                    Container(width: fullWidth(context)/8.5,
                                                    margin: EdgeInsets.only(top: smallSize(context)),
                                                    height: xxSmallSize(context),
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(mediumSize(context)),
                                                        color: Color(0xff474747).withOpacity(0.10)
                                                      ),
                                                    ),
                                                    Container(
                                                      padding: EdgeInsets.all(
                                                          smallSize(context)),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Container(
                                                            margin: EdgeInsets.only(left: smallSize(context)),

                                                            width: iconSizeLarge(
                                                                context),
                                                            height: iconSizeLarge(
                                                                context),
                                                            child:
                                                                SvgPicture.asset(
                                                              "assets/tick.svg",
                                                              height:
                                                                  iconSizeLarge(
                                                                      context),
                                                              width:
                                                                  iconSizeLarge(
                                                                      context),
                                                            ),
                                                          ),
                                                          Row(
                                                            children: [
                                                              Container(
                                                                  margin: EdgeInsets.symmetric(
                                                                      horizontal:
                                                                          mediumSize(
                                                                              context)),
                                                                  child: Text(
                                                                      "لغت انگلیسی 504",
                                                                      style: theme
                                                                          .textTheme.bodyText1!
                                                                          .copyWith(
                                                                          color:
                                                                          Color(0xff474747),
                                                                          fontSize: standardSize(
                                                                              context) /
                                                                              1.5,
                                                                          fontWeight:
                                                                          FontWeight.w900)
                                                                  )



                                                              ),
                                                              Container(
                                                                height: fullWidth(context)/6.25,
                                                                width:  fullWidth(context)/6.25,

                                                                decoration: BoxDecoration(
                                                                  shape: BoxShape.circle,
                                                                    image: DecorationImage(image: AssetImage("assets/image_sheet_first.png"),
                                                                      fit: BoxFit.cover
                                                                    ),

                                                                        )
                                                                ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(width: fullWidth(context),
                                                      margin: EdgeInsets.only(top: smallSize(context)),
                                                      height: 1,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.circular(mediumSize(context)),
                                                          color: Color(0xffEEEEEE)
                                                      ),
                                                    ),

                                                    Container(
                                                      padding: EdgeInsets.all(
                                                          smallSize(context)),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Expanded(
                                                              child: SizedBox()),
                                                          Row(
                                                            children: [
                                                              Container(
                                                                  child: Text(
                                                                      "صفر تا صد زبان انگلیسی",
                                                                      style: theme
                                                                          .textTheme.bodyText1!
                                                                          .copyWith(
                                                                          color:
                                                                          Color(0xff474747),
                                                                          fontSize: standardSize(
                                                                              context) /
                                                                              1.5,
                                                                          fontWeight:
                                                                          FontWeight.w900))),
                                                              Container(
                                                                  height: fullWidth(context)/6.25,
                                                                  width:  fullWidth(context)/6.25,

                                                                  decoration: BoxDecoration(
                                                                    shape: BoxShape.circle,
                                                                    image: DecorationImage(image: AssetImage("assets/image_sheet_second.png"),
                                                                        fit: BoxFit.cover
                                                                    ),

                                                                  )
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Container(width: fullWidth(context),
                                                      margin: EdgeInsets.only(top: smallSize(context)),
                                                      height: 1,
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.circular(mediumSize(context)),
                                                          color: Color(0xffEEEEEE)
                                                      ),
                                                    ),

                                                    Container(
                                                      padding: EdgeInsets.all(
                                                          smallSize(context)),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Expanded(
                                                              child: SizedBox()),
                                                          Row(
                                                            children: [
                                                              Container(
                                                                  child: Text(
                                                                      "افزوده دوره جدید",
                                                                      style: theme
                                                                          .textTheme.bodyText1!
                                                                          .copyWith(
                                                                          color:
                                                                          Color(0xff474747),
                                                                          fontSize: standardSize(
                                                                              context) /
                                                                              1.5,
                                                                          fontWeight:
                                                                          FontWeight.w900)
                                                                  )),
                                                              Container(
                                                                  height: fullWidth(context)/6.25,
                                                                  width:  fullWidth(context)/6.25,

                                                                  decoration: BoxDecoration(
                                                                    shape: BoxShape.circle,
                                                                    image: DecorationImage(image: AssetImage("assets/add.png"),
                                                                        fit: BoxFit.cover
                                                                    ),

                                                                  )
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            });
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.symmetric(horizontal: xxSmallSize(context)),
                                            child: Text("صفر تا صد زبان انگلیسی",
                                                style: theme
                                                    .textTheme
                                                    .bodyText1!
                                                    .copyWith(
                                                    color: Color(0xff474747),

                                                    fontSize: standardSize(context) /
                                                        1.5,
                                                    fontWeight:
                                                    FontWeight.w900)),
                                          ),
                                          Icon(Icons.keyboard_arrow_down,
                                            color: Color(0xff474747),
                                          ),
                                        ],
                                      ),
                                    ),
                                    ScaleTransition(
                                      scale: _animationHeader,
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: mediumSize(context),
                                            left: mediumSize(context),
                                            right: mediumSize(context),
                                            bottom: standardSize(context)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      bottom:
                                                          xxSmallSize(context)),
                                                  child: Text(
                                                    "صبح بخیر  🌞",
                                                    style: theme
                                                        .textTheme.bodyText1!
                                                        .copyWith(
                                                            color: theme
                                                                .primaryColor,
                                                            fontSize:
                                                                standardSize(
                                                                        context) /
                                                                    1.3,
                                                            fontWeight:
                                                                FontWeight.w900),
                                                  ),
                                                ),
                                                Container(
                                                  child: Text(
                                                    "نازنین بیاتی",
                                                    // (model.data?...0name != '' ? model.data!.name : "اطلاعات وارد نشده است !"),
                                                    style: theme
                                                        .textTheme.caption!
                                                        .copyWith(
                                                      color: Color(0xff474747),
                                                      fontSize:
                                                          mediumSize(context) /
                                                              1.2,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            CupertinoContextMenuCustom(
                                              previewSize: Size(
                                                  fullWidth(context) / 5,
                                                  fullWidth(context) / 5),
                                              previewBuilder:
                                                  (BuildContext context,
                                                      Animation<double> animation,
                                                      Widget child) {
                                                return Container(
                                                  width: fullWidth(context) / 7.2,
                                                  height:
                                                      fullWidth(context) / 7.2,
                                                  child: CircleAvatar(
                                                    child: Image.asset(
                                                      avatar,
                                                      width: largeSize(context) /
                                                          0.85,
                                                      height: largeSize(context) /
                                                          0.85,
                                                    ),
                                                    backgroundColor:
                                                        Color(0xfff3f4f9),
                                                  ),
                                                );
                                              },
                                              actions: [
                                                CupertinoContextMenuAction(
                                                    onPressed: () async {
                                                      Navigator.of(context,
                                                              rootNavigator: true)
                                                          .pop();
                                                      setState(() {
                                                        avatar =
                                                            "assets/character_boy.png";
                                                      });
                                                      await CustomProgressDialog
                                                          .future(
                                                        context,
                                                        blur: 3,
                                                        dismissable: false,
                                                        future: Future.delayed(
                                                            Duration(seconds: 10),
                                                            () {
                                                          return "WOHOOO";
                                                        }),
                                                        loadingWidget: Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(12),
                                                            color: Colors
                                                                .grey.shade200,
                                                          ),
                                                          height: 100,
                                                          width: 100,
                                                          child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Container(
                                                                width: 60,
                                                                height: 60,
                                                                child:
                                                                    ColorFiltered(
                                                                  colorFilter:
                                                                      ColorFilter.mode(
                                                                          Colors
                                                                              .black,
                                                                          BlendMode
                                                                              .srcIn),
                                                                  child: LottieBuilder
                                                                      .asset(
                                                                          'assets/lottie_loading.json'),
                                                                ),
                                                              ),
                                                              Text(
                                                                "صبر کن...",
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                textDirection:
                                                                    TextDirection
                                                                        .rtl,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .caption,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                    child: Container(
                                                      height:
                                                          fullWidth(context) / 9,
                                                      child: Directionality(
                                                        textDirection:
                                                            TextDirection.rtl,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                              width: fullWidth(
                                                                      context) /
                                                                  7.2,
                                                              height: fullWidth(
                                                                      context) /
                                                                  7.2,
                                                              decoration:
                                                                  BoxDecoration(
                                                                shape: BoxShape
                                                                    .circle,
                                                                boxShadow: [
                                                                  BoxShadow(
                                                                      color: Colors
                                                                          .grey
                                                                          .shade300,
                                                                      blurRadius:
                                                                          4,
                                                                      offset: Offset(
                                                                          0,
                                                                          4) // changes position of shadow
                                                                      ),
                                                                ],
                                                              ),
                                                              child: CircleAvatar(
                                                                child:
                                                                    Image.asset(
                                                                  "assets/character_boy.png",
                                                                  width: largeSize(
                                                                      context),
                                                                  height:
                                                                      largeSize(
                                                                          context),
                                                                ),
                                                                backgroundColor:
                                                                    Color(
                                                                        0xfff3f4f9),
                                                              ),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets.only(
                                                                  right:
                                                                      xxSmallSize(
                                                                          context)),
                                                              child: Text(
                                                                "فرهاد بیاتی",
                                                                style: theme.textTheme.caption!.copyWith(
                                                                    color: Color(
                                                                        0xff474747),
                                                                    fontSize:
                                                                        mediumSize(
                                                                                context) /
                                                                            1.2,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                              ),
                                                            ),
                                                            Expanded(
                                                                child:
                                                                    SizedBox()),
                                                            Container(
                                                              margin: EdgeInsets.only(
                                                                  left: xxSmallSize(
                                                                      context)),
                                                              child: SvgPicture
                                                                  .asset(
                                                                "assets/tick.svg",
                                                                width: fullWidth(
                                                                        context) /
                                                                    16,
                                                                height: fullWidth(
                                                                        context) /
                                                                    16,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    )),
                                                CupertinoContextMenuAction(
                                                    onPressed: () async {
                                                      Navigator.of(context,
                                                              rootNavigator: true)
                                                          .pop();

                                                      setState(() {
                                                        avatar =
                                                            "assets/character_boy.png";
                                                      });

                                                      await CustomProgressDialog
                                                          .future(
                                                        context,
                                                        blur: 3,
                                                        dismissable: false,
                                                        future: Future.delayed(
                                                            Duration(seconds: 10),
                                                            () {
                                                          return "WOHOOO";
                                                        }),
                                                        loadingWidget: Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(12),
                                                            color: Colors
                                                                .grey.shade200,
                                                          ),
                                                          height: 100,
                                                          width: 100,
                                                          child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Container(
                                                                width: 60,
                                                                height: 60,
                                                                child:
                                                                    ColorFiltered(
                                                                  colorFilter:
                                                                      ColorFilter.mode(
                                                                          Colors
                                                                              .black,
                                                                          BlendMode
                                                                              .srcIn),
                                                                  child: LottieBuilder
                                                                      .asset(
                                                                          'assets/lottie_loading.json'),
                                                                ),
                                                              ),
                                                              Text(
                                                                "صبر کن...",
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                textDirection:
                                                                    TextDirection
                                                                        .rtl,
                                                                style: Theme.of(
                                                                        context)
                                                                    .textTheme
                                                                    .caption,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                    child: Container(
                                                      height:
                                                          fullWidth(context) / 9,
                                                      child: Directionality(
                                                        textDirection:
                                                            TextDirection.rtl,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                              width: fullWidth(
                                                                      context) /
                                                                  7.2,
                                                              height: fullWidth(
                                                                      context) /
                                                                  7.2,
                                                              decoration:
                                                                  BoxDecoration(
                                                                shape: BoxShape
                                                                    .circle,
                                                                boxShadow: [
                                                                  BoxShadow(
                                                                      color: Colors
                                                                          .grey
                                                                          .shade300,
                                                                      blurRadius:
                                                                          4,
                                                                      offset: Offset(
                                                                          0,
                                                                          4) // changes position of shadow
                                                                      ),
                                                                ],
                                                              ),
                                                              child: CircleAvatar(
                                                                child:
                                                                    Image.asset(
                                                                  "assets/character_gril.png",
                                                                  width: largeSize(
                                                                      context),
                                                                  height:
                                                                      largeSize(
                                                                          context),
                                                                ),
                                                                backgroundColor:
                                                                    Color(
                                                                        0xfff3f4f9),
                                                              ),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets.only(
                                                                  right:
                                                                      xxSmallSize(
                                                                          context)),
                                                              child: Text(
                                                                "نازنین بیاتی",
                                                                style: theme.textTheme.caption!.copyWith(
                                                                    color: Color(
                                                                        0xff474747),
                                                                    fontSize:
                                                                        mediumSize(
                                                                                context) /
                                                                            1.2,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    )),
                                              ],
                                              child: GestureDetector(
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            ProfilePage(),
                                                      ));
                                                },
                                                child: Container(
                                                  width: fullWidth(context) / 7.2,
                                                  height:
                                                      fullWidth(context) / 7.2,
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    boxShadow: [
                                                      BoxShadow(
                                                          color: Colors
                                                              .grey.shade300,
                                                          blurRadius: 4,
                                                          offset: Offset(0,
                                                              4) // changes position of shadow
                                                          ),
                                                    ],
                                                  ),
                                                  child: CircleAvatar(
                                                    child: Image.asset(
                                                      avatar,
                                                      width: largeSize(context) /
                                                          0.85,
                                                      height: largeSize(context) /
                                                          0.85,
                                                    ),
                                                    backgroundColor:
                                                        Color(0xfff3f4f9),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    SlideTransition(
                                        position: _offsetAnimationBanner,
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      ListeningPage(),
                                                ));
                                          },
                                          child: Container(
                                            width: fullWidth(context),
                                            height: fullHeight(context) / 4,
                                            child: Stack(
                                              children: [
                                                Positioned(
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                      left: mediumSize(context),
                                                      right: mediumSize(context),
                                                      top: xSmallSize(context),
                                                      bottom: xSmallSize(context),
                                                    ),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              mediumSize(
                                                                  context)),
                                                      image: DecorationImage(
                                                        fit: BoxFit.cover,
                                                        image: AssetImage(
                                                            "assets/bg_category_card_yellow.jpg"),
                                                      ),
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color:
                                                              Color(0xfff8d56d),
                                                          spreadRadius: 1,
                                                          blurRadius: 8,
                                                          offset: Offset(0,
                                                              2), // changes position of shadow
                                                        ),
                                                      ],
                                                    ),
                                                    child: Stack(
                                                      children: [
                                                        Positioned(
                                                          right: standardSize(
                                                                  context) /
                                                              0.9,
                                                          top: largeSize(
                                                              context),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Container(

                                                                child: Text(
                                                                    "دوره تخصصی\nزبان انگلیسی",
                                                                    style: theme
                                                                        .textTheme
                                                                        .bodyText1!
                                                                        .copyWith(
                                                                            color: Colors
                                                                                .white,
                                                                            shadows: [
                                                                              BoxShadow(
                                                                                color: Colors.grey.withOpacity(1),
                                                                                spreadRadius: 5,
                                                                                blurRadius: 7,
                                                                                offset: Offset(0, 3), // changes position of shadow
                                                                              ),
                                                                            ],
                                                                            fontSize: standardSize(context) /
                                                                                1.5,
                                                                            fontWeight:
                                                                                FontWeight.w900)),
                                                              ),
                                                              Container(
                                                                margin: EdgeInsets.only(
                                                                    top: smallSize(
                                                                        context),
                                                                    right: xSmallSize(
                                                                        context)),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              xSmallSize(context)),
                                                                  boxShadow: [
                                                                    BoxShadow(
                                                                        color: Color(
                                                                            0xff4c456f),
                                                                        blurRadius:
                                                                            0,
                                                                        spreadRadius:
                                                                            1.2,
                                                                        offset:
                                                                            Offset(
                                                                                4,
                                                                                5))
                                                                  ],
                                                                  color: theme
                                                                      .backgroundColor,
                                                                ),
                                                                padding: EdgeInsets
                                                                    .symmetric(
                                                                        horizontal:
                                                                            xSmallSize(
                                                                                context),
                                                                        vertical:
                                                                            4),
                                                                child: Text(
                                                                    'بیشتر بدانید!',
                                                                    style: theme
                                                                        .textTheme
                                                                        .caption!
                                                                        .copyWith(
                                                                      color: Color(
                                                                          0xff474747),
                                                                      fontSize:
                                                                          mediumSize(context) /
                                                                              1.5,
                                                                    )),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )),
                                    ScaleTransition(
                                      scale: _animationTitleCourse,
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: mediumSize(context),
                                            left: mediumSize(context),
                                            right: mediumSize(context),
                                            bottom: standardSize(context)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              child: Text("دوره های تخصصی",
                                                  style: theme
                                                      .textTheme.bodyText1!
                                                      .copyWith(
                                                          color:
                                                              Color(0xff474747),
                                                          fontSize: standardSize(
                                                                  context) /
                                                              1.5,
                                                          fontWeight:
                                                              FontWeight.w900)),
                                            ),
                                            GestureDetector(
                                              // onTap: (){
                                              //   Navigator.push(context, MaterialPageRoute(builder: (context)=>
                                              //   NewWordQuizPage()));
                                              // },
                                              child: Container(
                                                child: Text(
                                                  "مشاهده همه!",
                                                  style: theme.textTheme.caption!
                                                      .copyWith(
                                                    color: Color(0xff474747),
                                                    fontSize:
                                                        mediumSize(context) / 1.2,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                        width: fullWidth(context),
                                        height: fullHeight(context) / 6,
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          itemCount: categoryList().length,
                                          physics: BouncingScrollPhysics(),
                                          scrollDirection: Axis.horizontal,
                                          padding: EdgeInsets.only(
                                              right: mediumSize(context)),
                                          itemBuilder: (context, index) =>
                                              categoryCard(categoryList()[index],
                                                  context, false),
                                        )
                                        // PageView.builder(
                                        //     pageSnapping: true,
                                        //     physics: BouncingScrollPhysics(),
                                        //     itemCount: categoryList().length,
                                        //     scrollDirection: Axis.horizontal,
                                        //     controller: PageController(initialPage: 1, viewportFraction: 0.85,),
                                        //     itemBuilder: (context, index) =>
                                        //         categoryCard(categoryList()[index],
                                        //             context, false)),
                                        ),
                                    ScaleTransition(
                                      scale: _animationTitleLesson,
                                      child: Container(
                                        margin: EdgeInsets.only(
                                            top: mediumSize(context),
                                            left: mediumSize(context),
                                            right: mediumSize(context),
                                            bottom: mediumSize(context)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: mediumSize(context)),
                                              child: Text("درس ها",
                                                  style: theme
                                                      .textTheme.bodyText1!
                                                      .copyWith(
                                                          color:
                                                              Color(0xff474747),
                                                          fontSize: standardSize(
                                                                  context) /
                                                              1.5,
                                                          fontWeight:
                                                              FontWeight.w900)),
                                            ),
                                            Container(
                                              child: Text(
                                                "مشاهده همه!",
                                                style: theme.textTheme.caption!
                                                    .copyWith(
                                                  color: Color(0xff474747),
                                                  fontSize:
                                                      mediumSize(context) / 1.2,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: ListView.builder(
                                        padding: EdgeInsets.only(
                                            right: mediumSize(context),
                                            left: mediumSize(context)),
                                        physics: BouncingScrollPhysics(),
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount: homeLessonList().length,
                                        itemBuilder: (context, index) =>
                                            lessonBox(homeLessonList()[index],
                                                context, ),
                                      ),
                                      // child: AnimatedListItem(this.index, key: key),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                                    top: fullWidth(context)/3.3,
                                // bottom: 0,
                                // right: 0,
                                left: largeSize(context),

                                // alignment: Alignment.center,
                                child: Container(

                                  child: Image.asset(
                                    "assets/pic_school_home.png",
                                    scale: 4,
                                  ),
                                )),

                          ],
                        ))));
  }

  // Widget homePageShimmer(BuildContext context, bool isBusy) {
  //   var theme = Theme.of(context);
  //   return Scaffold(
  //       backgroundColor: theme.backgroundColor,
  //       body: SingleChildScrollView(
  //           physics: BouncingScrollPhysics(),
  //           child: Container(
  //             margin: EdgeInsets.only(top: xlargeSize(context)),
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.start,
  //               children: [
  //                 Container(
  //                   margin:
  //                       EdgeInsets.symmetric(horizontal: standardSize(context)),
  //                   child: Row(
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: [
  //                       Column(
  //                         crossAxisAlignment: CrossAxisAlignment.start,
  //                         children: [
  //                           IconButton(
  //                             icon: Icon(Icons.access_alarm),
  //                             onPressed: () {},
  //                             visualDensity: VisualDensity(
  //                                 horizontal: -4.0, vertical: -4.0),
  //                             padding: EdgeInsets.zero,
  //                           ),
  //                           Container(
  //                               margin:
  //                                   EdgeInsets.only(right: xSmallSize(context)),
  //                               child: Shimmer.fromColors(
  //                                   baseColor: Colors.grey.shade300,
  //                                   highlightColor: Colors.grey.shade200,
  //                                   child: Container(
  //                                     decoration: BoxDecoration(
  //                                         color: Colors.grey.shade300,
  //                                         borderRadius: BorderRadius.circular(
  //                                             xxSmallSize(context))),
  //                                     width: xxLargeSize(context),
  //                                     height: smallSize(context),
  //                                   ))),
  //                           Container(
  //                               margin: EdgeInsets.only(
  //                                   top: xSmallSize(context),
  //                                   right: xSmallSize(context)),
  //                               child: Shimmer.fromColors(
  //                                   baseColor: Colors.grey.shade300,
  //                                   highlightColor: Colors.grey.shade200,
  //                                   child: Container(
  //                                     decoration: BoxDecoration(
  //                                         color: Colors.grey.shade300,
  //                                         borderRadius: BorderRadius.circular(
  //                                             xxSmallSize(context))),
  //                                     width: xxLargeSize(context) / 1.5,
  //                                     height: smallSize(context),
  //                                   ))),
  //                         ],
  //                       ),
  //                       Shimmer.fromColors(
  //                         baseColor: Colors.grey.shade300,
  //                         highlightColor: Colors.grey.shade200,
  //                         child: Container(
  //                             decoration: BoxDecoration(
  //                                 color: Colors.grey.shade300,
  //                                 shape: BoxShape.circle),
  //                             width: fullWidth(context) / 7.5,
  //                             height: fullWidth(context) / 7.5),
  //                       )
  //                     ],
  //                   ),
  //                 ),
  //                 Shimmer.fromColors(
  //                   baseColor: Colors.grey.shade300,
  //                   highlightColor: Colors.grey.shade200,
  //                   child: Container(
  //                     margin: EdgeInsets.symmetric(
  //                         horizontal: standardSize(context),
  //                         vertical: standardSize(context)),
  //                     decoration: BoxDecoration(
  //                         color: Colors.grey.shade300,
  //                         borderRadius: BorderRadius.circular(16)),
  //                     width: fullWidth(context),
  //                     height: fullHeight(context) / 4.5,
  //                   ),
  //                 ),
  //                 Container(
  //                   margin: EdgeInsets.symmetric(
  //                       vertical: xSmallSize(context),
  //                       horizontal: standardSize(context)),
  //                   child: Row(
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: [
  //                       Shimmer.fromColors(
  //                           baseColor: Colors.grey.shade300,
  //                           highlightColor: Colors.grey.shade200,
  //                           child: Container(
  //                             decoration: BoxDecoration(
  //                                 color: Colors.grey.shade300,
  //                                 borderRadius: BorderRadius.circular(
  //                                     xxSmallSize(context))),
  //                             width: fullWidth(context) / 2.3,
  //                             height: smallSize(context),
  //                           )),
  //                       Shimmer.fromColors(
  //                           baseColor: Colors.grey.shade300,
  //                           highlightColor: Colors.grey.shade200,
  //                           child: Container(
  //                             decoration: BoxDecoration(
  //                                 color: Colors.grey.shade300,
  //                                 borderRadius: BorderRadius.circular(
  //                                     xxSmallSize(context))),
  //                             width: xxLargeSize(context) / 1.5,
  //                             height: smallSize(context),
  //                           ))
  //                     ],
  //                   ),
  //                 ),
  //                 Container(
  //                   margin: EdgeInsets.symmetric(
  //                       vertical: xSmallSize(context),
  //                       horizontal: standardSize(context)),
  //                   child: Row(
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: [
  //                       Shimmer.fromColors(
  //                           baseColor: Colors.grey.shade300,
  //                           highlightColor: Colors.grey.shade200,
  //                           child: Container(
  //                             decoration: BoxDecoration(
  //                                 color: Colors.grey.shade300,
  //                                 borderRadius: BorderRadius.circular(
  //                                     xxSmallSize(context))),
  //                             width: fullWidth(context) / 2.3,
  //                             height: smallSize(context),
  //                           )),
  //                       Shimmer.fromColors(
  //                           baseColor: Colors.grey.shade300,
  //                           highlightColor: Colors.grey.shade200,
  //                           child: Container(
  //                             decoration: BoxDecoration(
  //                                 color: Colors.grey.shade300,
  //                                 borderRadius: BorderRadius.circular(
  //                                     xxSmallSize(context))),
  //                             width: xxLargeSize(context) / 1.5,
  //                             height: smallSize(context),
  //                           ))
  //                     ],
  //                   ),
  //                 ),
  //
  //               ],
  //             ),
  //           )));
  // }

  Widget categoryCard(
      CategoryModel category, BuildContext context, bool isBusy) {
    var theme = Theme.of(context);
    return isBusy
        ? Shimmer.fromColors(
            baseColor: Colors.grey.shade300,
            highlightColor: Colors.grey.shade200,
            child: Container(
                margin: EdgeInsets.symmetric(
                    vertical: mediumSize(context),
                    horizontal: xSmallSize(context)),
                width: fullWidth(context) / 2.4,
                height: fullHeight(context) / 3,
                decoration: BoxDecoration(
                    color: Colors.grey.shade300,
                    borderRadius: BorderRadius.circular(smallSize(context)))),
          )
        : AnimatedOpacity(
            duration: Duration(milliseconds: 2000),
            opacity: _animateCourseCard ? 1 : 0,
            curve: Curves.decelerate,
            child: Container(
              width: fullWidth(context) / 1.3,
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Container(
                      margin: EdgeInsets.only(
                        left: category.title.contains("دوره پیشرفته")
                            ? 0
                            : standardSize(context),
                        // right: smallSize(context),
                        top: 6,
                        bottom: xSmallSize(context),
                      ),
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(mediumSize(context)),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(category.backgroundImage),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: category.shadowColor,
                            spreadRadius: 1,
                            blurRadius: 8,
                            offset: Offset(0, 2), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                              right: standardSize(context),
                              top: mediumSize(context),
                              child: Container(
                                child: Text(
                                  category.title,
                                  style: theme.textTheme.bodyText1!.copyWith(
                                      color: Colors.white,
                                      shadows: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(1),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                      fontSize: standardSize(context) / 1.5,
                                      fontWeight: FontWeight.w900),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ),
                  Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        margin: EdgeInsets.only(
                          left: smallSize(context),
                        ),
                        child: Image.asset(
                          category.image,
                        ),
                        width: fullWidth(context) / 6,
                        height: fullWidth(context) / 6,
                      ))
                ],
              ),
            ),
          );
  }

  Widget lessonBox(
      CartBoxModel cartBoxModel, BuildContext context) {
    var theme = Theme.of(context);
    return
      // isBusy
      //   ? Shimmer.fromColors(
      //       enabled: true,
      //       baseColor: Colors.grey.shade300,
      //       highlightColor: Colors.grey.shade200,
      //       child: Container(
      //         margin: EdgeInsets.symmetric(vertical: mediumSize(context)),
      //         height: fullHeight(context) / 7,
      //         width: fullWidth(context),
      //         decoration: BoxDecoration(
      //           color: Colors.grey.shade300,
      //           borderRadius: BorderRadius.circular(smallSize(context)),
      //         ),
      //       ))
      //   :
      GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LessonPage(cartBoxModel.textTitle)));
            },
            child: AnimatedOpacity(
              duration: Duration(milliseconds: 2000),
              opacity: _animateLessonCard ? 1 : 0,
              curve: Curves.easeInOutQuart,
              child: Container(
                margin: EdgeInsets.only(bottom: largeSize(context) / 0.8),
                decoration: BoxDecoration(
                  color: theme.backgroundColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(fullWidth(context) / 1.8),
                      bottomLeft: Radius.circular(fullWidth(context) / 1.8),
                      bottomRight: Radius.circular(fullWidth(context)),
                      topRight: Radius.circular(fullWidth(context))),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 7,
                      offset: Offset(0, 6), // changes position of shadow
                    ),
                  ],
                ),
                height: fullHeight(context) / 7,
                width: fullWidth(context),
                child: Stack(
                  children: [
                    Positioned(
                      right: 0,
                      bottom: 0,
                      top: 0,
                      child: Container(
                        height: fullHeight(context) / 7.2,
                        width: fullHeight(context) / 7.2,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(cartBoxModel.imageBox)),
                          border: Border.all(
                            color: theme.primaryColor,
                            width: 3.2,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Align(
                        alignment: Alignment(-0.3, -0.1),
                        child: Text(cartBoxModel.textTitle,
                            style: theme.textTheme.bodyText1!.copyWith(
                                color: Color(0xff4c456f),
                                fontSize: standardSize(context) / 1.5,
                                fontWeight: FontWeight.w900)),
                      ),
                    ),
                    Container(
                      child: Align(
                        alignment: Alignment(-0.3, 0.5),
                        child: bodyText1(context, cartBoxModel.subTitles),
                      ),
                    ),
                    Align(
                      alignment: Alignment(-1, -1),
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(cartBoxModel.textBox,
                            style: theme.textTheme.caption!.copyWith(
                                color: Colors.white,
                                fontSize: smallSize(context) / 0.9,
                                fontWeight: FontWeight.w900)),
                        height: standardSize(context),
                        width: xlargeSize(context),
                        decoration: BoxDecoration(
                          color: theme.primaryColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(fullWidth(context) / 9),
                              bottomRight:
                                  Radius.circular(fullWidth(context) / 9)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }
}
