// import 'package:audio_service/audio_service.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_language_app/pages/listening_page/widgets/common.dart';
// import 'package:flutter_language_app/theme/dimens.dart';
// import 'package:flutter_language_app/widgets/wish_button/wish_button.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:just_audio/just_audio.dart';
// import 'package:rxdart/rxdart.dart';
//
//
//
// class ListeningPage extends StatefulWidget {
//
//   @override
//   State<StatefulWidget> createState() => ListeningPageState();
// }
//
//
// class ListeningPageState extends State<ListeningPage> {
//     AudioHandler? _audioHandler;
//    @override
//    void initState(){
//      super.initState();
//      initAudioService();
//    }
//
//   Future<AudioHandler> initAudioService() async {
//     return
//       _audioHandler = await AudioService.init(
//         builder: () => AudioPlayerHandler(),
//         config: const AudioServiceConfig(
//           androidNotificationChannelId: 'com.ryanheise.myapp.channel.audio',
//           androidNotificationChannelName: 'Audio playback',
//           androidNotificationOngoing: true,
//         ),
//       );
//   }
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: const Text('Audio Service Demo'),
//         ),
//         body: Container(
//           margin: EdgeInsets.symmetric(horizontal: smallSize(context)),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.start,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Align(
//                 alignment: Alignment.topCenter,
//                 child: Stack(
//                   children: [
//                     Align(
//                       alignment: Alignment.topCenter,
//                       child: Container(
//                         width: fullWidth(context) / 1.6,
//                         height: fullWidth(context) / 1.6,
//                         decoration: BoxDecoration(
//                           color: Colors.purple,
//                           shape: BoxShape.circle,
//                         ),
//                       ),
//                     ),
//                     Align(
//                       alignment: Alignment.topCenter,
//                       child: Container(
//                         margin: EdgeInsets.only(top: smallSize(context)),
//                         width: fullWidth(context) / 1.8,
//                         height: fullWidth(context) / 1.8,
//                         decoration: BoxDecoration(
//                             color: Colors.purple,
//                             borderRadius:
//                             BorderRadius.circular(fullWidth(context) / 7.2),
//                             image: const DecorationImage(
//                                 image: AssetImage('assets/pic_listening.jpg'),
//                                 fit: BoxFit.cover)),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Container(
//                 margin: EdgeInsets.only(top: mediumSize(context)),
//                 child: Text('احمد و محمود',
//                     style: Theme.of(context).textTheme.bodyText1!.copyWith(
//                         color: Color(0xff4c456f),
//                         fontSize: standardSize(context) / 1.2,
//                         fontWeight: FontWeight.w900)),
//               ),
//               Container(
//                   child: Text(
//                     'نوشته شده توسط جعفر موسوی',
//                     style: Theme.of(context).textTheme.bodyText1!.copyWith(
//                         color: Color(0xff4c456f),
//                         fontSize: standardSize(context) / 1.5,
//                         fontWeight: FontWeight.w900),
//                   )),
//               Spacer(),
//               Row(
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                 children: [
//                   Container(
//                       child: WishButtonWidget(2, false, standardSize(context))),
//                   Container(
//                       child: WishButtonWidget(2, false, standardSize(context))),
//                   Container(
//                       child: WishButtonWidget(2, false, standardSize(context))),
//                   // IconButton(
//                   //   splashColor: Colors.grey.shade200,
//                   //   splashRadius: mediumSize(context),
//                   //   color: Color(0xff3428ea),
//                   //   icon: Icon(
//                   //     volume >= 51 ? Icons.volume_up : volume <= 50 && volume != 0 ? Icons.volume_down : Icons.volume_mute,
//                   //     color: Color(0xff3428ea),
//                   //   ),
//                   //   onPressed: () {
//                   //   },
//                   // ),
//                 ],
//               ),
//               Container(
//                 margin: EdgeInsets.only(
//                     top: smallSize(context), bottom: smallSize(context)),
//                 height: fullWidth(context) / 2.8,
//                 child: Column(
//                   children: [
//                     Container(
//                       margin:
//                       EdgeInsets.symmetric(horizontal: xSmallSize(context)),
//                       child: StreamBuilder<MediaState>(
//                         // stream: _mediaStateStream,
//                         builder: (context, snapshot) {
//                           final mediaState = snapshot.data;
//                           return Column(
//                             children: [
//                               SeekBar(
//                                 duration: mediaState?.mediaItem?.duration ??
//                                     Duration.zero,
//                                 position: mediaState?.position ?? Duration.zero,
//                                 onChangeEnd: (newPosition) {
//                                   _audioHandler!.seek(newPosition);
//                                 },
//                               ),
//                               Row(
//                                 mainAxisAlignment:
//                                 MainAxisAlignment.spaceEvenly,
//                                 children: [
//                                   Container(
//                                     margin: EdgeInsets.symmetric(
//                                         horizontal: mediumSize(context)),
//                                     child: Text(
//                                         '${mediaState?.position.inMinutes.toStringAsFixed(0)}'
//                                     ),
//                                   ),
//                                   Container(
//                                     margin: EdgeInsets.symmetric(
//                                         horizontal: mediumSize(context)),
//                                     child: Text(mediaState?.mediaItem?.duration
//                                         .toString() ??
//                                         Duration.zero.toString()),
//                                   ),
//                                 ],
//                               ),
//                             ],
//                           );
//                         },
//                       ),
//                     ),
//                     StreamBuilder<bool>(
//                         // stream: _audioHandler!.playbackState
//                         //     .map((state) => state.playing)
//                         //     .distinct(),
//                         builder: (context, snapshot) {
//                           final playing = snapshot.data ?? false;
//                           return Container(
//                             margin: EdgeInsets.only(top: smallSize(context)),
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: [
//                                 IconButton(
//                                   splashColor: Colors.grey.shade200,
//                                   splashRadius: mediumSize(context),
//                                   icon: SvgPicture.asset(
//                                     'assets/repeat.svg',
//                                     width: iconSize(context),
//                                     height: iconSize(context),
//                                     color: const Color(0xff3428ea),
//                                   ),
//                                   onPressed: () {
//                                     setState(() {
//                                       if (playing == false) {
//                                         _audioHandler!.play();
//                                       }
//                                     });
//                                   },
//                                 ),
//                                 IconButton(
//                                   splashColor: Colors.grey.shade200,
//                                   splashRadius: mediumSize(context),
//                                   icon: SvgPicture.asset(
//                                     "assets/rewind_button.svg",
//                                     width: iconSize(context),
//                                     height: iconSize(context),
//                                     color: const Color(0xff3428ea),
//                                   ),
//                                   onPressed: () {
//                                     _audioHandler!.rewind();
//                                   },
//                                 ),
//                                 Container(
//                                   decoration: BoxDecoration(
//                                       borderRadius: BorderRadius.circular(
//                                           standardSize(context)),
//                                       gradient: const LinearGradient(colors: [
//                                         Color(0xff3428ea),
//                                         Color(0xffa573ff),
//                                       ])),
//                                   child: IconButton(
//                                     splashRadius: standardSize(context),
//                                     splashColor: Colors.grey.shade200,
//                                     icon: Icon(
//                                       playing ? Icons.pause : Icons.play_arrow,
//                                       color: Colors.white,
//                                     ),
//                                     onPressed: () async {
//                                       playing == true
//                                           ? _audioHandler!.pause()
//                                           : _audioHandler!.play();
//                                     },
//                                   ),
//                                 ),
//                                 IconButton(
//                                   splashColor: Colors.grey.shade200,
//                                   splashRadius: mediumSize(context),
//                                   icon: SvgPicture.asset(
//                                     "assets/rewind_button_next.svg",
//                                     width: iconSize(context),
//                                     height: iconSize(context),
//                                     color: const Color(0xff3428ea),
//                                   ),
//                                   onPressed: () {
//                                     _audioHandler!.fastForward();
//                                   },
//                                 ),
//                                 IconButton(
//                                   splashColor: Colors.grey.shade200,
//                                   splashRadius: mediumSize(context),
//                                   icon: SvgPicture.asset(
//                                     "assets/shuffle.svg",
//                                     width: iconSize(context),
//                                     height: iconSize(context),
//                                     color: const Color(0xff3428ea),
//                                   ),
//                                   onPressed: () {},
//                                 ),
//                                 // IconButton(
//                                 //   splashColor: theme.primaryColor,
//                                 //   icon: SvgPicture.asset(
//                                 //       'assets/ic_backward.svg'),
//                                 //   onPressed: () {},
//                                 //   color: Colors.black,
//                                 // ),
//                               ],
//                             ),
//                           );
//                         })
//                   ],
//                 ),
//               )
//             ],
//           ),
//         ),
//       // Center(
//       //   child: Column(
//       //     mainAxisAlignment: MainAxisAlignment.center,
//       //     children: [
//       //       // Show media item title
//       //       StreamBuilder<MediaItem?>(
//       //         stream: _audioHandler.mediaItem,
//       //         builder: (context, snapshot) {
//       //           final mediaItem = snapshot.data;
//       //           return Text(mediaItem?.title ?? '');
//       //         },
//       //       ),
//       //       // Play/pause/stop buttons.
//       //       StreamBuilder<bool>(
//       //         stream: _audioHandler.playbackState
//       //             .map((state) => state.playing)
//       //             .distinct(),
//       //         builder: (context, snapshot) {
//       //           final playing = snapshot.data ?? false;
//       //           return Row(
//       //             mainAxisAlignment: MainAxisAlignment.center,
//       //             children: [
//       //               _button(Icons.fast_rewind, _audioHandler.rewind),
//       //               if (playing)
//       //                 _button(Icons.pause, _audioHandler.pause)
//       //               else
//       //                 _button(Icons.play_arrow, _audioHandler.play),
//       //               _button(Icons.stop, _audioHandler.stop),
//       //               _button(Icons.fast_forward, _audioHandler.fastForward),
//       //             ],
//       //           );
//       //         },
//       //       ),
//       //       // A seek bar.
//       //       StreamBuilder<MediaState>(
//       //         stream: _mediaStateStream,
//       //         builder: (context, snapshot) {
//       //           final mediaState = snapshot.data;
//       //           return SeekBar(
//       //             duration: mediaState?.mediaItem?.duration ?? Duration.zero,
//       //             position: mediaState?.position ?? Duration.zero,
//       //             onChangeEnd: (newPosition) {
//       //               _audioHandler.seek(newPosition);
//       //             },
//       //           );
//       //         },
//       //       ),
//       //     ],
//       //   ),
//       // ),
//     );
//   }
//
//   /// A stream reporting the combined state of the current media item and its
//   /// current position.
//   Stream<MediaState> get _mediaStateStream =>
//       Rx.combineLatest2<MediaItem?, Duration, MediaState>(
//           _audioHandler!.mediaItem,
//           AudioService.position,
//               (mediaItem, position) => MediaState(mediaItem, position));
//
//   IconButton _button(IconData iconData, VoidCallback onPressed) => IconButton(
//     icon: Icon(iconData),
//     iconSize: 64.0,
//     onPressed: onPressed,
//   );
// }
//
// class MediaState {
//   final MediaItem? mediaItem;
//   final Duration position;
//
//   MediaState(this.mediaItem, this.position);
// }
//
// class AudioPlayerHandler extends BaseAudioHandler with SeekHandler {
//   static final _item = MediaItem(
//     id: 'https://files.olgoirani.com/Files/Content/1399/12/11055/a-ba-vafa-ja-moondam-az-karbala.mp3',
//     album: "Science Friday",
//     title: "A Salute To Head-Scratching Science",
//     artist: "Science Friday and WNYC Studios",
//     duration: const Duration(milliseconds: 5739820),
//     artUri: Uri.parse(
//         'https://media.wnyc.org/i/1400/1400/l/80/1/ScienceFriday_WNYCStudios_1400.jpg'),
//   );
//
//   final _player = AudioPlayer();
//
//   AudioPlayerHandler() {
//     _player.playbackEventStream.map(_transformEvent).pipe(playbackState);
//     mediaItem.add(_item);
//
//     _player.setAudioSource(AudioSource.uri(Uri.parse(_item.id)));
//   }
//
//   @override
//   Future<void> play() => _player.play();
//
//   @override
//   Future<void> pause() => _player.pause();
//
//   @override
//   Future<void> seek(Duration position) => _player.seek(position);
//
//   @override
//   Future<void> stop() => _player.stop();
//
//   /// Transform a just_audio event into an audio_service state.
//   ///
//   /// This method is used from the constructor. Every event received from the
//   /// just_audio player will be transformed into an audio_service state so that
//   /// it can be broadcast to audio_service clients.
//   PlaybackState _transformEvent(PlaybackEvent event) {
//     return PlaybackState(
//       controls: [
//         MediaControl.rewind,
//         if (_player.playing) MediaControl.pause else MediaControl.play,
//         MediaControl.stop,
//         MediaControl.fastForward,
//       ],
//       systemActions: const {
//         MediaAction.seek,
//         MediaAction.seekForward,
//         MediaAction.seekBackward,
//       },
//       androidCompactActionIndices: const [0, 1, 3],
//       processingState: const {
//         ProcessingState.idle: AudioProcessingState.idle,
//         ProcessingState.loading: AudioProcessingState.loading,
//         ProcessingState.buffering: AudioProcessingState.buffering,
//         ProcessingState.ready: AudioProcessingState.ready,
//         ProcessingState.completed: AudioProcessingState.completed,
//       }[_player.processingState]!,
//       playing: _player.playing,
//       updatePosition: _player.position,
//       bufferedPosition: _player.bufferedPosition,
//       speed: _player.speed,
//       queueIndex: event.currentIndex,
//     );
//   }
// }
