import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/lessons/select_word_page.dart';
import 'package:flutter_language_app/pages/listening_page/listening_notifier.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/action_widgets.dart';
import 'package:flutter_language_app/widgets/wish_button/wish_button.dart';
import 'package:flutter_svg/svg.dart';
import 'package:line_icons/line_icons.dart';
import 'package:stacked/stacked.dart';

class ListeningPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ListeningPageState();
}

class ListeningPageState extends State<ListeningPage> {
  bool isPlaying = false;
  bool isStopped = true;
  String currentTime = "00:00";
  String completeTime = "00:00";
  double sliderValue = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<ListeningVM>.reactive(
        viewModelBuilder: () => ListeningVM(),
        builder: (context, model, child) => Directionality(
              textDirection: TextDirection.rtl,
              child: Scaffold(
                appBar: AppBar(
                  elevation: 0,
                  brightness: Brightness.light,
                  leading: Container(
                    margin: EdgeInsets.only(right: mediumSize(context)),
                    child: backIcon(context),
                  ),
                  title: Text('داستان',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: Color(0xff474747))),
                  centerTitle: true,
                  backgroundColor: theme.backgroundColor,
                ),
                body: Container(
                  margin: EdgeInsets.symmetric(horizontal: smallSize(context)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                width: fullWidth(context) / 1.6,
                                height: fullWidth(context) / 1.6,
                                decoration: BoxDecoration(
                                  color: Colors.purple,
                                  shape: BoxShape.circle,
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.topCenter,
                              child: Container(
                                margin:
                                    EdgeInsets.only(top: smallSize(context)),
                                width: fullWidth(context) / 1.8,
                                height: fullWidth(context) / 1.8,
                                decoration: BoxDecoration(
                                    color: Colors.purple,
                                    borderRadius: BorderRadius.circular(
                                        fullWidth(context) / 7.2),
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/pic_listening.jpg'),
                                        fit: BoxFit.cover)),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: mediumSize(context)),
                        child: Text('احمد و محمود',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(
                                    color: Color(0xff4c456f),
                                    fontSize: standardSize(context) / 1.2,
                                    fontWeight: FontWeight.w900)),
                      ),
                      Container(
                          child: Text(
                        'نوشته شده توسط جعفر موسوی',
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: Color(0xff4c456f),
                            fontSize: standardSize(context) / 1.5,
                            fontWeight: FontWeight.w900),
                      )),
                      Spacer(),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                              child: WishButtonWidget(
                                  2, false, standardSize(context))),
                          Container(
                              child: WishButtonWidget(
                                  2, false, standardSize(context))),
                          Container(
                              child: WishButtonWidget(
                                  2, false, standardSize(context)))
                        ],
                      ),
                      Container(
                          margin: EdgeInsets.only(
                              top: smallSize(context),
                              bottom: smallSize(context)),
                          height: fullWidth(context) / 2.8,
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: xSmallSize(context)),
                                child: Slider(
                                  inactiveColor: Color(0xfff3f4f9),
                                  activeColor: Color(0xff3428ea),
                                  value: sliderValue,
                                  onChanged: (g) {
                                    setState(() {
                                      sliderValue = g;
                                    });
                                  },
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: mediumSize(context)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      currentTime,
                                      style: theme.textTheme.caption!
                                          .copyWith(color: Color(0xffbdbdbd)),
                                    ),
                                    Expanded(
                                      child: SizedBox(),
                                    ),
                                    Text(
                                      completeTime,
                                      style: theme.textTheme.caption!
                                          .copyWith(color: Color(0xffbdbdbd)),
                                    ),

                                  ],
                                ),
                              ),
                              Container(
                                margin:
                                    EdgeInsets.only(top: smallSize(context)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    IconButton(
                                      splashColor: splashColor(),
                                      splashRadius: mediumSize(context),
                                      icon: SvgPicture.asset(
                                        'assets/repeat.svg',
                                        width: iconSize(context),
                                        height: iconSize(context),
                                        color: Color(0xff3428ea),
                                      ),
                                      onPressed: () {
                                        // _audioPlayer.seek(Duration(seconds: -10));
                                      },
                                    ),
                                    IconButton(
                                      splashColor: splashColor(),
                                      splashRadius: mediumSize(context),
                                      icon: SvgPicture.asset(
                                        "assets/rewind_button_next.svg",
                                        width: iconSize(context),
                                        height: iconSize(context),
                                        color: Color(0xff3428ea),
                                      ),
                                      onPressed: () {},
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              standardSize(context)),
                                          gradient: LinearGradient(colors: [
                                            Color(0xff3428ea),
                                            Color(0xffa573ff),
                                          ])),
                                      child: IconButton(
                                        splashRadius: standardSize(context),
                                        splashColor: splashColor(),
                                        icon: Icon(
                                          isPlaying
                                              ? Icons.pause
                                              : Icons.play_arrow,
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          // if (isPlaying) {
                                          //   _audioPlayer.pause();
                                          //
                                          //   setState(() {
                                          //     isPlaying = false;
                                          //   });
                                          // } else {
                                          //   _audioPlayer.play(
                                          //       "https://storage.kashoob.com/dl/202105/enc_16212700430317333343351.mp3");
                                          //
                                          //   setState(() {
                                          //     isStopped = false;
                                          //     isPlaying = !isPlaying;
                                          //   });
                                          // }
                                        },
                                      ),
                                    ),
                                    IconButton(
                                      splashColor: splashColor(),
                                      splashRadius: mediumSize(context),
                                      icon: SvgPicture.asset(
                                        "assets/rewind_button.svg",
                                        width: iconSize(context),
                                        height: iconSize(context),
                                        color: Color(0xff3428ea),
                                      ),
                                      onPressed: () {},
                                    ),
                                    IconButton(
                                      splashColor: splashColor(),
                                      splashRadius: mediumSize(context),
                                      icon: SvgPicture.asset(
                                        "assets/shuffle.svg",
                                        width: iconSize(context),
                                        height: iconSize(context),
                                        color: Color(0xff3428ea),
                                      ),
                                      onPressed: () {
                                        // _audioPlayer.seek(Duration(seconds: 10));
                                      },
                                    ),
                                    // IconButton(
                                    //   splashColor: theme.primaryColor,
                                    //   icon: SvgPicture.asset(
                                    //       'assets/ic_backward.svg'),
                                    //   onPressed: () {},
                                    //   color: Colors.black,
                                    // ),
                                  ],
                                ),
                              )
                            ],
                          ))
                    ],
                  ),
                ),
              ),
            ));
  }
}
