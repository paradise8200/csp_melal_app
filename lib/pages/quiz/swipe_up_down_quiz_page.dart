import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/image_widget.dart';
import 'package:flutter_svg/svg.dart';

class SwipeUpDownQuizPage extends StatefulWidget {
  @override
  SwipeUpDownQuizPageState createState() => SwipeUpDownQuizPageState();
}

class SwipeUpDownQuizPageState extends State<SwipeUpDownQuizPage> {
  Alignment boxAlign = Alignment(0.0,-0.2);

  double firstBoxOpacity = 1;

  double latterBoxOpacity = 1;

  double questionBoxOpacity = 1;

  double titleBoxOpacity = 0;

  double arrowOpacity = 1;

  bool disableBox = false;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: mediumSize(context)),
        child: FloatingActionButton(
          onPressed: () {},
          backgroundColor: Colors.transparent,
          child: Container(
              padding: EdgeInsets.all(mediumSize(context)),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                      Color(0xff3428ea),
                      Color(0xffa573ff),
                    ],
                    tileMode: TileMode.clamp,
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter),
                boxShadow: [
                  BoxShadow(
                      color: Color(0xff976afc).withOpacity(0.50),
                      blurRadius: 5,
                      spreadRadius: 3,
                      offset: Offset(0, 1))
                ],
                borderRadius: BorderRadius.circular(
                  xxLargeSize(context),
                ),
              ),
              child:
                  SvgPicture.asset("assets/volume.svg", color: Colors.white)),
        ),
      ),
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        leading: SizedBox(),
        toolbarHeight: 0,
      ),
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.only(top: smallSize(context)),
        child: Stack(
          children: [
            Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.symmetric(vertical: smallSize(context)),
                child: Directionality(
                  textDirection: TextDirection.rtl,
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Text(
                      "به بالا یا پایین بکش!",
                      textAlign: TextAlign.center,
                      style: theme.textTheme.bodyText2!.copyWith(
                          color: Colors.black,
                          fontSize: mediumSize(context) / 1, //
                          fontWeight: FontWeight.w600,
                          shadows: [
                            BoxShadow(
                              color: Color(0xff474747).withOpacity(0.42),
                              spreadRadius: 2,
                              blurRadius: 8,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            )
                          ]),
                    ),
                  ),
                )),
            AnimatedAlign(
              duration: Duration(milliseconds: 800),
              alignment: boxAlign,
              child: Stack(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Opacity(
                        opacity: arrowOpacity,
                        child: Container(
                            margin:
                                EdgeInsets.only(bottom: standardSize(context)),
                            color: Colors.transparent,
                            width: fullWidth(context),
                            child: SvgPicture.asset(
                              "assets/svg_swip_up.svg",
                              width: xSmallSize(context),
                              height: xSmallSize(context),
                            )),
                      ),
                      GestureDetector(
                        onVerticalDragEnd: disableBox ? null : (details) {
                          if (details.primaryVelocity! < 0) {
                            setState(() {
                              boxAlign = Alignment(0.0,-0.7);
                              latterBoxOpacity = 0;
                              questionBoxOpacity = 0;
                              titleBoxOpacity = 1;
                              arrowOpacity = 0;
                              disableBox = true;
                            });
                            print("drag to up");
                          }
                        },
                        child: AnimatedOpacity(
                          duration: Duration(milliseconds: 200),
                          opacity: firstBoxOpacity,
                          child: Container(
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color:
                                          Color(0xff7f77fe).withOpacity(0.50),
                                      blurRadius: 10,
                                      spreadRadius: 3,
                                      offset: Offset(0, 4))
                                ],
                                borderRadius: BorderRadius.only(
                                  topLeft:
                                      Radius.circular(standardSize(context)),
                                  topRight:
                                      Radius.circular(standardSize(context)),
                                ),
                                color: Colors.grey),
                            width: fullWidth(context) / 1.2,
                            height: fullWidth(context) / 1.8,
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(standardSize(context)),
                                topRight:
                                    Radius.circular(standardSize(context)),
                              ),
                              child: Stack(
                                children: [
                                  Positioned.fill(
                                      child: imageWidget(
                                          "https://s4.uupload.ir/files/mother_m86y.jpg",
                                          fit: BoxFit.cover)),
                                  Positioned(
                                    top: 0,
                                    right: 0,
                                    left: 0,
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "the mother",
                                        style: theme.textTheme.bodyText2!
                                            .copyWith(
                                                fontFamily: "gilroy",
                                                color: Colors.white,
                                                fontWeight: FontWeight.w700),
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.black.withOpacity(0.30),
                                      ),
                                      width: fullWidth(context),
                                      padding: EdgeInsets.symmetric(
                                          vertical: xSmallSize(context) / 1.2),
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 0,
                                    right: 0,
                                    left: 0,
                                    child: AnimatedOpacity(
                                      duration: Duration(milliseconds: 1000),
                                      opacity: titleBoxOpacity,
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          "مادر",
                                          style: theme.textTheme.bodyText2!
                                              .copyWith(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w700),
                                        ),
                                        decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.30),
                                        ),
                                        width: fullWidth(context),
                                        padding: EdgeInsets.symmetric(
                                            vertical:
                                                xSmallSize(context) / 1.2),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onVerticalDragEnd: disableBox ? null : (details) {
                          if (details.primaryVelocity! > 0) {
                            print("drag to down");
                            setState(() {
                              boxAlign = Alignment(0.0,0.25);
                              firstBoxOpacity = 0;
                              questionBoxOpacity = 0;
                              titleBoxOpacity = 1;
                              arrowOpacity = 0;
                              disableBox = true;
                            });
                          }
                        },
                        child: AnimatedOpacity(
                          duration: Duration(milliseconds: 200),
                          opacity: latterBoxOpacity,
                          child: Container(
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                      color:
                                          Color(0xff7f77fe).withOpacity(0.50),
                                      blurRadius: 10,
                                      spreadRadius: 3,
                                      offset: Offset(0, 4))
                                ],
                                borderRadius: BorderRadius.only(
                                  bottomLeft:
                                      Radius.circular(standardSize(context)),
                                  bottomRight:
                                      Radius.circular(standardSize(context)),
                                ),
                                color: Colors.grey),
                            width: fullWidth(context) / 1.2,
                            height: fullWidth(context) / 1.8,
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                bottomLeft:
                                    Radius.circular(standardSize(context)),
                                bottomRight:
                                    Radius.circular(standardSize(context)),
                              ),
                              child: Stack(
                                children: [
                                  Positioned.fill(
                                      child: imageWidget(
                                          "https://s4.uupload.ir/files/sister_t4ao.jpg",
                                          fit: BoxFit.cover)),
                                  Positioned(
                                    bottom: 0,
                                    right: 0,
                                    left: 0,
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "the sister",
                                        style: theme.textTheme.bodyText2!
                                            .copyWith(
                                                fontFamily: "gilroy",
                                                color: Colors.white,
                                                fontWeight: FontWeight.w700),
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.black.withOpacity(0.30),
                                      ),
                                      width: fullWidth(context),
                                      padding: EdgeInsets.symmetric(
                                          vertical: xSmallSize(context) / 1.2),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Opacity(
                        opacity: arrowOpacity,
                        child: Container(
                            margin: EdgeInsets.only(top: standardSize(context)),
                            color: Colors.transparent,
                            width: fullWidth(context),
                            child: SvgPicture.asset(
                              "assets/swipe_down.svg",
                              width: xSmallSize(context),
                              height: xSmallSize(context),
                            )),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            AnimatedOpacity(
              duration: Duration(milliseconds: 200),
              opacity: questionBoxOpacity,
              child: Align(
                alignment: Alignment(0.0,-0.07),
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xff3428ea),
                          Color(0xffa573ff),
                        ],
                        tileMode: TileMode.clamp,
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter),
                    borderRadius: BorderRadius.circular(xSmallSize(context)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0xff976afc).withOpacity(0.50),
                          blurRadius: 5,
                          spreadRadius: 3,
                          offset: Offset(0, 1))
                    ],
                  ),
                  padding: EdgeInsets.symmetric(
                      vertical: 7, horizontal: largeSize(context)),
                  child: Text(
                    "مادر",
                    textAlign: TextAlign.center,
                    style: theme.textTheme.bodyText2!.copyWith(
                        color: Colors.white,
                        fontSize: mediumSize(context) / 1, //
                        fontWeight: FontWeight.w600,
                        shadows: [
                          BoxShadow(
                            color: Color(0xff474747).withOpacity(0.42),
                            spreadRadius: 2,
                            blurRadius: 8,
                            offset: Offset(0, 2), // changes position of shadow
                          )
                        ]),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
