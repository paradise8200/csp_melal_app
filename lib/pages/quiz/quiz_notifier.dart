import 'package:flutter_language_app/models/choice_pic_quiz_model.dart';
import 'package:flutter_language_app/models/matching_quiz_model.dart';
import 'package:flutter_language_app/repo/quiz_repo.dart';
import 'package:stacked/stacked.dart';

class QuizVM extends FutureViewModel {
  late List<MatchingQuizModel> _listMatchingQuizAnswer;

  List<MatchingQuizModel> get listMatchingQuizAnswer => _listMatchingQuizAnswer;

  late List<WordModel> _answerWord;
  List<WordModel> get answerWord => _answerWord;

   String _correctAnswer = "";
  // List<WordModel> get answerWord => correctAnswer;

  late List<ChoicePicModel> _listChoicePicQuizAnswer;
  List<ChoicePicModel> get listChoicePicQuizAnswer => _listChoicePicQuizAnswer;

  Future<List<MatchingQuizModel>> getMatchingQuizDataFromServer() async {
    await Future.delayed(Duration(seconds: 1));
    var result = QuizRepository.getMatchingQuiz();
    _listMatchingQuizAnswer = result;
    _listMatchingQuizAnswer.toSet();
    // _listMatchingQuizAnswer.shuffle();
    notifyListeners();
    return result;
  }

  Future<List<ChoicePicModel>> getChoicePicDataFromServer() async {
    await Future.delayed(Duration(milliseconds: 300));
    var result = QuizRepository.getChoicePicQuiz();
    _listChoicePicQuizAnswer = result;
    _listChoicePicQuizAnswer.shuffle();
    notifyListeners();
    return result;
  }

  Future<List<WordModel>> getDataWordModel() async {
    await Future.delayed(Duration(seconds: 1));
    var result = QuizRepository.getAnswerWord();
    _answerWord = result;
    _answerWord.toSet();
    answerWord.where((element) => element.isSelectedd);
    answerWord.any((element) => element.isSelectedd);
    answerWord.lastWhere((element) => element.isSelectedd);

    // _listMatchingQuizAnswer.shuffle();
    notifyListeners();
    return result;
  }

  Future getDataFromServer() async {
    var a = getChoicePicDataFromServer();
    var b = getMatchingQuizDataFromServer();
    var c = getDataWordModel();
    var futures = <Future>[a, b, c];
    var _data = await Future.wait(futures);
    notifyListeners();
    return _data;
  }

  @override
  Future futureToRun() {
    return getDataFromServer();
  }
}
