import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_language_app/pages/quiz/quiz_notifier.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:stacked/stacked.dart';

class MultiChoiceWidget extends ViewModelWidget<QuizVM> {
  final String txt;

  MultiChoiceWidget(this.txt);

  @override
  Widget build(BuildContext context, model) {
    var theme = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: standardSize(context)),
          decoration: BoxDecoration(
              color: Color(0xffF1F2F7),
              boxShadow: [BoxShadow(color: Color(0xff000000).withOpacity(0.16),
              spreadRadius:3,
                offset: Offset(0, 2),
                blurRadius: 4

              )],
              borderRadius: BorderRadius.circular(mediumSize(context))),
          width: fullWidth(context) / 1.3,
          height: fullWidth(context) / 7,
          child: Container(
              child: Text(
            txt,
            textAlign: TextAlign.center,
            style: theme.textTheme.bodyText1!
                .copyWith(color: Color(0xff474747), fontFamily: "gilroy",
            fontWeight: FontWeight.w100,
            fontSize: mediumSize(context),


            ),
          )),
        ),
      ],
    );
  }
}
