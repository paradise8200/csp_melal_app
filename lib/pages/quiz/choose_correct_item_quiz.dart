import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/models/matching_quiz_model.dart';
import 'package:flutter_language_app/pages/quiz/quiz_notifier.dart';
import 'package:flutter_language_app/pages/quiz/widgets/multi_choise_widget.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/image_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stacked/stacked.dart';

class ChooseCorrectItemPage extends StatefulWidget {
  @override
  _MatchingQuizPageState createState() => _MatchingQuizPageState();
}

class _MatchingQuizPageState extends State<ChooseCorrectItemPage> {
  late int score;

  bool gameOver = false;
  double _sigmaX = 0.0; // from 0-10
  double _sigmaY = 0.0; // from 0-10
  double _opacity = 0.1; // from 0-1.0

  @override
  void initState() {
    super.initState();
    initGame();
  }

  initGame() {
    score = 0;
    gameOver = false;
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<QuizVM>.reactive(
        viewModelBuilder: () => QuizVM(),
        builder: (context, model, child) => Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              toolbarHeight: 0,
              elevation: 0,
            ),
            body: Container(
              margin: EdgeInsets.only(top: smallSize(context)),
              width: fullWidth(context),
              height: fullHeight(context),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                        // alignment: Alignment.center,
                        // width: fullWidth(context),
                        margin:
                            EdgeInsets.symmetric(vertical: smallSize(context)),
                        child: Text(
                          "!تصویر صحیح را انتخاب کنید",
                          textAlign: TextAlign.center,
                          style: theme.textTheme.bodyText2!.copyWith(
                              color: Colors.black,
                              fontSize: mediumSize(context) / 1, //
                              fontWeight: FontWeight.w600,
                              shadows: [
                                BoxShadow(
                                  color: Color(0xff474747).withOpacity(0.42),
                                  spreadRadius: 2,
                                  blurRadius: 8,
                                  offset: Offset(
                                      0, 2), // changes position of shadow
                                )
                              ]),
                        )),
                    Container(
                      height: fullWidth(context) / 1.4,
                      width: fullWidth(context) / 1.3,
                      margin: EdgeInsets.symmetric(
                          horizontal: smallSize(context),
                          vertical: mediumSize(context)),
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(xlargeSize(context) / 1.4),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xff7f77fe).withOpacity(0.50),
                              blurRadius: 10,
                              spreadRadius: 3,
                              offset: Offset(0, 4))
                        ],
                      ),
                      child: ClipRRect(
                        borderRadius:
                            BorderRadius.circular(xlargeSize(context) / 1.4),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              child: Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                  image:
                                      AssetImage("assets/gradinet_image.jpg"),
                                  fit: BoxFit.cover,
                                )),
                                child: BackdropFilter(
                                  filter: ImageFilter.blur(
                                      sigmaX: 17, sigmaY: 17),
                                  child: Container(
                                    color: Colors.transparent.withOpacity(0),
                                  ),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                alignment: Alignment.center,
                                child: FloatingActionButton(
                                  onPressed: () {},
                                  backgroundColor: Colors.transparent,
                                  child: Container(
                                      padding:
                                          EdgeInsets.all(mediumSize(context)),
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            colors: [
                                              Color(0xff3428ea),
                                              Color(0xffa573ff),
                                            ],
                                            tileMode: TileMode.clamp,
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Color(0xff976afc)
                                                  .withOpacity(0.50),
                                              blurRadius: 5,
                                              spreadRadius: 3,
                                              offset: Offset(0, 1))
                                        ],
                                        borderRadius: BorderRadius.circular(
                                          xxLargeSize(context),
                                        ),
                                      ),
                                      child: SvgPicture.asset(
                                          "assets/volume.svg",
                                          color: Colors.white)),
                                ),
                                width: fullWidth(context),
                                height: fullWidth(context) / 6.5,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: mediumSize(context)),
                      width: fullWidth(context) / 1.3,
                      height: fullWidth(context) / 1.2,
                      child: GridView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.all(standardSize(context)),
                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 14 / 14,
                            crossAxisSpacing: mediumSize(context),
                            mainAxisSpacing: mediumSize(context)

                        ),
                        itemCount: 4,
                        itemBuilder: (context, index) {
                          return
                            Container(
                            // Align(
                            //   alignment: Alignment.bottomCenter,
                            //   child: Container(
                            //     alignment: Alignment.center,
                            //     child: Text(
                            //       "زن , بانو",
                            //       style: theme.textTheme.bodyText2!.copyWith(
                            //           color: Colors.white, fontWeight: FontWeight.w700),
                            //     ),
                            //     decoration: BoxDecoration(
                            //       color: Colors.black.withOpacity(0.30),
                            //       borderRadius: BorderRadius.only(
                            //           bottomRight:
                            //           Radius.circular(xlargeSize(context) / 1.2),
                            //           bottomLeft:
                            //           Radius.circular(xlargeSize(context) / 1.2)),
                            //     ),
                            //     width: fullWidth(context),
                            //     height: fullWidth(context) / 6.5,
                            //   ),
                            // ),

                          decoration: BoxDecoration(
                            color: Colors.white,
                                borderRadius: BorderRadius.circular(xlargeSize(context) / 3),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors
                                        .black
                                        .withOpacity(
                                        0.16),
                                    blurRadius:
                                    7,
                                    spreadRadius:
                                    1.5,
                                    offset:
                                    Offset(
                                        0,
                                        5))
                              ],
                            ),

                            child:model.isBusy?CupertinoActivityIndicator(): ClipRRect(
                                borderRadius: BorderRadius.circular(
                                    xlargeSize(context) / 3),
                                child: Stack(
                                  children: [
                                    Positioned.fill(
                                      child: imageWidget(
                                          model.listMatchingQuizAnswer[index].image),
                                    ),
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          "زن , بانو",
                                          style: theme.textTheme.bodyText2!.copyWith(
                                              color: Colors.white, fontWeight: FontWeight.w700,
                                          fontSize: smallSize(context)),
                                        ),
                                        decoration: BoxDecoration(
                                          color: Colors.black.withOpacity(0.30),
                                          borderRadius: BorderRadius.only(
                                              bottomRight:
                                              Radius.circular(xlargeSize(context) / 3),
                                              bottomLeft:
                                              Radius.circular(xlargeSize(context) / 3)),
                                        ),
                                        width: fullWidth(context),
                                        height: largeSize(context),
                                      ),
                                    ),

                                  ],
                                )),
                          );
                        },
                      ),
                    )
                  ],
                ),
              ),
            )));
  }
}
