import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_language_app/models/matching_quiz_model.dart';
import 'package:flutter_language_app/pages/quiz/quiz_notifier.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/image_widget.dart';
import 'package:stacked/stacked.dart';

class MatchingQuizPage extends StatefulWidget {
  @override
  _MatchingQuizPageState createState() => _MatchingQuizPageState();
}

class _MatchingQuizPageState extends State<MatchingQuizPage> {
  late int score;

  bool gameOver = false;

  @override
  void initState() {
    super.initState();
    initGame();
  }

  initGame() {
    score = 0;
    gameOver = false;
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<QuizVM>.reactive(
        viewModelBuilder: () => QuizVM(),
        builder: (context, model, child) => Scaffold(
              appBar: AppBar(
                elevation: 0,
                toolbarHeight: 0,
                backgroundColor: Colors.white,
                automaticallyImplyLeading: false,
                leading: SizedBox(),
              ),
              body:
                  // model.isBusy
                  //     ? CupertinoActivityIndicator()
                  //     : model.listMatchingQuizAnswer.length == 0
                  //         ? Center(
                  //             child: Column(
                  //               mainAxisAlignment: MainAxisAlignment.center,
                  //               children: [
                  //                 Container(
                  //                     margin: EdgeInsets.only(bottom: 20),
                  //                     child: Text(
                  //                       "🎉 You Win 🎉",
                  //                       style: TextStyle(
                  //                           fontSize: 30,
                  //                           color: Colors.green,
                  //                           fontWeight: FontWeight.bold),
                  //                     )),
                  //                 ElevatedButton(
                  //                     onPressed: () {
                  //                       setState(() {
                  //                         model.getDataFromServer();
                  //                       });
                  //                     },
                  //                     child: Text("Repeat Game")),
                  //               ],
                  //             ),
                  //           )
                  //         : gameOver
                  //             ? Center(
                  //                 child: Column(
                  //                   mainAxisAlignment: MainAxisAlignment.center,
                  //                   children: [
                  //                     Container(
                  //                         margin: EdgeInsets.only(bottom: 20),
                  //                         child: Text(
                  //                           "Game Over",
                  //                           style: TextStyle(
                  //                               fontSize: 30,
                  //                               color: Colors.red,
                  //                               fontWeight: FontWeight.bold),
                  //                         )),
                  //                     ElevatedButton(
                  //                         onPressed: () {
                  //                           setState(() {
                  //                             initGame();
                  //                           });
                  //                         },
                  //                         child: Text("Repeat Game")),
                  //                   ],
                  //                 ),
                  //               )
                  //             :
                  model.isBusy
                      ? CupertinoActivityIndicator()
                      : SingleChildScrollView(
                          child: Container(
                              margin: EdgeInsets.only(
                                top: smallSize(context),
                                  left: largeSize(context) / 0.6,
                                  right: largeSize(context) / 0.6,
                                  ),
                              child: Column(
                                children: [
                                  Directionality(
                                      textDirection: TextDirection.rtl,
                                      child: Container(
                                        margin: EdgeInsets.symmetric(vertical: smallSize(context)),
                                        width: fullWidth(context),
                                        alignment: Alignment.center,
                                        child: Text(
                                          "کلمات مربوط انتخاب کنید!",
                                          style: theme.textTheme.bodyText2!
                                              .copyWith(
                                            color: Colors.black,
                                            fontSize: mediumSize(context) / 1,
                                            fontWeight: FontWeight.w600,
                                            shadows: [
                                              BoxShadow(
                                                color: Color(0xff474747)
                                                    .withOpacity(0.42),
                                                spreadRadius: 2,
                                                blurRadius: 8,
                                                offset: Offset(0,
                                                    2), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                        ),
                                      )),
                                  Row(
                                    children: [
                                      gameOver
                                          ? SizedBox()
                                          :
                                      Column(
                                              children: model
                                                  .listMatchingQuizAnswer
                                                  .map((item) => Draggable<MatchingQuizModel>(
                                                        data: item,
                                                        childWhenDragging:
                                                            Container(
                                                          alignment:
                                                              Alignment.center,
                                                          child: Text(item.name,
                                                              style: theme
                                                                  .textTheme
                                                                  .bodyText1!
                                                                  .copyWith(
                                                                      fontFamily:
                                                                          "gilroy",
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500,
                                                                      color: Colors
                                                                          .white,
                                                                      height:
                                                                          1.2)),
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius.circular(
                                                                    mediumSize(
                                                                            context) /
                                                                        1.2),
                                                            gradient:
                                                                LinearGradient(
                                                              begin: Alignment
                                                                  .topLeft,
                                                              end: Alignment
                                                                  .bottomRight,
                                                              stops: [0.0, 1.0],
                                                              colors: [
                                                                Color(
                                                                    0xff3428ea),
                                                                Color(
                                                                    0xffa573ff),
                                                              ],
                                                            ),
                                                            boxShadow: [
                                                              BoxShadow(
                                                                  color: Color(
                                                                      0xff9D6EFD),
                                                                  blurRadius: 4,
                                                                  offset:
                                                                      Offset(
                                                                          0, 0))
                                                            ],
                                                          ),
                                                          width: fullWidth(
                                                                  context) /
                                                              3.8,
                                                          height: fullWidth(
                                                                  context) /
                                                              6,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: largeSize(
                                                                      context)),
                                                        ),
                                                        feedback: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: largeSize(
                                                                      context)),
                                                          alignment:
                                                              Alignment.center,
                                                          child: Text(item.name,
                                                              style: theme
                                                                  .textTheme
                                                                  .bodyText1!
                                                                  .copyWith(
                                                                      fontFamily:
                                                                          "gilroy",
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500,
                                                                      color: Colors
                                                                          .white,
                                                                      height:
                                                                          1.2)),
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius.circular(
                                                                    mediumSize(
                                                                            context) /
                                                                        1.2),
                                                            gradient:
                                                                LinearGradient(
                                                              begin: Alignment
                                                                  .topLeft,
                                                              end: Alignment
                                                                  .bottomRight,
                                                              stops: [0.0, 1.0],
                                                              colors: [
                                                                Color(
                                                                    0xff3428ea),
                                                                Color(
                                                                    0xffa573ff),
                                                              ],
                                                            ),
                                                            boxShadow: [
                                                              BoxShadow(
                                                                  color: Color(
                                                                      0xff9D6EFD),
                                                                  blurRadius: 4,
                                                                  offset:
                                                                      Offset(
                                                                          0, 0))
                                                            ],
                                                          ),
                                                          width: fullWidth(
                                                                  context) /
                                                              3.8,
                                                          height: fullWidth(
                                                                  context) /
                                                              6,
                                                        ),
                                                        child: Container(
                                                          alignment:
                                                              Alignment.center,
                                                          child: Text(item.name,
                                                              style: theme
                                                                  .textTheme
                                                                  .bodyText1!
                                                                  .copyWith(
                                                                      fontFamily:
                                                                          "gilroy",
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                      height:
                                                                          1.2)),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius.circular(
                                                                    mediumSize(
                                                                            context) /
                                                                        1.2),
                                                            border: Border.all(
                                                                color: Color(
                                                                    0xffF1F2F7),
                                                                width: 1),
                                                            boxShadow: [
                                                              BoxShadow(
                                                                  color: Color(
                                                                      0xffF1F2F7),
                                                                  blurRadius: 0,
                                                                  spreadRadius:
                                                                      1.2,
                                                                  offset:
                                                                      Offset(
                                                                          1, 3))
                                                            ],
                                                          ),
                                                          width: fullWidth(
                                                                  context) /
                                                              3.8,
                                                          height: fullWidth(
                                                                  context) /
                                                              6,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: largeSize(
                                                                      context)),
                                                        ),
                                                      ))
                                                  .toList(),
                                            ),
                                      Spacer(),
                                      gameOver
                                          ? SizedBox()
                                          : Column(
                                              children: model.listMatchingQuizAnswer
                                                  .map((item) => DragTarget<MatchingQuizModel>(
                                                      onWillAccept:
                                                          (receivedItem) {
                                                        setState(() {
                                                          item.accepting = true;
                                                        });
                                                        return true;
                                                      },
                                                      onAccept: (receivedItem) {
                                                        if (item.name ==
                                                            receivedItem.name) {
                                                          setState(() {
                                                            model
                                                                .listMatchingQuizAnswer
                                                                .remove(
                                                                    receivedItem);
                                                            model
                                                                .listMatchingQuizAnswer
                                                                .remove(item);
                                                            score += 10;
                                                            item.accepting =
                                                                false;
                                                          });
                                                        } else {
                                                          setState(() {
                                                            score -= 5;
                                                            item.accepting =
                                                                false;
                                                            if (score < -5) {
                                                              setState(() {
                                                                gameOver = true;
                                                              });
                                                            }
                                                          });
                                                        }
                                                      },
                                                      onLeave: (receivedItem) {
                                                        setState(() {
                                                          item.accepting =
                                                              false;
                                                        });
                                                      },
                                                      builder: (context,
                                                              candidateData,
                                                              rejectedData) =>
                                                          Container(
                                                            margin: EdgeInsets.only(
                                                                top: largeSize(
                                                                    context)),
                                                            width: fullWidth(
                                                                    context) /
                                                                6,
                                                            height: fullWidth(
                                                                    context) /
                                                                6,
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius.circular(
                                                                      smallSize(
                                                                          context)),
                                                              color:
                                                                  Colors.white,
                                                              boxShadow: [
                                                                BoxShadow(
                                                                    color: Colors
                                                                        .black
                                                                        .withOpacity(
                                                                            0.16),
                                                                    blurRadius:
                                                                        7,
                                                                    spreadRadius:
                                                                        1.5,
                                                                    offset:
                                                                        Offset(
                                                                            0,
                                                                            5))
                                                              ],
                                                            ),
                                                            child: ClipRRect(
                                                              borderRadius:
                                                                  BorderRadius.circular(
                                                                      smallSize(
                                                                          context)),
                                                              child: imageWidget(
                                                                  item.image),
                                                            ),
                                                          )))
                                                  .toList(),
                                            ),
                                    ],
                                  ),
                                ],
                              )),
                        ),
            ));
  }
}
