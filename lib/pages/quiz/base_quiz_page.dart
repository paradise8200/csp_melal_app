import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/pages/quiz/choice_pic_quiz_page.dart';
import 'package:flutter_language_app/pages/quiz/swipe_up_down_quiz_page.dart';
import 'package:flutter_language_app/pages/quiz/matching_quiz_page.dart';
import 'package:flutter_language_app/pages/quiz/multiple_choice_quiz_page.dart';
import 'package:flutter_language_app/pages/quiz/new_word_quiz_page.dart';
import 'package:flutter_language_app/pages/quiz/speak_quiz_page.dart';
import 'package:flutter_language_app/pages/quiz/swip_up_down_quiz.dart';
import 'package:flutter_language_app/pages/quiz/type_english_quiz.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import 'choose_correct_item_quiz.dart';

class BaseQuizPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => BaseQuizPageState();
}

class BaseQuizPageState extends State<BaseQuizPage> {
  late PageController pageController;

  @override
  void initState() {
    pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: SizedBox(),
        elevation: 0,
        toolbarHeight: 0,
        backgroundColor: Colors.white,
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(
                right: standardSize(context),
                left: standardSize(context),
                top: mediumSize(context)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: standardSize(context) / 0.7,
                  height: standardSize(context) / 0.7,
                  decoration: BoxDecoration(
                      color: Color(0xffF1F2F7),
                      borderRadius: BorderRadius.circular(xSmallSize(context))),
                  padding: EdgeInsets.all(8),
                  child: Container(
                      child: SvgPicture.asset("assets/blue_heart.svg")),
                ),
                GestureDetector(
                  onTap: () {
                    pageController.animateToPage(
                        pageController.page!.toInt() - 1,
                        duration: Duration(milliseconds: 300),
                        curve: Curves.bounceIn);
                  },
                  child: Container(
                    width: standardSize(context) / 0.7,
                    height: standardSize(context) / 0.7,
                    decoration: BoxDecoration(
                        color: Color(0xffF1F2F7),
                        borderRadius: BorderRadius.circular(xSmallSize(context))),
                    padding: EdgeInsets.all(8),
                    child: Container(
                        child: SvgPicture.asset("assets/blue_heart.svg")),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    pageController.animateToPage(
                        pageController.page!.toInt() + 1,
                        duration: Duration(milliseconds: 300),
                        curve: Curves.bounceIn);
                  },
                  child: Container(
                    width: standardSize(context) / 0.7,
                    height: standardSize(context) / 0.7,
                    decoration: BoxDecoration(
                        color: Color(0xffF1F2F7),
                        borderRadius:
                            BorderRadius.circular(xSmallSize(context))),
                    padding: EdgeInsets.all(8),
                    child: Container(
                        child: SvgPicture.asset("assets/blue_heart.svg")),
                  ),
                ),
                Container(
                  width: fullWidth(context) / 2.5,
                  child: StepProgressIndicator(
                    totalSteps: 10,
                    currentStep: 3,
                    padding: 0,
                    unselectedColor: Color(0xffEAEAEA),
                    roundedEdges: Radius.circular(smallSize(context)),
                    selectedGradientColor: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [0.0, 1.0],
                      colors: [
                        Color(0xff3428ea),
                        Color(0xffa573ff),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    width: standardSize(context) / 0.7,
                    height: standardSize(context) / 0.7,
                    decoration: BoxDecoration(
                        color: Color(0xffF1F2F7),
                        borderRadius:
                            BorderRadius.circular(xSmallSize(context))),
                    padding: EdgeInsets.all(8),
                    child: Container(
                        padding: EdgeInsets.all(3),
                        child: SvgPicture.asset("assets/close.svg")),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: PageView(
              controller: pageController,
              physics: NeverScrollableScrollPhysics(),
              onPageChanged: (value) {
                setState(() {
                  // selectedStep = value;
                });
              },
              children: [
                SwipeUpDownQuizPage(),
                TypeEnglishQuizPage(),
                SwipUpDownQuizPage(),
                ChooseCorrectItemPage(),
                SpeakQuizPage(),
                NewWordQuizPage(),
                MatchingQuizPage(),
                MultipleChoiceQuizPage(),
                ChoicePicQuizPage(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
