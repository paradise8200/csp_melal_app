import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_language_app/models/choice_pic_quiz_model.dart';
import 'package:flutter_language_app/pages/quiz/quiz_notifier.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/image_widget.dart';
import 'package:stacked/stacked.dart';

class ChoicePicQuizPage extends StatefulWidget {
  @override
  _ChoicePicQuizPageState createState() => _ChoicePicQuizPageState();
}

class _ChoicePicQuizPageState extends State<ChoicePicQuizPage> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<QuizVM>.reactive(
        viewModelBuilder: () => QuizVM(),
        builder: (context, model, child) => Scaffold(
              appBar: AppBar(
                elevation: 0,
                toolbarHeight: 0,
                backgroundColor: Colors.white,
                automaticallyImplyLeading: false,
                leading: SizedBox(),
              ),
              body: model.isBusy
                  ? CupertinoActivityIndicator()
                  : SingleChildScrollView(
                      child: Container(
                          margin: EdgeInsets.only(
                            left: mediumSize(context),
                              right: mediumSize(context),
                              top: standardSize(context)),
                          child: Stack(

                            children: [
                              Column(
                                children: [
                                  Directionality(
                                      textDirection: TextDirection.rtl,
                                      child: Container(
                                        width: fullWidth(context),
                                        alignment: Alignment.center,
                                        child: Text(
                                          "تصویر صحیح انتخاب کن!",
                                          style:
                                              theme.textTheme.bodyText2!.copyWith(
                                            color: Colors.black,
                                            fontSize: mediumSize(context) / 1,
                                            fontWeight: FontWeight.w600,
                                            shadows: [
                                              BoxShadow(
                                                color: Color(0xff474747)
                                                    .withOpacity(0.42),
                                                spreadRadius: 2,
                                                blurRadius: 8,
                                                offset: Offset(0,
                                                    2), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                        ),
                                      )),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: mediumSize(context)),
                                    child: GridView.builder(
                                      shrinkWrap: true,
                                      padding:
                                          EdgeInsets.all(smallSize(context)),
                                      physics: NeverScrollableScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              childAspectRatio: 1 / 1,
                                              crossAxisCount: 2,
                                              crossAxisSpacing:
                                                  standardSize(context),
                                              mainAxisSpacing: 60),
                                      itemCount:
                                          model.listChoicePicQuizAnswer.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return DragTarget<ChoicePicModel>(
                                            onWillAccept: (receivedItem) {
                                              setState(() {
                                                model
                                                    .listChoicePicQuizAnswer[
                                                        index]
                                                    .accepting = true;
                                              });
                                              return true;
                                            },
                                            onAccept: (receivedItem) {
                                              if (model
                                                      .listChoicePicQuizAnswer[
                                                          index]
                                                      .name ==
                                                  receivedItem.name) {
                                                setState(() {
                                                  // model.listMatchingQuizAnswer.remove(item);
                                                  model.listMatchingQuizAnswer
                                                      .remove(index);
                                                  // score += 10;
                                                  model
                                                      .listChoicePicQuizAnswer[
                                                          index]
                                                      .accepting = false;
                                                  model
                                                      .listChoicePicQuizAnswer[
                                                          index]
                                                      .selected = true;
                                                });
                                              } else {
                                                setState(() {
                                                  // score -= 5;
                                                  model
                                                      .listChoicePicQuizAnswer[
                                                          index]
                                                      .accepting = false;
                                                  // if (score < -5) {
                                                  //   setState(() {
                                                  //     gameOver = true;
                                                  //   });
                                                  // }
                                                });
                                              }
                                            },
                                            onLeave: (receivedItem) {
                                              setState(() {
                                                model
                                                    .listChoicePicQuizAnswer[
                                                        index]
                                                    .accepting = false;
                                              });
                                            },
                                            builder:
                                                (context, candidateData,
                                                        rejectedData) =>
                                                    Container(
                                                      width:
                                                          fullWidth(context) /
                                                              8,
                                                      height:
                                                          fullWidth(context) /
                                                              8,
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                mediumSize(
                                                                    context)),
                                                        color: Colors.white,
                                                        boxShadow: [
                                                          BoxShadow(
                                                              color: Colors
                                                                  .black
                                                                  .withOpacity(
                                                                      0.16),
                                                              blurRadius: 6,
                                                              spreadRadius: 1,
                                                              offset:
                                                                  Offset(0, 3))
                                                        ],
                                                      ),
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                mediumSize(
                                                                    context)),
                                                        child: Stack(
                                                          children: [
                                                            Positioned.fill(
                                                                child: imageWidget(model
                                                                    .listChoicePicQuizAnswer[
                                                                        index]
                                                                    .image)
                                                                // imageWidget(model
                                                                //     .listChoicePicQuizAnswer[
                                                                //         index]
                                                                //     .image)
                                                                ),
                                                            model
                                                                    .listChoicePicQuizAnswer[
                                                                        index]
                                                                    .selected
                                                                ? Positioned(
                                                                    bottom: 0,
                                                                    right: 0,
                                                                    left: 0,
                                                                    child:
                                                                        Container(
                                                                      alignment:
                                                                          Alignment
                                                                              .center,
                                                                      child:
                                                                          Text(
                                                                        model
                                                                            .listChoicePicQuizAnswer[index]
                                                                            .name,
                                                                        style: theme.textTheme.bodyText2!.copyWith(
                                                                            fontFamily:
                                                                                "gilroy",
                                                                            color:
                                                                                Colors.white,
                                                                            fontWeight: FontWeight.w300),
                                                                      ),
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        color: Colors
                                                                            .black
                                                                            .withOpacity(0.3),
                                                                        borderRadius: BorderRadius.only(
                                                                            bottomRight:
                                                                                Radius.circular(mediumSize(context)),
                                                                            bottomLeft: Radius.circular(mediumSize(context))),
                                                                      ),
                                                                      width: fullWidth(
                                                                          context),
                                                                      padding: EdgeInsets.symmetric(
                                                                          vertical:
                                                                              xSmallSize(context) / 1.2),
                                                                    ),
                                                                  )
                                                                : SizedBox(),
                                                          ],
                                                        ),
                                                      ),
                                                    ));
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              Positioned(
                                top: 45,
                                right: 0,
                                left: 0,
                                bottom: 0,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Stack(
                                      alignment: Alignment.center,
                                      children: model.listChoicePicQuizAnswer
                                          .where((element) => !element.selected)
                                          .map((item) =>
                                              Draggable<ChoicePicModel>(
                                                data: item,
                                                childWhenDragging: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                          smallSize(context)),
                                                  color: Colors.white,
                                                  child: Text(item.name,
                                                      style: theme
                                                          .textTheme.bodyText1!
                                                          .copyWith(
                                                              fontFamily:
                                                                  "gilroy",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                              color:
                                                                  Colors.black,
                                                              height: 1.2)),
                                                ),
                                                feedback: Text(item.name,
                                                    style: theme
                                                        .textTheme.bodyText1!
                                                        .copyWith(
                                                            fontFamily:
                                                                "gilroy",
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            color: Colors.black,
                                                            height: 1.2)),
                                                child: Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal:
                                                          smallSize(context)),
                                                  color: Colors.white,
                                                  child: Text(item.name,
                                                      style: theme
                                                          .textTheme.bodyText1!
                                                          .copyWith(
                                                              fontFamily:
                                                                  "gilroy",
                                                              color:
                                                                  Colors.black,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              height: 1.2)),
                                                ),
                                              ))
                                          .toList(),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )),
                    ),
            ));
  }
}
