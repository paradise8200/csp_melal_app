// import 'package:flutter/material.dart';
// import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
//
// class ConversationPage extends StatefulWidget {
//   // final ChatGroups groupInfo;
//
//   // ConversationPage(this.groupInfo);
//
//   @override
//   _ConversationPageState createState() => _ConversationPageState();
// }
//
// final TextEditingController textEditingController = new TextEditingController();
// bool? isSearch;
// PagingController? _pagingController;
//
// final ScrollController listScrollController = new ScrollController();
//
// class _ConversationPageState extends State<ConversationPage> {
//   TextEditingController? _sendMessagesController;
//   List<Asset> images = List<Asset>();
//   String _error = 'No Error Dectected';
//
//   // FocusNode _focusNode;
//
//   @override
//   void initState() {
//     super.initState();
//     _sendMessagesController = TextEditingController();
//     // _focusNode = FocusNode();
//   }
//
//   void onEmojiSelected(Emoji emoji) {
//     _sendMessagesController.text += emoji.text;
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     // _focusNode.dispose();
//   }
//
//   Future<void> loadAssets() async {
//     List<Asset> resultList = List<Asset>();
//     String error = 'No Error Dectected';
//
//     try {
//       resultList = await MultiImagePicker.pickImages(
//         maxImages: 300,
//         enableCamera: true,
//         selectedAssets: images,
//         cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
//         materialOptions: MaterialOptions(
//           actionBarColor: "#141415",
//           actionBarTitle: "Select image",
//           allViewTitle: "All Photos",
//           useDetailsView: true,
//           selectCircleStrokeColor: "#000000",
//         ),
//       );
//     } on Exception catch (e) {
//       error = e.toString();
//     }
//
//     if (!mounted) return;
//
//     setState(() {
//       //todo send image
//       images = resultList;
//       _error = error;
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     var theme = Theme.of(context);
//     var language = Languages.of(context);
//
//     return ViewModelBuilder<GroupConversationVm>.reactive(
//         disposeViewModel: false,
//         viewModelBuilder: () => GroupConversationVm(widget.groupInfo),
//         builder: (context, model, child) {
//           // model.isBusy ? MyLoading() : _sendMessagesController.text=model.data. ;
//           // model.isBusy?MyLoading(): _sendMessagesController.text =model.data.messages[0].message;
//           return Scaffold(
//             backgroundColor: theme.scaffoldBackgroundColor,
//             bottomNavigationBar: Container(
//                 decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(standardSize(context)),
//                     color:
//                     pref.isDark ? theme.backgroundColor : theme.cardColor),
//                 margin: EdgeInsets.symmetric(
//                     horizontal: smallSize(context),
//                     vertical: mediumSize(context)),
//                 child: Row(
//                   children: [
//                     SizedBox(
//                       width: xxSmallSize(context),
//                     ),
//                     Container(
//                         padding: EdgeInsets.all(7),
//                         width: fullWidth(context) / 10,
//                         height: fullWidth(context) / 10,
//                         decoration: BoxDecoration(
//                             color: theme.primaryColor,
//                             borderRadius:
//                             BorderRadius.circular(standardSize(context))),
//                         child: SvgPicture.asset(
//                           ic_camera_light,
//                         )),
//                     SizedBox(
//                       width: xxSmallSize(context),
//                     ),
//                     Expanded(
//                       child: new TextFormField(
//                         cursorColor: theme.primaryColor,
//                         style: Theme.of(context).textTheme.bodyText2,
//                         controller: _sendMessagesController,
//                         onChanged: (value) {
//                           setState(() {});
//                         },
//                         textInputAction: TextInputAction.send,
//                         decoration: new InputDecoration(
//                             contentPadding: EdgeInsets.symmetric(
//                                 vertical: mediumSize(context),
//                                 horizontal: xSmallSize(context)),
//                             hintText: language.txtMessage,
//                             hintStyle: theme.textTheme.bodyText2.copyWith(
//                                 color: pref.isDark
//                                     ? Color(0xffffffff).withOpacity(0.50)
//                                     : Color(0xff474747).withOpacity(0.50)),
//                             border: InputBorder.none),
//                       ),
//                     ),
//                     _sendMessagesController.text != null &&
//                         _sendMessagesController.text.trim().isNotEmpty
//                         ? InkWell(
//                       onTap: () {
//                         model.sendMessages(
//                             _sendMessagesController.text.trim(),
//                             ChatMessageType.message);
//                         _sendMessagesController.clear();
//                       },
//                       child: Container(
//                         // padding: EdgeInsets.all(7),
//                         // width: fullWidth(context) / 10,
//                         // height: fullWidth(context) / 10,
//                         // decoration: BoxDecoration(
//                         //     color: Color(0xff1DC5CB),
//                         //     borderRadius: BorderRadius.circular(
//                         //         standardSize(context))),
//                           margin: EdgeInsets.only(
//                               right: standardSize(context) / 1.2),
//                           child: SvgPicture.asset(
//                             "assets/images/Send.svg",
//                             color: theme.primaryColor,
//                           )),
//                     )
//                         : Row(
//                       children: [
//                         Material(
//                           color: Colors.transparent,
//                           child: InkWell(
//                             borderRadius:
//                             BorderRadius.all(Radius.circular(200)),
//                             onTap: () {
//                               //todo show recording State
//                               FocusScope.of(context)
//                                   .requestFocus(FocusNode());
//                             },
//                             child: Container(
//                               padding: EdgeInsets.symmetric(
//                                   horizontal: smallSize(context),
//                                   vertical: smallSize(context)),
//                               child: new SvgPicture.asset(
//                                 ic_voice,
//                                 width: iconSizeLarge(context),
//                                 height: iconSizeLarge(context),
//                                 color: pref.isDark
//                                     ? Color(0xffAEAEAE)
//                                     : Color(0xffB0B1B2),
//                               ),
//                             ),
//                           ),
//                         ),
//                         Material(
//                           color: Colors.transparent,
//                           child: InkWell(
//                             onTap: () {
//                               FocusScope.of(context)
//                                   .requestFocus(FocusNode());
//
//                               loadAssets();
//                             },
//                             borderRadius:
//                             BorderRadius.all(Radius.circular(100)),
//                             child: Container(
//                               padding: EdgeInsets.symmetric(
//                                   horizontal: smallSize(context),
//                                   vertical: smallSize(context)),
//                               child: new SvgPicture.asset(
//                                 ic_gallery,
//                                 color: pref.isDark
//                                     ? Color(0xffAEAEAE)
//                                     : Color(0xffB0B1B2),
//                                 width: iconSizeLarge(context),
//                                 height: iconSizeLarge(context),
//                               ),
//                             ),
//                           ),
//                         ),
//                         Material(
//                           color: Colors.transparent,
//                           child: InkWell(
//                             borderRadius:
//                             BorderRadius.all(Radius.circular(100)),
//                             onTap: () async {
//                               //  final gif = await GiphyPicker.pickGif(
//                               //   context: context,
//                               //   apiKey: '3dXYghLK19UXIiyrUgSfJLYPo5qIfXdb',
//                               //   fullScreenDialog: false,
//                               //   previewType: GiphyPreviewType.previewWebp,
//                               //   decorator: GiphyDecorator(
//                               //     showAppBar: false,
//                               //     searchElevation: 4,
//                               //     giphyTheme: ThemeData.dark().copyWith(
//                               //       inputDecorationTheme: InputDecorationTheme(
//                               //         border: InputBorder.none,
//                               //         enabledBorder: InputBorder.none,
//                               //         focusedBorder: InputBorder.none,
//                               //         contentPadding: EdgeInsets.zero,
//                               //       ),
//                               //     ),
//                               //   ),
//                               // );
//
//                               // if (gif != null) {
//                               //   setState(() => _gif = gif);
//                               // }
//
//                               // showModalBottomSheet(
//                               //   context: context,
//                               //   shape: RoundedRectangleBorder(
//                               //       borderRadius:
//                               //       BorderRadius.vertical(top: Radius.circular(20.0))),
//                               //   backgroundColor: Colors.white,
//                               //   isScrollControlled: true,
//                               //   builder: (BuildContext subcontext) {
//                               //     return Container(
//                               //       decoration: BoxDecoration(borderRadius: BorderRadius.circular(12) ),
//                               //       child: EmojiKeyboard(
//                               //         floatingHeader: false,
//                               //         color: Theme.of(context).backgroundColor,
//                               //         onEmojiSelected: (Emoji emoji){
//                               //           setState(() {
//                               //             onEmojiSelected(emoji);
//                               //
//                               //           });
//                               //         },
//                               //       ),
//                               //     );
//                               //   },
//                               // );
//                               ///todo open emoji page
//                             },
//                             child: Container(
//                               padding: EdgeInsets.symmetric(
//                                   horizontal: smallSize(context),
//                                   vertical: smallSize(context)),
//                               child: new SvgPicture.asset(
//                                 ic_smile,
//                                 color: pref.isDark
//                                     ? Color(0xffAEAEAE)
//                                     : Color(0xffB0B1B2),
//                                 width: iconSizeLarge(context),
//                                 height: iconSizeLarge(context),
//                               ),
//                             ),
//                           ),
//                         )
//                       ],
//                     ),
//                   ],
//                 )),
//             resizeToAvoidBottomInset: true,
//             appBar:
//             // ChatAppBar()
//             AppBar(
//               shadowColor: Color(0xff455B63).withOpacity(0.08),
//               backgroundColor: theme.backgroundColor,
//               title: Row(
//                 children: [
//                   Container(
//                     height: fullWidth(context) / 12,
//                     width: fullWidth(context) / 12,
//                     child: customAvatarSmallSize(
//                         context, widget.groupInfo.shortInfo.avatar,
//                         hasBorder: false),
//                   ),
//                   Flexible(
//                     child: GestureDetector(
//                       onTap: model.busy(model.isBusyLazy)? null:() =>Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) => model.lastPageResult.info.chatType=="Group"?GroupInfoPage(model):InfoMemberPage(model.userID))),
//                       child: Container(
//                         margin:
//                         EdgeInsets.only(left: xSmallSize(context) / 1.2),
//                         child: Text(
//                           widget.groupInfo.shortInfo.title ?? "",
//                           overflow: TextOverflow.ellipsis,
//                           style: theme.textTheme.bodyText1.copyWith(
//                               color: pref.isDark
//                                   ? Colors.white
//                                   : Color(0xff474747)),
//                           maxLines: 1,
//                         ),
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//               centerTitle: false,
//               actions: [
//                 Row(children: [
//                   Visibility(
//                     visible: model.groupInfo.flagged == "0" ? true : false,
//                     child: IconButton(
//                       splashColor: splashColor(pref.isDark),
//                       splashRadius: standardSize(context) / 1.1,
//                       icon: SvgPicture.asset(ic_flaged,
//                           width: iconSize(context),
//                           height: iconSize(context),
//                           color: Color(0xffFFC107)),
//                       onPressed: () {},
//                     ),
//                   ),
//                   Container(
//                     margin: EdgeInsets.only(right: xxSmallSize(context)),
//                     child: IconButton(
//                       splashColor: splashColor(pref.isDark),
//                       splashRadius: standardSize(context) / 1.1,
//                       icon: SvgPicture.asset(ic_info,
//                           width: iconSizeLarge(context),
//                           height: iconSizeLarge(context),
//                           color:
//                           pref.isDark ? Colors.white : Color(0xff474747)),
//                       onPressed: () {
//                         Navigator.push(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) =>  model.lastPageResult.info.chatType=="Group"?GroupInfoPage(model):InfoMemberPage(model.lastPageResult.messages[1].id))
//
//                         );
//                       },
//                     ),
//                   )
//                 ])
//               ],
//               leading: backIcon(context),
//             ),
//             body: Stack(children: [
//               model.busy(model.isBusyLazy)
//                   ? MyLoading()
//                   : Container(
//                 // height: fullHeight(context)/1.3,
//                 child: ListView.builder(
//                   physics: BouncingScrollPhysics(),
//                   shrinkWrap: true,
//                   padding: EdgeInsets.only(
//                     top: xSmallSize(context),
//                     right: xxSmallSize(context),
//                   ),
//                   itemBuilder: (context, index) => index % 2 == 0
//                       ?
//                   //This is the sent message. We'll later use data from firebase instead of index to determine the message is sent or received.
//                   Container(
//                       child: Column(children: [
//                         Row(
//                           children: <Widget>[
//                             Flexible(
//                               child: Container(
//                                 child: Text(
//                                   model.messages[index].message =
//                                       _sendMessagesController
//                                           .value.text,
//                                   // "Thanks 😐❤️",
//                                   style: theme.textTheme.bodyText2
//                                       .copyWith(color: Colors.white),
//                                 ),
//                                 padding: EdgeInsets.fromLTRB(
//                                     smallSize(context),
//                                     xSmallSize(context),
//                                     smallSize(context),
//                                     xSmallSize(context)),
//                                 width: fullWidth(context) / 2,
//                                 // width: 200.0,
//                                 decoration: BoxDecoration(
//                                   borderRadius: BorderRadius.only(
//                                     topLeft: Radius.circular(
//                                         standardSize(context)),
//                                     topRight: Radius.circular(
//                                         standardSize(context)),
//                                     bottomRight: Radius.circular(
//                                         xxSmallSize(context)),
//                                     bottomLeft: Radius.circular(
//                                         standardSize(context)),
//                                   ),
//                                   gradient: LinearGradient(
//                                       begin: Alignment.centerLeft,
//                                       end: Alignment.centerRight,
//                                       colors: [
//                                         Color(0xff6FFADF),
//                                         Color(0xff1EC6CB)
//                                       ]),
//                                 ),
//                                 margin: EdgeInsets.only(
//                                     right: xSmallSize(context)),
//                               ),
//                             )
//                           ],
//                           mainAxisAlignment: MainAxisAlignment
//                               .end, // aligns the chatitem to right end
//                         ),
//                         Row(
//                             mainAxisAlignment: MainAxisAlignment.end,
//                             children: <Widget>[
//                               Container(
//                                 child: Text(
//                                   "${model.messages[index].createdAt ?? "createdAt"}",
//                                   // DateFormat('dd MMM kk:mm').format(
//                                   //     DateTime
//                                   //         .fromMillisecondsSinceEpoch(
//                                   //             1565888474278)),
//                                   style: theme.textTheme.caption
//                                       .copyWith(
//                                       color: pref.isDark
//                                           ? Color(0xffA0A0A0)
//                                           .withOpacity(0.77)
//                                           : Color(0xffAEAEAE)
//                                           .withOpacity(0.77),
//                                       fontStyle: FontStyle.normal),
//                                 ),
//                                 margin:
//                                 EdgeInsets.all(xSmallSize(context)),
//                               )
//                             ])
//                       ]))
//                       :
//
//                   // This is a received message
//                   Container(
//                     padding: EdgeInsets.only(
//                       left: xxSmallSize(context),
//                     ),
//                     child: Column(
//                       children: <Widget>[
//                         Row(
//                           children: <Widget>[
//                             Container(
//                               child: Container(
//                                   width: fullWidth(context) / 12.8,
//                                   height: fullWidth(context) / 12.8,
//                                   child: customAvatarSmallSize(
//                                       context,
//                                       "$url_avatar_user_base${model.messages[index].sender.id}/${model.messages[index].sender.avatar}",
//                                       hasBorder: false)),
//                             ),
//                             Container(
//                               child: Text(
//                                 model.messages[index].message,
//                                 style: Theme.of(context)
//                                     .textTheme
//                                     .bodyText2
//                                     .copyWith(
//                                     color: pref.isDark
//                                         ? Colors.white
//                                         .withOpacity(0.77)
//                                         : Color(0xff474747)
//                                         .withOpacity(0.77)),
//                               ),
//                               padding: EdgeInsets.fromLTRB(
//                                   smallSize(context),
//                                   xSmallSize(context),
//                                   smallSize(context),
//                                   xSmallSize(context)),
//                               width: fullWidth(context) / 2,
//                               decoration: BoxDecoration(
//                                 borderRadius: BorderRadius.only(
//                                   topRight: Radius.circular(
//                                       standardSize(context)),
//                                   topLeft: Radius.circular(
//                                       standardSize(context)),
//                                   bottomRight: Radius.circular(
//                                       standardSize(context)),
//                                   bottomLeft: Radius.circular(
//                                       xxSmallSize(context)),
//                                 ),
//                                 gradient: LinearGradient(
//                                     begin: Alignment.centerLeft,
//                                     end: Alignment.centerRight,
//                                     colors: [
//                                       theme.backgroundColor,
//                                       theme.backgroundColor
//                                     ]),
//                               ),
//                               margin: EdgeInsets.only(
//                                   left: xSmallSize(context)),
//                             )
//                           ],
//                         ),
//                         Container(
//                           child: Text(
//                             model.messages[index].createdAt,
//                             style: theme.textTheme.caption.copyWith(
//                                 color: pref.isDark
//                                     ? Color(0xffA0A0A0)
//                                     .withOpacity(0.77)
//                                     : Color(0xffAEAEAE)
//                                     .withOpacity(0.77),
//                                 fontStyle: FontStyle.normal),
//                           ),
//                           margin:
//                           EdgeInsets.all(xSmallSize(context)),
//                         )
//                       ],
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                     ),
//                     margin: EdgeInsets.only(
//                         bottom: xSmallSize(context)),
//                   ),
//                   // ChatItemWidget(index),
//                   itemCount: model.messages.length,
//                   // reverse: true,
//                   controller: listScrollController,
//                 ),
//               ),
//
//               // Align(
//               //     alignment: Alignment.bottomCenter,
//               //     child: InputWidget())
//             ]),
//           );
//         });
//   }
// }