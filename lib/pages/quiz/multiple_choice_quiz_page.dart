import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/models/matching_quiz_model.dart';
import 'package:flutter_language_app/pages/quiz/quiz_notifier.dart';
import 'package:flutter_language_app/pages/quiz/widgets/multi_choise_widget.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/image_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stacked/stacked.dart';

class MultipleChoiceQuizPage extends StatefulWidget {
  @override
  _MatchingQuizPageState createState() => _MatchingQuizPageState();
}

class _MatchingQuizPageState extends State<MultipleChoiceQuizPage> {
  late int score;

  bool gameOver = false;


  @override
  void initState() {
    super.initState();
    initGame();
  }


  initGame() {
    score = 0;
    gameOver = false;
  }


  @override
  Widget build(BuildContext context) {
    var theme =Theme.of(context);
    return ViewModelBuilder<QuizVM>.reactive(
        viewModelBuilder: () => QuizVM(),
        builder: (context, model, child) => Scaffold(

            appBar: AppBar(backgroundColor: Colors.white,
              toolbarHeight: 0,
              elevation: 0,
            ),
            body:Container(
              margin: EdgeInsets.only(top: smallSize(context)),
              width:fullWidth(context),
              height: fullHeight(context),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      // alignment: Alignment.center,
                      // width: fullWidth(context),
                        margin: EdgeInsets.symmetric(vertical: smallSize(context)),

                        child:
                        Text("!گزینه صحیح را انتخاب کنید",
                          textAlign: TextAlign.center,
                          style: theme.textTheme.bodyText2!.copyWith(
                              color: Colors.black,
                              fontSize: mediumSize(context) / 1,//
                              fontWeight: FontWeight.w600,
                              shadows: [
                                BoxShadow(
                                  color: Color(0xff474747).withOpacity(0.42),
                                  spreadRadius: 2,
                                  blurRadius: 8,
                                  offset: Offset(0, 2), // changes position of shadow

                                )] ),
                        )),
                    Container(
                      height: fullWidth(context) / 1.4,
                      width: fullWidth(context) / 1.3,
                      margin: EdgeInsets.symmetric(horizontal: smallSize(context),vertical: mediumSize(context)),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(xlargeSize(context) / 1.4),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xff7f77fe).withOpacity(0.50),
                              blurRadius: 10,
                              spreadRadius: 3,
                              offset: Offset(0, 4))
                        ],
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(xlargeSize(context) / 1.4),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              child: imageWidget(
                                  "https://s4.uupload.ir/files/sister_t4ao.jpg",
                                  fit: BoxFit.cover),
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                alignment: Alignment.center,
                                child: Text(
                                  "زن , بانو",
                                  style: theme.textTheme.bodyText2!.copyWith(
                                      color: Colors.white, fontWeight: FontWeight.w700),
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.30),
                                  borderRadius: BorderRadius.only(
                                      bottomRight:
                                      Radius.circular(xlargeSize(context) / 1.4),
                                      bottomLeft:
                                      Radius.circular(xlargeSize(context) / 1.4)),
                                ),
                                width: fullWidth(context),
                                height: fullWidth(context) / 6.5,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    MultiChoiceWidget("Family"),
                    MultiChoiceWidget("Woman"),
                    MultiChoiceWidget("Happy"),
                    MultiChoiceWidget("Aunt"),


                  ],),
              ),
            )
        ));
  }
}

