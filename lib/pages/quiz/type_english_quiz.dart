import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/models/matching_quiz_model.dart';
import 'package:flutter_language_app/pages/quiz/quiz_notifier.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/widgets/image_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stacked/stacked.dart';

class TypeEnglishQuizPage extends StatefulWidget {
  @override
  _MatchingQuizPageState createState() => _MatchingQuizPageState();
}

class _MatchingQuizPageState extends State<TypeEnglishQuizPage> {
  late int score;

  bool gameOver = false;

  @override
  void initState() {
    super.initState();
    initGame();
  }

  initGame() {
    score = 0;
    gameOver = false;
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return ViewModelBuilder<QuizVM>.reactive(
        viewModelBuilder: () => QuizVM(),
        builder: (context, model, child) => Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              toolbarHeight: 0,
              elevation: 0,
            ),
            body: Container(
              margin: EdgeInsets.only(top: smallSize(context)),
              width: fullWidth(context),
              height: fullHeight(context),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                        // alignment: Alignment.center,
                        // width: fullWidth(context),
                        margin:
                            EdgeInsets.symmetric(vertical: smallSize(context)),
                        child: Text(
                          "!به انگلیسی بنویس",
                          textAlign: TextAlign.center,
                          style: theme.textTheme.bodyText2!.copyWith(
                              color: Colors.black,
                              fontSize: mediumSize(context) / 1, //
                              fontWeight: FontWeight.w600,
                              shadows: [
                                BoxShadow(
                                  color: Color(0xff474747).withOpacity(0.42),
                                  spreadRadius: 2,
                                  blurRadius: 8,
                                  offset: Offset(
                                      0, 2), // changes position of shadow
                                )
                              ]),
                        )),
                    Container(
                      height: fullWidth(context) / 1.4,
                      width: fullWidth(context) / 1.3,
                      margin: EdgeInsets.symmetric(
                          horizontal: smallSize(context),
                          vertical: mediumSize(context)),
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(xlargeSize(context) / 1.4),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xff7f77fe).withOpacity(0.50),
                              blurRadius: 10,
                              spreadRadius: 3,
                              offset: Offset(0, 4))
                        ],
                      ),
                      child: ClipRRect(
                        borderRadius:
                            BorderRadius.circular(xlargeSize(context) / 1.4),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              child: Container(
                                decoration: BoxDecoration(),
                                child: imageWidget(
                                    "https://s4.uupload.ir/files/thankes_ktlq.jpg"),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                                    Container(
                                      color: Colors.blue,
                                      width: fullWidth(context)/1.1 ,
                                      height: fullWidth(context)/6 ,
                                      child:GridView.builder(

                                        scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        padding: EdgeInsets.all(smallSize(context)),
                                        physics: BouncingScrollPhysics(),
                                        gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            childAspectRatio: 1/1,
                                            crossAxisCount: 1,
                                            crossAxisSpacing: standardSize(context),
                                            ),
                                        itemCount: model.answerWord.length,
                                        itemBuilder: (BuildContext context, int index) {
                                          return DragTarget<WordModel>(

                                              onWillAccept: (receivedItem) {
                                                setState(() {


                                                });
                                                return true;
                                              },
                                              onAccept: (receivedItem) {
                                                setState(() {
                                                  model.answerWord[index].isSelectedd=true;

                                                });
                                         /*       if (model.answerWord[index]
                                                    .wordAnswer ==
                                                    receivedItem.wordAnswer) {
                                                  setState(() {
                                                    // model.listMatchingQuizAnswer.remove(item);
                                                    model.answerWord
                                                        .remove(index);
                                                    // score += 10;

                                                    model.answerWord[index]
                                                        .isSelectedd = true;
                                                  });
                                                } else {
                                                  setState(() {
                                                    // score -= 5;

                                                    // if (score < -5) {
                                                    //   setState(() {
                                                    //     gameOver = true;
                                                    //   });
                                                    // }
                                                  });
                                                }*/
                                              },
                                              onLeave: (receivedItem) {
                                                setState(() {

                                                });
                                              },
                                              // children: model.answerWord.where((element) => !element.isSelectedd).map((item) =>

                                              builder:
                                                  (context, candidateData,
                                                  rejectedData) {
                                                  return Stack(
                                                    children: [

                                                      model
                                                          .answerWord[
                                                      index]
                                                          .isSelectedd
                                                          ? Positioned(
                                                        bottom: 0,
                                                        right: 0,
                                                        left: 0,
                                                        child: Container(
                                                          alignment:
                                                          Alignment
                                                              .center,
                                                          child:  Text(
                                                            // model.getDataWordModel().toString(),

                                                            // model.answerWord.where((element) => !element.isSelectedd).map((item) =>
                                                            model.answerWord[index].wordAnswer,
                                                            style: theme
                                                                .textTheme
                                                                .bodyText2!
                                                                .copyWith(
                                                                fontFamily:
                                                                "gilroy",
                                                                color: Colors
                                                                    .white,
                                                                fontWeight:
                                                                FontWeight.w700),
                                                          ),
                                                          decoration:
                                                          BoxDecoration(
                                                            color: Colors
                                                                .black
                                                                .withOpacity(
                                                                0.30),
                                                            borderRadius: BorderRadius.only(
                                                                bottomRight:
                                                                Radius.circular(mediumSize(
                                                                    context)),
                                                                bottomLeft:
                                                                Radius.circular(
                                                                    mediumSize(context))),
                                                          ),
                                                          width: fullWidth(context),),
                                                      )
                                                          : Container(width: fullWidth(context),
                                                      height: fullWidth(context)/5,
                                                        color: Colors.red,
                                                      ),
                                                    ],
                                                  );});
                                          },
                                      ),

                                    ),
                    Wrap(
                      direction: Axis.horizontal,
                      children: [
                        Wrap(
                          direction: Axis.horizontal,
                          children: model.answerWord.where((element) => !element.isSelectedd).map((item) =>
                              Draggable<WordModel>(
                                    data: item,
                                    childWhenDragging: Container(
                                      alignment: Alignment.center,
                                      child: Text(item.wordAnswer,
                                          style: theme.textTheme.bodyText1!
                                              .copyWith(
                                                  fontFamily: "gilroy",
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.white,
                                                  height: 1.2)),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            mediumSize(context) / 1.2),
                                        gradient: LinearGradient(
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                          stops: [0.0, 1.0],
                                          colors: [
                                            Color(0xff3428ea),
                                            Color(0xffa573ff),
                                          ],
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Color(0xff9D6EFD),
                                              blurRadius: 4,
                                              offset: Offset(0, 0))
                                        ],
                                      ),
                                      width: fullWidth(context) / 3.8,
                                      height: fullWidth(context) / 6,
                                      margin: EdgeInsets.only(
                                          top: largeSize(context)),
                                    ),
                                    feedback: Container(
                                      margin: EdgeInsets.only(
                                          top: largeSize(context)),
                                      alignment: Alignment.center,
                                      child: Text(item.wordAnswer,
                                          style: theme.textTheme.bodyText1!
                                              .copyWith(
                                                  fontFamily: "gilroy",
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.white,
                                                  height: 1.2)),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            mediumSize(context) / 1.2),
                                        gradient: LinearGradient(
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight,
                                          stops: [0.0, 1.0],
                                          colors: [
                                            Color(0xff3428ea),
                                            Color(0xffa573ff),
                                          ],
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Color(0xff9D6EFD),
                                              blurRadius: 4,
                                              offset: Offset(0, 0))
                                        ],
                                      ),
                                      width: fullWidth(context) / 3.8,
                                      height: fullWidth(context) / 6,
                                    ),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      children: [
                                        Container(
                                          alignment: Alignment.center,
                                          child: Text(item.wordAnswer,
                                              style: theme.textTheme.bodyText1!
                                                  .copyWith(
                                                      fontFamily: "gilroy",
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      height: 1.2)),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(
                                                mediumSize(context) / 1.2),
                                            border: Border.all(
                                                color: Color(0xffF1F2F7),
                                                width: 1),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Color(0xffF1F2F7),
                                                  blurRadius: 0,
                                                  spreadRadius: 1.2,
                                                  offset: Offset(1, 3))
                                            ],
                                          ),
                                          width: fullWidth(context) / 3.8,
                                          height: fullWidth(context) / 6,
                                          margin: EdgeInsets.only(
                                              top: largeSize(context)),
                                        ),
                                      ],
                                    ),
                                  ))
                              .toList(),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )));
  }
}
