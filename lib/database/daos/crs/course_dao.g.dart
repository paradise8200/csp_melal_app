// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'course_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$CourseDaoMixin on DatabaseAccessor<AppDatabase> {
  $CourseTable get course => attachedDatabase.course;
}
