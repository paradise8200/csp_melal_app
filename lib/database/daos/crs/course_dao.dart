
import 'package:flutter_language_app/database/database.dart';
import 'package:flutter_language_app/database/tables/cor/course_table.dart';
import 'package:moor/moor.dart';

part 'course_dao.g.dart';

@UseDao(tables: [Course])
class CourseDao extends DatabaseAccessor<AppDatabase> with _$CourseDaoMixin {
  CourseDao(AppDatabase db) : super(db);

  Future<int> insertItem(CourseCompanion entry) {
    try {
      return into(attachedDatabase.course)
          .insert(entry, onConflict: DoUpdate((old) => CourseCompanion()));
    } catch (e) {
      print("catch in inset :input:$entry e:$e");
      throw ("خطا در انجام عملیات لطفا بعدا تلاش کنید");
    }
  }

  Future updateItem(CourseCompanion entity) =>
      update(attachedDatabase.course).replace(entity);

  Stream<List<CourseData>> get watchAll =>
      select(attachedDatabase.course).watch();


  Stream<List<CourseData>> getLazy(int offset, {int limit = 25}) {
    return (select(attachedDatabase.course)
      // ..where((t) => t..isNull())
      ..limit(limit, offset: offset)
      ..orderBy([
            (t) =>
            OrderingTerm(expression: t.updateDate, mode: OrderingMode.desc)
      ]))
        .watch();
  }

  Future<List<CourseData>> getAll()async {
    return await (select(attachedDatabase.course)
    // ..where((t) => t..isNull())
      ..orderBy([
            (t) =>
            OrderingTerm(expression: t.updateDate, mode: OrderingMode.desc)
      ]))
        .get();

  }


  Future<CourseData> getByID(int id) async {
    return await (select(attachedDatabase.course)
      ..where((t) => t.id.equals(id)))
        .getSingle();
  }

  Future<int> clearTable() async {
    try {
      return await (delete(attachedDatabase.course).go());
    } catch (e) {
      print("error is $e");
      return 0;
    }
  }
}
