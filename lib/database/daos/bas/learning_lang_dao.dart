import 'package:flutter_language_app/database/database.dart';
import 'package:flutter_language_app/database/tables/bas/learning_lang_table.dart';
import 'package:moor/moor.dart';

part 'learning_lang_dao.g.dart';

@UseDao(tables: [LearningLang])
class LearningLangDao extends DatabaseAccessor<AppDatabase>
    with _$LearningLangDaoMixin {
  LearningLangDao(AppDatabase db) : super(db);

  Future<int> addAddress(LearningLangCompanion entry) {
    try {
      return into(attachedDatabase.learningLang).insert(entry,
          onConflict: DoUpdate((old) => LearningLangCompanion()));
    } catch (e) {
      print("catch in inset :input:$entry e:$e");
      throw ("خطا در انجام عملیات لطفا بعدا تلاش کنید");
    }
  }

  Future<int> insertx(LearningLangCompanion entity) {
    try {
      return into(attachedDatabase.learningLang)
          .insert(entity, mode: InsertMode.insertOrReplace);
    } catch (e) {
      print("catch in addAddress: $e");
      throw ("خطا در انجام عملیات لطفا بعدا تلاش کنید");
    }
  }

  Future updates(LearningLangCompanion entity) =>
      update(attachedDatabase.learningLang).replace(entity);

  Stream<List<LearningLangData>> get watchAll =>
      select(attachedDatabase.learningLang).watch();

  Future<LearningLangData> getByID(int id) async {
    return await (select(attachedDatabase.learningLang)
          ..where((t) => t.id.equals(id)))
        .getSingle();
  }

  Future<int> clearTable() async {
    try {
      return await (delete(attachedDatabase.learningLang).go());
    } catch (e) {
      print("error is $e");
      return 0;
    }
  }
}

