// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'learning_lang_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$LearningLangDaoMixin on DatabaseAccessor<AppDatabase> {
  $LearningLangTable get learningLang => attachedDatabase.learningLang;
}
