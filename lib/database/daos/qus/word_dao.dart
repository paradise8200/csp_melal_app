
import 'package:flutter_language_app/database/database.dart';
import 'package:flutter_language_app/database/tables/qus/word_table.dart';
import 'package:moor/moor.dart';

part 'word_dao.g.dart';


@UseDao(tables: [Word])
class WordDao extends DatabaseAccessor<AppDatabase> with _$WordDaoMixin {
  WordDao(AppDatabase db) : super(db);

  Future<int> insertItem(WordCompanion entry) {
    try {
      return into(attachedDatabase.word)
          .insert(entry, onConflict: DoUpdate((old) => WordCompanion()));
    } catch (e) {
      print("catch in inset :input:$entry e:$e");
      throw ("خطا در انجام عملیات لطفا بعدا تلاش کنید");
    }
  }

  Future updateItem(WordCompanion entity) =>
      update(attachedDatabase.word).replace(entity);

  Stream<List<WordData>> get watchAll =>
      select(attachedDatabase.word).watch();


  Future<List<WordData>> getLazy(int offset, {int limit = 25}) {
    return (select(attachedDatabase.word)
    // ..where((t) => t..isNull())
      ..limit(limit, offset: offset)
      ..orderBy([
            (t) =>
            OrderingTerm(expression: t.updateDate, mode: OrderingMode.desc)
      ]))
        .get();
  }


  Future<WordData> getByID(int id) async {
    return await (select(attachedDatabase.word)
      ..where((t) => t.id.equals(id)))
        .getSingle();
  }

  Future<int> clearTable() async {
    try {
      return await (delete(attachedDatabase.word).go());
    } catch (e) {
      print("error is $e");
      return 0;
    }
  }
}
