import 'package:flutter_language_app/utilities/date_time_controller.dart';
import 'package:moor/moor.dart';

class Question extends Table {
  IntColumn get id => integer()();

  IntColumn get idLocal => integer().autoIncrement()();

  IntColumn get orderID => integer().withDefault(const Constant(0))();

  DateTimeColumn get createDate =>
      dateTime().withDefault(Constant(getNowDateTime()))();

  DateTimeColumn get updateDate =>
      dateTime().withDefault(Constant(getNowDateTime()))();

  IntColumn get createBy => integer()();

  IntColumn get updateBy => integer()();

  TextColumn get title => text()();

  TextColumn get description => text().withDefault(const Constant(""))();

  TextColumn get imagePath => text().withDefault(const Constant(""))();

  BoolColumn get isActive => boolean().withDefault(const Constant(true))();

  BoolColumn get hasApprovedContent =>
      boolean().withDefault(const Constant(false))();

  BoolColumn get hasApprovedIT =>
      boolean().withDefault(const Constant(false))();

  BoolColumn get isSelected => boolean().withDefault(const Constant(false))();

//****************************************************************

}
