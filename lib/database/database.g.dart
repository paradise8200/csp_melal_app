// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class LearningLangData extends DataClass
    implements Insertable<LearningLangData> {
  final int id;
  final int idLocal;
  final int orderID;
  final DateTime createDate;
  final DateTime updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;
  final bool isActive;
  final bool hasApprovedContent;
  final bool hasApprovedIT;
  final bool isSelected;
  final String iconPath;
  LearningLangData(
      {required this.id,
      required this.idLocal,
      required this.orderID,
      required this.createDate,
      required this.updateDate,
      required this.createBy,
      required this.updateBy,
      required this.title,
      required this.description,
      required this.imagePath,
      required this.isActive,
      required this.hasApprovedContent,
      required this.hasApprovedIT,
      required this.isSelected,
      required this.iconPath});
  factory LearningLangData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return LearningLangData(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      idLocal: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id_local'])!,
      orderID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}order_i_d'])!,
      createDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_date'])!,
      updateDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_date'])!,
      createBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_by'])!,
      updateBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_by'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      imagePath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image_path'])!,
      isActive: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_active'])!,
      hasApprovedContent: const BoolType().mapFromDatabaseResponse(
          data['${effectivePrefix}has_approved_content'])!,
      hasApprovedIT: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}has_approved_i_t'])!,
      isSelected: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_selected'])!,
      iconPath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}icon_path'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['id_local'] = Variable<int>(idLocal);
    map['order_i_d'] = Variable<int>(orderID);
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    map['create_by'] = Variable<int>(createBy);
    map['update_by'] = Variable<int>(updateBy);
    map['title'] = Variable<String>(title);
    map['description'] = Variable<String>(description);
    map['image_path'] = Variable<String>(imagePath);
    map['is_active'] = Variable<bool>(isActive);
    map['has_approved_content'] = Variable<bool>(hasApprovedContent);
    map['has_approved_i_t'] = Variable<bool>(hasApprovedIT);
    map['is_selected'] = Variable<bool>(isSelected);
    map['icon_path'] = Variable<String>(iconPath);
    return map;
  }

  LearningLangCompanion toCompanion(bool nullToAbsent) {
    return LearningLangCompanion(
      id: Value(id),
      idLocal: Value(idLocal),
      orderID: Value(orderID),
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      createBy: Value(createBy),
      updateBy: Value(updateBy),
      title: Value(title),
      description: Value(description),
      imagePath: Value(imagePath),
      isActive: Value(isActive),
      hasApprovedContent: Value(hasApprovedContent),
      hasApprovedIT: Value(hasApprovedIT),
      isSelected: Value(isSelected),
      iconPath: Value(iconPath),
    );
  }

  factory LearningLangData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return LearningLangData(
      id: serializer.fromJson<int>(json['id']),
      idLocal: serializer.fromJson<int>(json['idLocal']),
      orderID: serializer.fromJson<int>(json['orderID']),
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      createBy: serializer.fromJson<int>(json['createBy']),
      updateBy: serializer.fromJson<int>(json['updateBy']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String>(json['description']),
      imagePath: serializer.fromJson<String>(json['imagePath']),
      isActive: serializer.fromJson<bool>(json['isActive']),
      hasApprovedContent: serializer.fromJson<bool>(json['hasApprovedContent']),
      hasApprovedIT: serializer.fromJson<bool>(json['hasApprovedIT']),
      isSelected: serializer.fromJson<bool>(json['isSelected']),
      iconPath: serializer.fromJson<String>(json['iconPath']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'idLocal': serializer.toJson<int>(idLocal),
      'orderID': serializer.toJson<int>(orderID),
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'createBy': serializer.toJson<int>(createBy),
      'updateBy': serializer.toJson<int>(updateBy),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String>(description),
      'imagePath': serializer.toJson<String>(imagePath),
      'isActive': serializer.toJson<bool>(isActive),
      'hasApprovedContent': serializer.toJson<bool>(hasApprovedContent),
      'hasApprovedIT': serializer.toJson<bool>(hasApprovedIT),
      'isSelected': serializer.toJson<bool>(isSelected),
      'iconPath': serializer.toJson<String>(iconPath),
    };
  }

  LearningLangData copyWith(
          {int? id,
          int? idLocal,
          int? orderID,
          DateTime? createDate,
          DateTime? updateDate,
          int? createBy,
          int? updateBy,
          String? title,
          String? description,
          String? imagePath,
          bool? isActive,
          bool? hasApprovedContent,
          bool? hasApprovedIT,
          bool? isSelected,
          String? iconPath}) =>
      LearningLangData(
        id: id ?? this.id,
        idLocal: idLocal ?? this.idLocal,
        orderID: orderID ?? this.orderID,
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        createBy: createBy ?? this.createBy,
        updateBy: updateBy ?? this.updateBy,
        title: title ?? this.title,
        description: description ?? this.description,
        imagePath: imagePath ?? this.imagePath,
        isActive: isActive ?? this.isActive,
        hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
        hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
        isSelected: isSelected ?? this.isSelected,
        iconPath: iconPath ?? this.iconPath,
      );
  @override
  String toString() {
    return (StringBuffer('LearningLangData(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          idLocal.hashCode,
          $mrjc(
              orderID.hashCode,
              $mrjc(
                  createDate.hashCode,
                  $mrjc(
                      updateDate.hashCode,
                      $mrjc(
                          createBy.hashCode,
                          $mrjc(
                              updateBy.hashCode,
                              $mrjc(
                                  title.hashCode,
                                  $mrjc(
                                      description.hashCode,
                                      $mrjc(
                                          imagePath.hashCode,
                                          $mrjc(
                                              isActive.hashCode,
                                              $mrjc(
                                                  hasApprovedContent.hashCode,
                                                  $mrjc(
                                                      hasApprovedIT.hashCode,
                                                      $mrjc(
                                                          isSelected.hashCode,
                                                          iconPath
                                                              .hashCode)))))))))))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is LearningLangData &&
          other.id == this.id &&
          other.idLocal == this.idLocal &&
          other.orderID == this.orderID &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.createBy == this.createBy &&
          other.updateBy == this.updateBy &&
          other.title == this.title &&
          other.description == this.description &&
          other.imagePath == this.imagePath &&
          other.isActive == this.isActive &&
          other.hasApprovedContent == this.hasApprovedContent &&
          other.hasApprovedIT == this.hasApprovedIT &&
          other.isSelected == this.isSelected &&
          other.iconPath == this.iconPath);
}

class LearningLangCompanion extends UpdateCompanion<LearningLangData> {
  final Value<int> id;
  final Value<int> idLocal;
  final Value<int> orderID;
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<int> createBy;
  final Value<int> updateBy;
  final Value<String> title;
  final Value<String> description;
  final Value<String> imagePath;
  final Value<bool> isActive;
  final Value<bool> hasApprovedContent;
  final Value<bool> hasApprovedIT;
  final Value<bool> isSelected;
  final Value<String> iconPath;
  const LearningLangCompanion({
    this.id = const Value.absent(),
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.createBy = const Value.absent(),
    this.updateBy = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
  });
  LearningLangCompanion.insert({
    required int id,
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    required int createBy,
    required int updateBy,
    required String title,
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
  })  : id = Value(id),
        createBy = Value(createBy),
        updateBy = Value(updateBy),
        title = Value(title);
  static Insertable<LearningLangData> custom({
    Expression<int>? id,
    Expression<int>? idLocal,
    Expression<int>? orderID,
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<int>? createBy,
    Expression<int>? updateBy,
    Expression<String>? title,
    Expression<String>? description,
    Expression<String>? imagePath,
    Expression<bool>? isActive,
    Expression<bool>? hasApprovedContent,
    Expression<bool>? hasApprovedIT,
    Expression<bool>? isSelected,
    Expression<String>? iconPath,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (idLocal != null) 'id_local': idLocal,
      if (orderID != null) 'order_i_d': orderID,
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (createBy != null) 'create_by': createBy,
      if (updateBy != null) 'update_by': updateBy,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (imagePath != null) 'image_path': imagePath,
      if (isActive != null) 'is_active': isActive,
      if (hasApprovedContent != null)
        'has_approved_content': hasApprovedContent,
      if (hasApprovedIT != null) 'has_approved_i_t': hasApprovedIT,
      if (isSelected != null) 'is_selected': isSelected,
      if (iconPath != null) 'icon_path': iconPath,
    });
  }

  LearningLangCompanion copyWith(
      {Value<int>? id,
      Value<int>? idLocal,
      Value<int>? orderID,
      Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<int>? createBy,
      Value<int>? updateBy,
      Value<String>? title,
      Value<String>? description,
      Value<String>? imagePath,
      Value<bool>? isActive,
      Value<bool>? hasApprovedContent,
      Value<bool>? hasApprovedIT,
      Value<bool>? isSelected,
      Value<String>? iconPath}) {
    return LearningLangCompanion(
      id: id ?? this.id,
      idLocal: idLocal ?? this.idLocal,
      orderID: orderID ?? this.orderID,
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      createBy: createBy ?? this.createBy,
      updateBy: updateBy ?? this.updateBy,
      title: title ?? this.title,
      description: description ?? this.description,
      imagePath: imagePath ?? this.imagePath,
      isActive: isActive ?? this.isActive,
      hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
      hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
      isSelected: isSelected ?? this.isSelected,
      iconPath: iconPath ?? this.iconPath,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (idLocal.present) {
      map['id_local'] = Variable<int>(idLocal.value);
    }
    if (orderID.present) {
      map['order_i_d'] = Variable<int>(orderID.value);
    }
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (createBy.present) {
      map['create_by'] = Variable<int>(createBy.value);
    }
    if (updateBy.present) {
      map['update_by'] = Variable<int>(updateBy.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (imagePath.present) {
      map['image_path'] = Variable<String>(imagePath.value);
    }
    if (isActive.present) {
      map['is_active'] = Variable<bool>(isActive.value);
    }
    if (hasApprovedContent.present) {
      map['has_approved_content'] = Variable<bool>(hasApprovedContent.value);
    }
    if (hasApprovedIT.present) {
      map['has_approved_i_t'] = Variable<bool>(hasApprovedIT.value);
    }
    if (isSelected.present) {
      map['is_selected'] = Variable<bool>(isSelected.value);
    }
    if (iconPath.present) {
      map['icon_path'] = Variable<String>(iconPath.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LearningLangCompanion(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath')
          ..write(')'))
        .toString();
  }
}

class $LearningLangTable extends LearningLang
    with TableInfo<$LearningLangTable, LearningLangData> {
  final GeneratedDatabase _db;
  final String? _alias;
  $LearningLangTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _idLocalMeta = const VerificationMeta('idLocal');
  late final GeneratedColumn<int?> idLocal = GeneratedColumn<int?>(
      'id_local', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _orderIDMeta = const VerificationMeta('orderID');
  late final GeneratedColumn<int?> orderID = GeneratedColumn<int?>(
      'order_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _createDateMeta = const VerificationMeta('createDate');
  late final GeneratedColumn<DateTime?> createDate = GeneratedColumn<DateTime?>(
      'create_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _updateDateMeta = const VerificationMeta('updateDate');
  late final GeneratedColumn<DateTime?> updateDate = GeneratedColumn<DateTime?>(
      'update_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _createByMeta = const VerificationMeta('createBy');
  late final GeneratedColumn<int?> createBy = GeneratedColumn<int?>(
      'create_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _updateByMeta = const VerificationMeta('updateBy');
  late final GeneratedColumn<int?> updateBy = GeneratedColumn<int?>(
      'update_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, false,
      typeName: 'TEXT', requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _imagePathMeta = const VerificationMeta('imagePath');
  late final GeneratedColumn<String?> imagePath = GeneratedColumn<String?>(
      'image_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _isActiveMeta = const VerificationMeta('isActive');
  late final GeneratedColumn<bool?> isActive = GeneratedColumn<bool?>(
      'is_active', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_active IN (0, 1))',
      defaultValue: const Constant(true));
  final VerificationMeta _hasApprovedContentMeta =
      const VerificationMeta('hasApprovedContent');
  late final GeneratedColumn<bool?> hasApprovedContent = GeneratedColumn<bool?>(
      'has_approved_content', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_content IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _hasApprovedITMeta =
      const VerificationMeta('hasApprovedIT');
  late final GeneratedColumn<bool?> hasApprovedIT = GeneratedColumn<bool?>(
      'has_approved_i_t', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_i_t IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _isSelectedMeta = const VerificationMeta('isSelected');
  late final GeneratedColumn<bool?> isSelected = GeneratedColumn<bool?>(
      'is_selected', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_selected IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _iconPathMeta = const VerificationMeta('iconPath');
  late final GeneratedColumn<String?> iconPath = GeneratedColumn<String?>(
      'icon_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        idLocal,
        orderID,
        createDate,
        updateDate,
        createBy,
        updateBy,
        title,
        description,
        imagePath,
        isActive,
        hasApprovedContent,
        hasApprovedIT,
        isSelected,
        iconPath
      ];
  @override
  String get aliasedName => _alias ?? 'learning_lang';
  @override
  String get actualTableName => 'learning_lang';
  @override
  VerificationContext validateIntegrity(Insertable<LearningLangData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('id_local')) {
      context.handle(_idLocalMeta,
          idLocal.isAcceptableOrUnknown(data['id_local']!, _idLocalMeta));
    }
    if (data.containsKey('order_i_d')) {
      context.handle(_orderIDMeta,
          orderID.isAcceptableOrUnknown(data['order_i_d']!, _orderIDMeta));
    }
    if (data.containsKey('create_date')) {
      context.handle(
          _createDateMeta,
          createDate.isAcceptableOrUnknown(
              data['create_date']!, _createDateMeta));
    }
    if (data.containsKey('update_date')) {
      context.handle(
          _updateDateMeta,
          updateDate.isAcceptableOrUnknown(
              data['update_date']!, _updateDateMeta));
    }
    if (data.containsKey('create_by')) {
      context.handle(_createByMeta,
          createBy.isAcceptableOrUnknown(data['create_by']!, _createByMeta));
    } else if (isInserting) {
      context.missing(_createByMeta);
    }
    if (data.containsKey('update_by')) {
      context.handle(_updateByMeta,
          updateBy.isAcceptableOrUnknown(data['update_by']!, _updateByMeta));
    } else if (isInserting) {
      context.missing(_updateByMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('image_path')) {
      context.handle(_imagePathMeta,
          imagePath.isAcceptableOrUnknown(data['image_path']!, _imagePathMeta));
    }
    if (data.containsKey('is_active')) {
      context.handle(_isActiveMeta,
          isActive.isAcceptableOrUnknown(data['is_active']!, _isActiveMeta));
    }
    if (data.containsKey('has_approved_content')) {
      context.handle(
          _hasApprovedContentMeta,
          hasApprovedContent.isAcceptableOrUnknown(
              data['has_approved_content']!, _hasApprovedContentMeta));
    }
    if (data.containsKey('has_approved_i_t')) {
      context.handle(
          _hasApprovedITMeta,
          hasApprovedIT.isAcceptableOrUnknown(
              data['has_approved_i_t']!, _hasApprovedITMeta));
    }
    if (data.containsKey('is_selected')) {
      context.handle(
          _isSelectedMeta,
          isSelected.isAcceptableOrUnknown(
              data['is_selected']!, _isSelectedMeta));
    }
    if (data.containsKey('icon_path')) {
      context.handle(_iconPathMeta,
          iconPath.isAcceptableOrUnknown(data['icon_path']!, _iconPathMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idLocal};
  @override
  LearningLangData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return LearningLangData.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $LearningLangTable createAlias(String alias) {
    return $LearningLangTable(_db, alias);
  }
}

class CourseLevelData extends DataClass implements Insertable<CourseLevelData> {
  final int id;
  final int idLocal;
  final int orderID;
  final DateTime createDate;
  final DateTime updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;
  final bool isActive;
  final bool hasApprovedContent;
  final bool hasApprovedIT;
  final bool isSelected;
  final String iconPath;
  final String videoURL;
  CourseLevelData(
      {required this.id,
      required this.idLocal,
      required this.orderID,
      required this.createDate,
      required this.updateDate,
      required this.createBy,
      required this.updateBy,
      required this.title,
      required this.description,
      required this.imagePath,
      required this.isActive,
      required this.hasApprovedContent,
      required this.hasApprovedIT,
      required this.isSelected,
      required this.iconPath,
      required this.videoURL});
  factory CourseLevelData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return CourseLevelData(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      idLocal: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id_local'])!,
      orderID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}order_i_d'])!,
      createDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_date'])!,
      updateDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_date'])!,
      createBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_by'])!,
      updateBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_by'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      imagePath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image_path'])!,
      isActive: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_active'])!,
      hasApprovedContent: const BoolType().mapFromDatabaseResponse(
          data['${effectivePrefix}has_approved_content'])!,
      hasApprovedIT: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}has_approved_i_t'])!,
      isSelected: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_selected'])!,
      iconPath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}icon_path'])!,
      videoURL: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}video_u_r_l'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['id_local'] = Variable<int>(idLocal);
    map['order_i_d'] = Variable<int>(orderID);
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    map['create_by'] = Variable<int>(createBy);
    map['update_by'] = Variable<int>(updateBy);
    map['title'] = Variable<String>(title);
    map['description'] = Variable<String>(description);
    map['image_path'] = Variable<String>(imagePath);
    map['is_active'] = Variable<bool>(isActive);
    map['has_approved_content'] = Variable<bool>(hasApprovedContent);
    map['has_approved_i_t'] = Variable<bool>(hasApprovedIT);
    map['is_selected'] = Variable<bool>(isSelected);
    map['icon_path'] = Variable<String>(iconPath);
    map['video_u_r_l'] = Variable<String>(videoURL);
    return map;
  }

  CourseLevelCompanion toCompanion(bool nullToAbsent) {
    return CourseLevelCompanion(
      id: Value(id),
      idLocal: Value(idLocal),
      orderID: Value(orderID),
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      createBy: Value(createBy),
      updateBy: Value(updateBy),
      title: Value(title),
      description: Value(description),
      imagePath: Value(imagePath),
      isActive: Value(isActive),
      hasApprovedContent: Value(hasApprovedContent),
      hasApprovedIT: Value(hasApprovedIT),
      isSelected: Value(isSelected),
      iconPath: Value(iconPath),
      videoURL: Value(videoURL),
    );
  }

  factory CourseLevelData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return CourseLevelData(
      id: serializer.fromJson<int>(json['id']),
      idLocal: serializer.fromJson<int>(json['idLocal']),
      orderID: serializer.fromJson<int>(json['orderID']),
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      createBy: serializer.fromJson<int>(json['createBy']),
      updateBy: serializer.fromJson<int>(json['updateBy']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String>(json['description']),
      imagePath: serializer.fromJson<String>(json['imagePath']),
      isActive: serializer.fromJson<bool>(json['isActive']),
      hasApprovedContent: serializer.fromJson<bool>(json['hasApprovedContent']),
      hasApprovedIT: serializer.fromJson<bool>(json['hasApprovedIT']),
      isSelected: serializer.fromJson<bool>(json['isSelected']),
      iconPath: serializer.fromJson<String>(json['iconPath']),
      videoURL: serializer.fromJson<String>(json['videoURL']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'idLocal': serializer.toJson<int>(idLocal),
      'orderID': serializer.toJson<int>(orderID),
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'createBy': serializer.toJson<int>(createBy),
      'updateBy': serializer.toJson<int>(updateBy),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String>(description),
      'imagePath': serializer.toJson<String>(imagePath),
      'isActive': serializer.toJson<bool>(isActive),
      'hasApprovedContent': serializer.toJson<bool>(hasApprovedContent),
      'hasApprovedIT': serializer.toJson<bool>(hasApprovedIT),
      'isSelected': serializer.toJson<bool>(isSelected),
      'iconPath': serializer.toJson<String>(iconPath),
      'videoURL': serializer.toJson<String>(videoURL),
    };
  }

  CourseLevelData copyWith(
          {int? id,
          int? idLocal,
          int? orderID,
          DateTime? createDate,
          DateTime? updateDate,
          int? createBy,
          int? updateBy,
          String? title,
          String? description,
          String? imagePath,
          bool? isActive,
          bool? hasApprovedContent,
          bool? hasApprovedIT,
          bool? isSelected,
          String? iconPath,
          String? videoURL}) =>
      CourseLevelData(
        id: id ?? this.id,
        idLocal: idLocal ?? this.idLocal,
        orderID: orderID ?? this.orderID,
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        createBy: createBy ?? this.createBy,
        updateBy: updateBy ?? this.updateBy,
        title: title ?? this.title,
        description: description ?? this.description,
        imagePath: imagePath ?? this.imagePath,
        isActive: isActive ?? this.isActive,
        hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
        hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
        isSelected: isSelected ?? this.isSelected,
        iconPath: iconPath ?? this.iconPath,
        videoURL: videoURL ?? this.videoURL,
      );
  @override
  String toString() {
    return (StringBuffer('CourseLevelData(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath, ')
          ..write('videoURL: $videoURL')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          idLocal.hashCode,
          $mrjc(
              orderID.hashCode,
              $mrjc(
                  createDate.hashCode,
                  $mrjc(
                      updateDate.hashCode,
                      $mrjc(
                          createBy.hashCode,
                          $mrjc(
                              updateBy.hashCode,
                              $mrjc(
                                  title.hashCode,
                                  $mrjc(
                                      description.hashCode,
                                      $mrjc(
                                          imagePath.hashCode,
                                          $mrjc(
                                              isActive.hashCode,
                                              $mrjc(
                                                  hasApprovedContent.hashCode,
                                                  $mrjc(
                                                      hasApprovedIT.hashCode,
                                                      $mrjc(
                                                          isSelected.hashCode,
                                                          $mrjc(
                                                              iconPath.hashCode,
                                                              videoURL
                                                                  .hashCode))))))))))))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CourseLevelData &&
          other.id == this.id &&
          other.idLocal == this.idLocal &&
          other.orderID == this.orderID &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.createBy == this.createBy &&
          other.updateBy == this.updateBy &&
          other.title == this.title &&
          other.description == this.description &&
          other.imagePath == this.imagePath &&
          other.isActive == this.isActive &&
          other.hasApprovedContent == this.hasApprovedContent &&
          other.hasApprovedIT == this.hasApprovedIT &&
          other.isSelected == this.isSelected &&
          other.iconPath == this.iconPath &&
          other.videoURL == this.videoURL);
}

class CourseLevelCompanion extends UpdateCompanion<CourseLevelData> {
  final Value<int> id;
  final Value<int> idLocal;
  final Value<int> orderID;
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<int> createBy;
  final Value<int> updateBy;
  final Value<String> title;
  final Value<String> description;
  final Value<String> imagePath;
  final Value<bool> isActive;
  final Value<bool> hasApprovedContent;
  final Value<bool> hasApprovedIT;
  final Value<bool> isSelected;
  final Value<String> iconPath;
  final Value<String> videoURL;
  const CourseLevelCompanion({
    this.id = const Value.absent(),
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.createBy = const Value.absent(),
    this.updateBy = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.videoURL = const Value.absent(),
  });
  CourseLevelCompanion.insert({
    required int id,
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    required int createBy,
    required int updateBy,
    required String title,
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.videoURL = const Value.absent(),
  })  : id = Value(id),
        createBy = Value(createBy),
        updateBy = Value(updateBy),
        title = Value(title);
  static Insertable<CourseLevelData> custom({
    Expression<int>? id,
    Expression<int>? idLocal,
    Expression<int>? orderID,
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<int>? createBy,
    Expression<int>? updateBy,
    Expression<String>? title,
    Expression<String>? description,
    Expression<String>? imagePath,
    Expression<bool>? isActive,
    Expression<bool>? hasApprovedContent,
    Expression<bool>? hasApprovedIT,
    Expression<bool>? isSelected,
    Expression<String>? iconPath,
    Expression<String>? videoURL,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (idLocal != null) 'id_local': idLocal,
      if (orderID != null) 'order_i_d': orderID,
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (createBy != null) 'create_by': createBy,
      if (updateBy != null) 'update_by': updateBy,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (imagePath != null) 'image_path': imagePath,
      if (isActive != null) 'is_active': isActive,
      if (hasApprovedContent != null)
        'has_approved_content': hasApprovedContent,
      if (hasApprovedIT != null) 'has_approved_i_t': hasApprovedIT,
      if (isSelected != null) 'is_selected': isSelected,
      if (iconPath != null) 'icon_path': iconPath,
      if (videoURL != null) 'video_u_r_l': videoURL,
    });
  }

  CourseLevelCompanion copyWith(
      {Value<int>? id,
      Value<int>? idLocal,
      Value<int>? orderID,
      Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<int>? createBy,
      Value<int>? updateBy,
      Value<String>? title,
      Value<String>? description,
      Value<String>? imagePath,
      Value<bool>? isActive,
      Value<bool>? hasApprovedContent,
      Value<bool>? hasApprovedIT,
      Value<bool>? isSelected,
      Value<String>? iconPath,
      Value<String>? videoURL}) {
    return CourseLevelCompanion(
      id: id ?? this.id,
      idLocal: idLocal ?? this.idLocal,
      orderID: orderID ?? this.orderID,
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      createBy: createBy ?? this.createBy,
      updateBy: updateBy ?? this.updateBy,
      title: title ?? this.title,
      description: description ?? this.description,
      imagePath: imagePath ?? this.imagePath,
      isActive: isActive ?? this.isActive,
      hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
      hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
      isSelected: isSelected ?? this.isSelected,
      iconPath: iconPath ?? this.iconPath,
      videoURL: videoURL ?? this.videoURL,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (idLocal.present) {
      map['id_local'] = Variable<int>(idLocal.value);
    }
    if (orderID.present) {
      map['order_i_d'] = Variable<int>(orderID.value);
    }
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (createBy.present) {
      map['create_by'] = Variable<int>(createBy.value);
    }
    if (updateBy.present) {
      map['update_by'] = Variable<int>(updateBy.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (imagePath.present) {
      map['image_path'] = Variable<String>(imagePath.value);
    }
    if (isActive.present) {
      map['is_active'] = Variable<bool>(isActive.value);
    }
    if (hasApprovedContent.present) {
      map['has_approved_content'] = Variable<bool>(hasApprovedContent.value);
    }
    if (hasApprovedIT.present) {
      map['has_approved_i_t'] = Variable<bool>(hasApprovedIT.value);
    }
    if (isSelected.present) {
      map['is_selected'] = Variable<bool>(isSelected.value);
    }
    if (iconPath.present) {
      map['icon_path'] = Variable<String>(iconPath.value);
    }
    if (videoURL.present) {
      map['video_u_r_l'] = Variable<String>(videoURL.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CourseLevelCompanion(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath, ')
          ..write('videoURL: $videoURL')
          ..write(')'))
        .toString();
  }
}

class $CourseLevelTable extends CourseLevel
    with TableInfo<$CourseLevelTable, CourseLevelData> {
  final GeneratedDatabase _db;
  final String? _alias;
  $CourseLevelTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _idLocalMeta = const VerificationMeta('idLocal');
  late final GeneratedColumn<int?> idLocal = GeneratedColumn<int?>(
      'id_local', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _orderIDMeta = const VerificationMeta('orderID');
  late final GeneratedColumn<int?> orderID = GeneratedColumn<int?>(
      'order_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _createDateMeta = const VerificationMeta('createDate');
  late final GeneratedColumn<DateTime?> createDate = GeneratedColumn<DateTime?>(
      'create_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _updateDateMeta = const VerificationMeta('updateDate');
  late final GeneratedColumn<DateTime?> updateDate = GeneratedColumn<DateTime?>(
      'update_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _createByMeta = const VerificationMeta('createBy');
  late final GeneratedColumn<int?> createBy = GeneratedColumn<int?>(
      'create_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _updateByMeta = const VerificationMeta('updateBy');
  late final GeneratedColumn<int?> updateBy = GeneratedColumn<int?>(
      'update_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, false,
      typeName: 'TEXT', requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _imagePathMeta = const VerificationMeta('imagePath');
  late final GeneratedColumn<String?> imagePath = GeneratedColumn<String?>(
      'image_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _isActiveMeta = const VerificationMeta('isActive');
  late final GeneratedColumn<bool?> isActive = GeneratedColumn<bool?>(
      'is_active', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_active IN (0, 1))',
      defaultValue: const Constant(true));
  final VerificationMeta _hasApprovedContentMeta =
      const VerificationMeta('hasApprovedContent');
  late final GeneratedColumn<bool?> hasApprovedContent = GeneratedColumn<bool?>(
      'has_approved_content', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_content IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _hasApprovedITMeta =
      const VerificationMeta('hasApprovedIT');
  late final GeneratedColumn<bool?> hasApprovedIT = GeneratedColumn<bool?>(
      'has_approved_i_t', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_i_t IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _isSelectedMeta = const VerificationMeta('isSelected');
  late final GeneratedColumn<bool?> isSelected = GeneratedColumn<bool?>(
      'is_selected', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_selected IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _iconPathMeta = const VerificationMeta('iconPath');
  late final GeneratedColumn<String?> iconPath = GeneratedColumn<String?>(
      'icon_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _videoURLMeta = const VerificationMeta('videoURL');
  late final GeneratedColumn<String?> videoURL = GeneratedColumn<String?>(
      'video_u_r_l', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        idLocal,
        orderID,
        createDate,
        updateDate,
        createBy,
        updateBy,
        title,
        description,
        imagePath,
        isActive,
        hasApprovedContent,
        hasApprovedIT,
        isSelected,
        iconPath,
        videoURL
      ];
  @override
  String get aliasedName => _alias ?? 'course_level';
  @override
  String get actualTableName => 'course_level';
  @override
  VerificationContext validateIntegrity(Insertable<CourseLevelData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('id_local')) {
      context.handle(_idLocalMeta,
          idLocal.isAcceptableOrUnknown(data['id_local']!, _idLocalMeta));
    }
    if (data.containsKey('order_i_d')) {
      context.handle(_orderIDMeta,
          orderID.isAcceptableOrUnknown(data['order_i_d']!, _orderIDMeta));
    }
    if (data.containsKey('create_date')) {
      context.handle(
          _createDateMeta,
          createDate.isAcceptableOrUnknown(
              data['create_date']!, _createDateMeta));
    }
    if (data.containsKey('update_date')) {
      context.handle(
          _updateDateMeta,
          updateDate.isAcceptableOrUnknown(
              data['update_date']!, _updateDateMeta));
    }
    if (data.containsKey('create_by')) {
      context.handle(_createByMeta,
          createBy.isAcceptableOrUnknown(data['create_by']!, _createByMeta));
    } else if (isInserting) {
      context.missing(_createByMeta);
    }
    if (data.containsKey('update_by')) {
      context.handle(_updateByMeta,
          updateBy.isAcceptableOrUnknown(data['update_by']!, _updateByMeta));
    } else if (isInserting) {
      context.missing(_updateByMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('image_path')) {
      context.handle(_imagePathMeta,
          imagePath.isAcceptableOrUnknown(data['image_path']!, _imagePathMeta));
    }
    if (data.containsKey('is_active')) {
      context.handle(_isActiveMeta,
          isActive.isAcceptableOrUnknown(data['is_active']!, _isActiveMeta));
    }
    if (data.containsKey('has_approved_content')) {
      context.handle(
          _hasApprovedContentMeta,
          hasApprovedContent.isAcceptableOrUnknown(
              data['has_approved_content']!, _hasApprovedContentMeta));
    }
    if (data.containsKey('has_approved_i_t')) {
      context.handle(
          _hasApprovedITMeta,
          hasApprovedIT.isAcceptableOrUnknown(
              data['has_approved_i_t']!, _hasApprovedITMeta));
    }
    if (data.containsKey('is_selected')) {
      context.handle(
          _isSelectedMeta,
          isSelected.isAcceptableOrUnknown(
              data['is_selected']!, _isSelectedMeta));
    }
    if (data.containsKey('icon_path')) {
      context.handle(_iconPathMeta,
          iconPath.isAcceptableOrUnknown(data['icon_path']!, _iconPathMeta));
    }
    if (data.containsKey('video_u_r_l')) {
      context.handle(_videoURLMeta,
          videoURL.isAcceptableOrUnknown(data['video_u_r_l']!, _videoURLMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idLocal};
  @override
  CourseLevelData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return CourseLevelData.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $CourseLevelTable createAlias(String alias) {
    return $CourseLevelTable(_db, alias);
  }
}

class CourseData extends DataClass implements Insertable<CourseData> {
  final int id;
  final int idLocal;
  final int orderID;
  final DateTime createDate;
  final DateTime updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;
  final bool isActive;
  final bool hasApprovedContent;
  final bool hasApprovedIT;
  final bool isSelected;
  final String iconPath;
  final String videoURL;
  final int languageID;
  CourseData(
      {required this.id,
      required this.idLocal,
      required this.orderID,
      required this.createDate,
      required this.updateDate,
      required this.createBy,
      required this.updateBy,
      required this.title,
      required this.description,
      required this.imagePath,
      required this.isActive,
      required this.hasApprovedContent,
      required this.hasApprovedIT,
      required this.isSelected,
      required this.iconPath,
      required this.videoURL,
      required this.languageID});
  factory CourseData.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return CourseData(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      idLocal: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id_local'])!,
      orderID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}order_i_d'])!,
      createDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_date'])!,
      updateDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_date'])!,
      createBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_by'])!,
      updateBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_by'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      imagePath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image_path'])!,
      isActive: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_active'])!,
      hasApprovedContent: const BoolType().mapFromDatabaseResponse(
          data['${effectivePrefix}has_approved_content'])!,
      hasApprovedIT: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}has_approved_i_t'])!,
      isSelected: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_selected'])!,
      iconPath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}icon_path'])!,
      videoURL: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}video_u_r_l'])!,
      languageID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}language_i_d'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['id_local'] = Variable<int>(idLocal);
    map['order_i_d'] = Variable<int>(orderID);
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    map['create_by'] = Variable<int>(createBy);
    map['update_by'] = Variable<int>(updateBy);
    map['title'] = Variable<String>(title);
    map['description'] = Variable<String>(description);
    map['image_path'] = Variable<String>(imagePath);
    map['is_active'] = Variable<bool>(isActive);
    map['has_approved_content'] = Variable<bool>(hasApprovedContent);
    map['has_approved_i_t'] = Variable<bool>(hasApprovedIT);
    map['is_selected'] = Variable<bool>(isSelected);
    map['icon_path'] = Variable<String>(iconPath);
    map['video_u_r_l'] = Variable<String>(videoURL);
    map['language_i_d'] = Variable<int>(languageID);
    return map;
  }

  CourseCompanion toCompanion(bool nullToAbsent) {
    return CourseCompanion(
      id: Value(id),
      idLocal: Value(idLocal),
      orderID: Value(orderID),
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      createBy: Value(createBy),
      updateBy: Value(updateBy),
      title: Value(title),
      description: Value(description),
      imagePath: Value(imagePath),
      isActive: Value(isActive),
      hasApprovedContent: Value(hasApprovedContent),
      hasApprovedIT: Value(hasApprovedIT),
      isSelected: Value(isSelected),
      iconPath: Value(iconPath),
      videoURL: Value(videoURL),
      languageID: Value(languageID),
    );
  }

  factory CourseData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return CourseData(
      id: serializer.fromJson<int>(json['id']),
      idLocal: serializer.fromJson<int>(json['idLocal']),
      orderID: serializer.fromJson<int>(json['orderID']),
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      createBy: serializer.fromJson<int>(json['createBy']),
      updateBy: serializer.fromJson<int>(json['updateBy']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String>(json['description']),
      imagePath: serializer.fromJson<String>(json['imagePath']),
      isActive: serializer.fromJson<bool>(json['isActive']),
      hasApprovedContent: serializer.fromJson<bool>(json['hasApprovedContent']),
      hasApprovedIT: serializer.fromJson<bool>(json['hasApprovedIT']),
      isSelected: serializer.fromJson<bool>(json['isSelected']),
      iconPath: serializer.fromJson<String>(json['iconPath']),
      videoURL: serializer.fromJson<String>(json['videoURL']),
      languageID: serializer.fromJson<int>(json['languageID']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'idLocal': serializer.toJson<int>(idLocal),
      'orderID': serializer.toJson<int>(orderID),
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'createBy': serializer.toJson<int>(createBy),
      'updateBy': serializer.toJson<int>(updateBy),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String>(description),
      'imagePath': serializer.toJson<String>(imagePath),
      'isActive': serializer.toJson<bool>(isActive),
      'hasApprovedContent': serializer.toJson<bool>(hasApprovedContent),
      'hasApprovedIT': serializer.toJson<bool>(hasApprovedIT),
      'isSelected': serializer.toJson<bool>(isSelected),
      'iconPath': serializer.toJson<String>(iconPath),
      'videoURL': serializer.toJson<String>(videoURL),
      'languageID': serializer.toJson<int>(languageID),
    };
  }

  CourseData copyWith(
          {int? id,
          int? idLocal,
          int? orderID,
          DateTime? createDate,
          DateTime? updateDate,
          int? createBy,
          int? updateBy,
          String? title,
          String? description,
          String? imagePath,
          bool? isActive,
          bool? hasApprovedContent,
          bool? hasApprovedIT,
          bool? isSelected,
          String? iconPath,
          String? videoURL,
          int? languageID}) =>
      CourseData(
        id: id ?? this.id,
        idLocal: idLocal ?? this.idLocal,
        orderID: orderID ?? this.orderID,
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        createBy: createBy ?? this.createBy,
        updateBy: updateBy ?? this.updateBy,
        title: title ?? this.title,
        description: description ?? this.description,
        imagePath: imagePath ?? this.imagePath,
        isActive: isActive ?? this.isActive,
        hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
        hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
        isSelected: isSelected ?? this.isSelected,
        iconPath: iconPath ?? this.iconPath,
        videoURL: videoURL ?? this.videoURL,
        languageID: languageID ?? this.languageID,
      );
  @override
  String toString() {
    return (StringBuffer('CourseData(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath, ')
          ..write('videoURL: $videoURL, ')
          ..write('languageID: $languageID')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          idLocal.hashCode,
          $mrjc(
              orderID.hashCode,
              $mrjc(
                  createDate.hashCode,
                  $mrjc(
                      updateDate.hashCode,
                      $mrjc(
                          createBy.hashCode,
                          $mrjc(
                              updateBy.hashCode,
                              $mrjc(
                                  title.hashCode,
                                  $mrjc(
                                      description.hashCode,
                                      $mrjc(
                                          imagePath.hashCode,
                                          $mrjc(
                                              isActive.hashCode,
                                              $mrjc(
                                                  hasApprovedContent.hashCode,
                                                  $mrjc(
                                                      hasApprovedIT.hashCode,
                                                      $mrjc(
                                                          isSelected.hashCode,
                                                          $mrjc(
                                                              iconPath.hashCode,
                                                              $mrjc(
                                                                  videoURL
                                                                      .hashCode,
                                                                  languageID
                                                                      .hashCode)))))))))))))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CourseData &&
          other.id == this.id &&
          other.idLocal == this.idLocal &&
          other.orderID == this.orderID &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.createBy == this.createBy &&
          other.updateBy == this.updateBy &&
          other.title == this.title &&
          other.description == this.description &&
          other.imagePath == this.imagePath &&
          other.isActive == this.isActive &&
          other.hasApprovedContent == this.hasApprovedContent &&
          other.hasApprovedIT == this.hasApprovedIT &&
          other.isSelected == this.isSelected &&
          other.iconPath == this.iconPath &&
          other.videoURL == this.videoURL &&
          other.languageID == this.languageID);
}

class CourseCompanion extends UpdateCompanion<CourseData> {
  final Value<int> id;
  final Value<int> idLocal;
  final Value<int> orderID;
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<int> createBy;
  final Value<int> updateBy;
  final Value<String> title;
  final Value<String> description;
  final Value<String> imagePath;
  final Value<bool> isActive;
  final Value<bool> hasApprovedContent;
  final Value<bool> hasApprovedIT;
  final Value<bool> isSelected;
  final Value<String> iconPath;
  final Value<String> videoURL;
  final Value<int> languageID;
  const CourseCompanion({
    this.id = const Value.absent(),
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.createBy = const Value.absent(),
    this.updateBy = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.videoURL = const Value.absent(),
    this.languageID = const Value.absent(),
  });
  CourseCompanion.insert({
    required int id,
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    required int createBy,
    required int updateBy,
    required String title,
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.videoURL = const Value.absent(),
    required int languageID,
  })  : id = Value(id),
        createBy = Value(createBy),
        updateBy = Value(updateBy),
        title = Value(title),
        languageID = Value(languageID);
  static Insertable<CourseData> custom({
    Expression<int>? id,
    Expression<int>? idLocal,
    Expression<int>? orderID,
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<int>? createBy,
    Expression<int>? updateBy,
    Expression<String>? title,
    Expression<String>? description,
    Expression<String>? imagePath,
    Expression<bool>? isActive,
    Expression<bool>? hasApprovedContent,
    Expression<bool>? hasApprovedIT,
    Expression<bool>? isSelected,
    Expression<String>? iconPath,
    Expression<String>? videoURL,
    Expression<int>? languageID,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (idLocal != null) 'id_local': idLocal,
      if (orderID != null) 'order_i_d': orderID,
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (createBy != null) 'create_by': createBy,
      if (updateBy != null) 'update_by': updateBy,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (imagePath != null) 'image_path': imagePath,
      if (isActive != null) 'is_active': isActive,
      if (hasApprovedContent != null)
        'has_approved_content': hasApprovedContent,
      if (hasApprovedIT != null) 'has_approved_i_t': hasApprovedIT,
      if (isSelected != null) 'is_selected': isSelected,
      if (iconPath != null) 'icon_path': iconPath,
      if (videoURL != null) 'video_u_r_l': videoURL,
      if (languageID != null) 'language_i_d': languageID,
    });
  }

  CourseCompanion copyWith(
      {Value<int>? id,
      Value<int>? idLocal,
      Value<int>? orderID,
      Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<int>? createBy,
      Value<int>? updateBy,
      Value<String>? title,
      Value<String>? description,
      Value<String>? imagePath,
      Value<bool>? isActive,
      Value<bool>? hasApprovedContent,
      Value<bool>? hasApprovedIT,
      Value<bool>? isSelected,
      Value<String>? iconPath,
      Value<String>? videoURL,
      Value<int>? languageID}) {
    return CourseCompanion(
      id: id ?? this.id,
      idLocal: idLocal ?? this.idLocal,
      orderID: orderID ?? this.orderID,
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      createBy: createBy ?? this.createBy,
      updateBy: updateBy ?? this.updateBy,
      title: title ?? this.title,
      description: description ?? this.description,
      imagePath: imagePath ?? this.imagePath,
      isActive: isActive ?? this.isActive,
      hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
      hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
      isSelected: isSelected ?? this.isSelected,
      iconPath: iconPath ?? this.iconPath,
      videoURL: videoURL ?? this.videoURL,
      languageID: languageID ?? this.languageID,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (idLocal.present) {
      map['id_local'] = Variable<int>(idLocal.value);
    }
    if (orderID.present) {
      map['order_i_d'] = Variable<int>(orderID.value);
    }
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (createBy.present) {
      map['create_by'] = Variable<int>(createBy.value);
    }
    if (updateBy.present) {
      map['update_by'] = Variable<int>(updateBy.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (imagePath.present) {
      map['image_path'] = Variable<String>(imagePath.value);
    }
    if (isActive.present) {
      map['is_active'] = Variable<bool>(isActive.value);
    }
    if (hasApprovedContent.present) {
      map['has_approved_content'] = Variable<bool>(hasApprovedContent.value);
    }
    if (hasApprovedIT.present) {
      map['has_approved_i_t'] = Variable<bool>(hasApprovedIT.value);
    }
    if (isSelected.present) {
      map['is_selected'] = Variable<bool>(isSelected.value);
    }
    if (iconPath.present) {
      map['icon_path'] = Variable<String>(iconPath.value);
    }
    if (videoURL.present) {
      map['video_u_r_l'] = Variable<String>(videoURL.value);
    }
    if (languageID.present) {
      map['language_i_d'] = Variable<int>(languageID.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CourseCompanion(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath, ')
          ..write('videoURL: $videoURL, ')
          ..write('languageID: $languageID')
          ..write(')'))
        .toString();
  }
}

class $CourseTable extends Course with TableInfo<$CourseTable, CourseData> {
  final GeneratedDatabase _db;
  final String? _alias;
  $CourseTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _idLocalMeta = const VerificationMeta('idLocal');
  late final GeneratedColumn<int?> idLocal = GeneratedColumn<int?>(
      'id_local', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _orderIDMeta = const VerificationMeta('orderID');
  late final GeneratedColumn<int?> orderID = GeneratedColumn<int?>(
      'order_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _createDateMeta = const VerificationMeta('createDate');
  late final GeneratedColumn<DateTime?> createDate = GeneratedColumn<DateTime?>(
      'create_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _updateDateMeta = const VerificationMeta('updateDate');
  late final GeneratedColumn<DateTime?> updateDate = GeneratedColumn<DateTime?>(
      'update_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _createByMeta = const VerificationMeta('createBy');
  late final GeneratedColumn<int?> createBy = GeneratedColumn<int?>(
      'create_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _updateByMeta = const VerificationMeta('updateBy');
  late final GeneratedColumn<int?> updateBy = GeneratedColumn<int?>(
      'update_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, false,
      typeName: 'TEXT', requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _imagePathMeta = const VerificationMeta('imagePath');
  late final GeneratedColumn<String?> imagePath = GeneratedColumn<String?>(
      'image_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _isActiveMeta = const VerificationMeta('isActive');
  late final GeneratedColumn<bool?> isActive = GeneratedColumn<bool?>(
      'is_active', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_active IN (0, 1))',
      defaultValue: const Constant(true));
  final VerificationMeta _hasApprovedContentMeta =
      const VerificationMeta('hasApprovedContent');
  late final GeneratedColumn<bool?> hasApprovedContent = GeneratedColumn<bool?>(
      'has_approved_content', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_content IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _hasApprovedITMeta =
      const VerificationMeta('hasApprovedIT');
  late final GeneratedColumn<bool?> hasApprovedIT = GeneratedColumn<bool?>(
      'has_approved_i_t', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_i_t IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _isSelectedMeta = const VerificationMeta('isSelected');
  late final GeneratedColumn<bool?> isSelected = GeneratedColumn<bool?>(
      'is_selected', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_selected IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _iconPathMeta = const VerificationMeta('iconPath');
  late final GeneratedColumn<String?> iconPath = GeneratedColumn<String?>(
      'icon_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _videoURLMeta = const VerificationMeta('videoURL');
  late final GeneratedColumn<String?> videoURL = GeneratedColumn<String?>(
      'video_u_r_l', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _languageIDMeta = const VerificationMeta('languageID');
  late final GeneratedColumn<int?> languageID = GeneratedColumn<int?>(
      'language_i_d', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        idLocal,
        orderID,
        createDate,
        updateDate,
        createBy,
        updateBy,
        title,
        description,
        imagePath,
        isActive,
        hasApprovedContent,
        hasApprovedIT,
        isSelected,
        iconPath,
        videoURL,
        languageID
      ];
  @override
  String get aliasedName => _alias ?? 'course';
  @override
  String get actualTableName => 'course';
  @override
  VerificationContext validateIntegrity(Insertable<CourseData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('id_local')) {
      context.handle(_idLocalMeta,
          idLocal.isAcceptableOrUnknown(data['id_local']!, _idLocalMeta));
    }
    if (data.containsKey('order_i_d')) {
      context.handle(_orderIDMeta,
          orderID.isAcceptableOrUnknown(data['order_i_d']!, _orderIDMeta));
    }
    if (data.containsKey('create_date')) {
      context.handle(
          _createDateMeta,
          createDate.isAcceptableOrUnknown(
              data['create_date']!, _createDateMeta));
    }
    if (data.containsKey('update_date')) {
      context.handle(
          _updateDateMeta,
          updateDate.isAcceptableOrUnknown(
              data['update_date']!, _updateDateMeta));
    }
    if (data.containsKey('create_by')) {
      context.handle(_createByMeta,
          createBy.isAcceptableOrUnknown(data['create_by']!, _createByMeta));
    } else if (isInserting) {
      context.missing(_createByMeta);
    }
    if (data.containsKey('update_by')) {
      context.handle(_updateByMeta,
          updateBy.isAcceptableOrUnknown(data['update_by']!, _updateByMeta));
    } else if (isInserting) {
      context.missing(_updateByMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('image_path')) {
      context.handle(_imagePathMeta,
          imagePath.isAcceptableOrUnknown(data['image_path']!, _imagePathMeta));
    }
    if (data.containsKey('is_active')) {
      context.handle(_isActiveMeta,
          isActive.isAcceptableOrUnknown(data['is_active']!, _isActiveMeta));
    }
    if (data.containsKey('has_approved_content')) {
      context.handle(
          _hasApprovedContentMeta,
          hasApprovedContent.isAcceptableOrUnknown(
              data['has_approved_content']!, _hasApprovedContentMeta));
    }
    if (data.containsKey('has_approved_i_t')) {
      context.handle(
          _hasApprovedITMeta,
          hasApprovedIT.isAcceptableOrUnknown(
              data['has_approved_i_t']!, _hasApprovedITMeta));
    }
    if (data.containsKey('is_selected')) {
      context.handle(
          _isSelectedMeta,
          isSelected.isAcceptableOrUnknown(
              data['is_selected']!, _isSelectedMeta));
    }
    if (data.containsKey('icon_path')) {
      context.handle(_iconPathMeta,
          iconPath.isAcceptableOrUnknown(data['icon_path']!, _iconPathMeta));
    }
    if (data.containsKey('video_u_r_l')) {
      context.handle(_videoURLMeta,
          videoURL.isAcceptableOrUnknown(data['video_u_r_l']!, _videoURLMeta));
    }
    if (data.containsKey('language_i_d')) {
      context.handle(
          _languageIDMeta,
          languageID.isAcceptableOrUnknown(
              data['language_i_d']!, _languageIDMeta));
    } else if (isInserting) {
      context.missing(_languageIDMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idLocal};
  @override
  CourseData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return CourseData.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $CourseTable createAlias(String alias) {
    return $CourseTable(_db, alias);
  }
}

class LessonData extends DataClass implements Insertable<LessonData> {
  final int id;
  final int idLocal;
  final int orderID;
  final DateTime createDate;
  final DateTime updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;
  final bool isActive;
  final bool hasApprovedContent;
  final bool hasApprovedIT;
  final bool isSelected;
  final String iconPath;
  final String videoURL;
  final String primaryColor;
  LessonData(
      {required this.id,
      required this.idLocal,
      required this.orderID,
      required this.createDate,
      required this.updateDate,
      required this.createBy,
      required this.updateBy,
      required this.title,
      required this.description,
      required this.imagePath,
      required this.isActive,
      required this.hasApprovedContent,
      required this.hasApprovedIT,
      required this.isSelected,
      required this.iconPath,
      required this.videoURL,
      required this.primaryColor});
  factory LessonData.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return LessonData(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      idLocal: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id_local'])!,
      orderID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}order_i_d'])!,
      createDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_date'])!,
      updateDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_date'])!,
      createBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_by'])!,
      updateBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_by'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      imagePath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image_path'])!,
      isActive: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_active'])!,
      hasApprovedContent: const BoolType().mapFromDatabaseResponse(
          data['${effectivePrefix}has_approved_content'])!,
      hasApprovedIT: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}has_approved_i_t'])!,
      isSelected: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_selected'])!,
      iconPath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}icon_path'])!,
      videoURL: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}video_u_r_l'])!,
      primaryColor: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}primary_color'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['id_local'] = Variable<int>(idLocal);
    map['order_i_d'] = Variable<int>(orderID);
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    map['create_by'] = Variable<int>(createBy);
    map['update_by'] = Variable<int>(updateBy);
    map['title'] = Variable<String>(title);
    map['description'] = Variable<String>(description);
    map['image_path'] = Variable<String>(imagePath);
    map['is_active'] = Variable<bool>(isActive);
    map['has_approved_content'] = Variable<bool>(hasApprovedContent);
    map['has_approved_i_t'] = Variable<bool>(hasApprovedIT);
    map['is_selected'] = Variable<bool>(isSelected);
    map['icon_path'] = Variable<String>(iconPath);
    map['video_u_r_l'] = Variable<String>(videoURL);
    map['primary_color'] = Variable<String>(primaryColor);
    return map;
  }

  LessonCompanion toCompanion(bool nullToAbsent) {
    return LessonCompanion(
      id: Value(id),
      idLocal: Value(idLocal),
      orderID: Value(orderID),
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      createBy: Value(createBy),
      updateBy: Value(updateBy),
      title: Value(title),
      description: Value(description),
      imagePath: Value(imagePath),
      isActive: Value(isActive),
      hasApprovedContent: Value(hasApprovedContent),
      hasApprovedIT: Value(hasApprovedIT),
      isSelected: Value(isSelected),
      iconPath: Value(iconPath),
      videoURL: Value(videoURL),
      primaryColor: Value(primaryColor),
    );
  }

  factory LessonData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return LessonData(
      id: serializer.fromJson<int>(json['id']),
      idLocal: serializer.fromJson<int>(json['idLocal']),
      orderID: serializer.fromJson<int>(json['orderID']),
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      createBy: serializer.fromJson<int>(json['createBy']),
      updateBy: serializer.fromJson<int>(json['updateBy']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String>(json['description']),
      imagePath: serializer.fromJson<String>(json['imagePath']),
      isActive: serializer.fromJson<bool>(json['isActive']),
      hasApprovedContent: serializer.fromJson<bool>(json['hasApprovedContent']),
      hasApprovedIT: serializer.fromJson<bool>(json['hasApprovedIT']),
      isSelected: serializer.fromJson<bool>(json['isSelected']),
      iconPath: serializer.fromJson<String>(json['iconPath']),
      videoURL: serializer.fromJson<String>(json['videoURL']),
      primaryColor: serializer.fromJson<String>(json['primaryColor']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'idLocal': serializer.toJson<int>(idLocal),
      'orderID': serializer.toJson<int>(orderID),
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'createBy': serializer.toJson<int>(createBy),
      'updateBy': serializer.toJson<int>(updateBy),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String>(description),
      'imagePath': serializer.toJson<String>(imagePath),
      'isActive': serializer.toJson<bool>(isActive),
      'hasApprovedContent': serializer.toJson<bool>(hasApprovedContent),
      'hasApprovedIT': serializer.toJson<bool>(hasApprovedIT),
      'isSelected': serializer.toJson<bool>(isSelected),
      'iconPath': serializer.toJson<String>(iconPath),
      'videoURL': serializer.toJson<String>(videoURL),
      'primaryColor': serializer.toJson<String>(primaryColor),
    };
  }

  LessonData copyWith(
          {int? id,
          int? idLocal,
          int? orderID,
          DateTime? createDate,
          DateTime? updateDate,
          int? createBy,
          int? updateBy,
          String? title,
          String? description,
          String? imagePath,
          bool? isActive,
          bool? hasApprovedContent,
          bool? hasApprovedIT,
          bool? isSelected,
          String? iconPath,
          String? videoURL,
          String? primaryColor}) =>
      LessonData(
        id: id ?? this.id,
        idLocal: idLocal ?? this.idLocal,
        orderID: orderID ?? this.orderID,
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        createBy: createBy ?? this.createBy,
        updateBy: updateBy ?? this.updateBy,
        title: title ?? this.title,
        description: description ?? this.description,
        imagePath: imagePath ?? this.imagePath,
        isActive: isActive ?? this.isActive,
        hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
        hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
        isSelected: isSelected ?? this.isSelected,
        iconPath: iconPath ?? this.iconPath,
        videoURL: videoURL ?? this.videoURL,
        primaryColor: primaryColor ?? this.primaryColor,
      );
  @override
  String toString() {
    return (StringBuffer('LessonData(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath, ')
          ..write('videoURL: $videoURL, ')
          ..write('primaryColor: $primaryColor')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          idLocal.hashCode,
          $mrjc(
              orderID.hashCode,
              $mrjc(
                  createDate.hashCode,
                  $mrjc(
                      updateDate.hashCode,
                      $mrjc(
                          createBy.hashCode,
                          $mrjc(
                              updateBy.hashCode,
                              $mrjc(
                                  title.hashCode,
                                  $mrjc(
                                      description.hashCode,
                                      $mrjc(
                                          imagePath.hashCode,
                                          $mrjc(
                                              isActive.hashCode,
                                              $mrjc(
                                                  hasApprovedContent.hashCode,
                                                  $mrjc(
                                                      hasApprovedIT.hashCode,
                                                      $mrjc(
                                                          isSelected.hashCode,
                                                          $mrjc(
                                                              iconPath.hashCode,
                                                              $mrjc(
                                                                  videoURL
                                                                      .hashCode,
                                                                  primaryColor
                                                                      .hashCode)))))))))))))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is LessonData &&
          other.id == this.id &&
          other.idLocal == this.idLocal &&
          other.orderID == this.orderID &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.createBy == this.createBy &&
          other.updateBy == this.updateBy &&
          other.title == this.title &&
          other.description == this.description &&
          other.imagePath == this.imagePath &&
          other.isActive == this.isActive &&
          other.hasApprovedContent == this.hasApprovedContent &&
          other.hasApprovedIT == this.hasApprovedIT &&
          other.isSelected == this.isSelected &&
          other.iconPath == this.iconPath &&
          other.videoURL == this.videoURL &&
          other.primaryColor == this.primaryColor);
}

class LessonCompanion extends UpdateCompanion<LessonData> {
  final Value<int> id;
  final Value<int> idLocal;
  final Value<int> orderID;
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<int> createBy;
  final Value<int> updateBy;
  final Value<String> title;
  final Value<String> description;
  final Value<String> imagePath;
  final Value<bool> isActive;
  final Value<bool> hasApprovedContent;
  final Value<bool> hasApprovedIT;
  final Value<bool> isSelected;
  final Value<String> iconPath;
  final Value<String> videoURL;
  final Value<String> primaryColor;
  const LessonCompanion({
    this.id = const Value.absent(),
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.createBy = const Value.absent(),
    this.updateBy = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.videoURL = const Value.absent(),
    this.primaryColor = const Value.absent(),
  });
  LessonCompanion.insert({
    required int id,
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    required int createBy,
    required int updateBy,
    required String title,
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.videoURL = const Value.absent(),
    this.primaryColor = const Value.absent(),
  })  : id = Value(id),
        createBy = Value(createBy),
        updateBy = Value(updateBy),
        title = Value(title);
  static Insertable<LessonData> custom({
    Expression<int>? id,
    Expression<int>? idLocal,
    Expression<int>? orderID,
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<int>? createBy,
    Expression<int>? updateBy,
    Expression<String>? title,
    Expression<String>? description,
    Expression<String>? imagePath,
    Expression<bool>? isActive,
    Expression<bool>? hasApprovedContent,
    Expression<bool>? hasApprovedIT,
    Expression<bool>? isSelected,
    Expression<String>? iconPath,
    Expression<String>? videoURL,
    Expression<String>? primaryColor,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (idLocal != null) 'id_local': idLocal,
      if (orderID != null) 'order_i_d': orderID,
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (createBy != null) 'create_by': createBy,
      if (updateBy != null) 'update_by': updateBy,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (imagePath != null) 'image_path': imagePath,
      if (isActive != null) 'is_active': isActive,
      if (hasApprovedContent != null)
        'has_approved_content': hasApprovedContent,
      if (hasApprovedIT != null) 'has_approved_i_t': hasApprovedIT,
      if (isSelected != null) 'is_selected': isSelected,
      if (iconPath != null) 'icon_path': iconPath,
      if (videoURL != null) 'video_u_r_l': videoURL,
      if (primaryColor != null) 'primary_color': primaryColor,
    });
  }

  LessonCompanion copyWith(
      {Value<int>? id,
      Value<int>? idLocal,
      Value<int>? orderID,
      Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<int>? createBy,
      Value<int>? updateBy,
      Value<String>? title,
      Value<String>? description,
      Value<String>? imagePath,
      Value<bool>? isActive,
      Value<bool>? hasApprovedContent,
      Value<bool>? hasApprovedIT,
      Value<bool>? isSelected,
      Value<String>? iconPath,
      Value<String>? videoURL,
      Value<String>? primaryColor}) {
    return LessonCompanion(
      id: id ?? this.id,
      idLocal: idLocal ?? this.idLocal,
      orderID: orderID ?? this.orderID,
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      createBy: createBy ?? this.createBy,
      updateBy: updateBy ?? this.updateBy,
      title: title ?? this.title,
      description: description ?? this.description,
      imagePath: imagePath ?? this.imagePath,
      isActive: isActive ?? this.isActive,
      hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
      hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
      isSelected: isSelected ?? this.isSelected,
      iconPath: iconPath ?? this.iconPath,
      videoURL: videoURL ?? this.videoURL,
      primaryColor: primaryColor ?? this.primaryColor,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (idLocal.present) {
      map['id_local'] = Variable<int>(idLocal.value);
    }
    if (orderID.present) {
      map['order_i_d'] = Variable<int>(orderID.value);
    }
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (createBy.present) {
      map['create_by'] = Variable<int>(createBy.value);
    }
    if (updateBy.present) {
      map['update_by'] = Variable<int>(updateBy.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (imagePath.present) {
      map['image_path'] = Variable<String>(imagePath.value);
    }
    if (isActive.present) {
      map['is_active'] = Variable<bool>(isActive.value);
    }
    if (hasApprovedContent.present) {
      map['has_approved_content'] = Variable<bool>(hasApprovedContent.value);
    }
    if (hasApprovedIT.present) {
      map['has_approved_i_t'] = Variable<bool>(hasApprovedIT.value);
    }
    if (isSelected.present) {
      map['is_selected'] = Variable<bool>(isSelected.value);
    }
    if (iconPath.present) {
      map['icon_path'] = Variable<String>(iconPath.value);
    }
    if (videoURL.present) {
      map['video_u_r_l'] = Variable<String>(videoURL.value);
    }
    if (primaryColor.present) {
      map['primary_color'] = Variable<String>(primaryColor.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LessonCompanion(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath, ')
          ..write('videoURL: $videoURL, ')
          ..write('primaryColor: $primaryColor')
          ..write(')'))
        .toString();
  }
}

class $LessonTable extends Lesson with TableInfo<$LessonTable, LessonData> {
  final GeneratedDatabase _db;
  final String? _alias;
  $LessonTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _idLocalMeta = const VerificationMeta('idLocal');
  late final GeneratedColumn<int?> idLocal = GeneratedColumn<int?>(
      'id_local', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _orderIDMeta = const VerificationMeta('orderID');
  late final GeneratedColumn<int?> orderID = GeneratedColumn<int?>(
      'order_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _createDateMeta = const VerificationMeta('createDate');
  late final GeneratedColumn<DateTime?> createDate = GeneratedColumn<DateTime?>(
      'create_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _updateDateMeta = const VerificationMeta('updateDate');
  late final GeneratedColumn<DateTime?> updateDate = GeneratedColumn<DateTime?>(
      'update_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _createByMeta = const VerificationMeta('createBy');
  late final GeneratedColumn<int?> createBy = GeneratedColumn<int?>(
      'create_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _updateByMeta = const VerificationMeta('updateBy');
  late final GeneratedColumn<int?> updateBy = GeneratedColumn<int?>(
      'update_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, false,
      typeName: 'TEXT', requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _imagePathMeta = const VerificationMeta('imagePath');
  late final GeneratedColumn<String?> imagePath = GeneratedColumn<String?>(
      'image_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _isActiveMeta = const VerificationMeta('isActive');
  late final GeneratedColumn<bool?> isActive = GeneratedColumn<bool?>(
      'is_active', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_active IN (0, 1))',
      defaultValue: const Constant(true));
  final VerificationMeta _hasApprovedContentMeta =
      const VerificationMeta('hasApprovedContent');
  late final GeneratedColumn<bool?> hasApprovedContent = GeneratedColumn<bool?>(
      'has_approved_content', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_content IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _hasApprovedITMeta =
      const VerificationMeta('hasApprovedIT');
  late final GeneratedColumn<bool?> hasApprovedIT = GeneratedColumn<bool?>(
      'has_approved_i_t', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_i_t IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _isSelectedMeta = const VerificationMeta('isSelected');
  late final GeneratedColumn<bool?> isSelected = GeneratedColumn<bool?>(
      'is_selected', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_selected IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _iconPathMeta = const VerificationMeta('iconPath');
  late final GeneratedColumn<String?> iconPath = GeneratedColumn<String?>(
      'icon_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _videoURLMeta = const VerificationMeta('videoURL');
  late final GeneratedColumn<String?> videoURL = GeneratedColumn<String?>(
      'video_u_r_l', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _primaryColorMeta =
      const VerificationMeta('primaryColor');
  late final GeneratedColumn<String?> primaryColor = GeneratedColumn<String?>(
      'primary_color', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        idLocal,
        orderID,
        createDate,
        updateDate,
        createBy,
        updateBy,
        title,
        description,
        imagePath,
        isActive,
        hasApprovedContent,
        hasApprovedIT,
        isSelected,
        iconPath,
        videoURL,
        primaryColor
      ];
  @override
  String get aliasedName => _alias ?? 'lesson';
  @override
  String get actualTableName => 'lesson';
  @override
  VerificationContext validateIntegrity(Insertable<LessonData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('id_local')) {
      context.handle(_idLocalMeta,
          idLocal.isAcceptableOrUnknown(data['id_local']!, _idLocalMeta));
    }
    if (data.containsKey('order_i_d')) {
      context.handle(_orderIDMeta,
          orderID.isAcceptableOrUnknown(data['order_i_d']!, _orderIDMeta));
    }
    if (data.containsKey('create_date')) {
      context.handle(
          _createDateMeta,
          createDate.isAcceptableOrUnknown(
              data['create_date']!, _createDateMeta));
    }
    if (data.containsKey('update_date')) {
      context.handle(
          _updateDateMeta,
          updateDate.isAcceptableOrUnknown(
              data['update_date']!, _updateDateMeta));
    }
    if (data.containsKey('create_by')) {
      context.handle(_createByMeta,
          createBy.isAcceptableOrUnknown(data['create_by']!, _createByMeta));
    } else if (isInserting) {
      context.missing(_createByMeta);
    }
    if (data.containsKey('update_by')) {
      context.handle(_updateByMeta,
          updateBy.isAcceptableOrUnknown(data['update_by']!, _updateByMeta));
    } else if (isInserting) {
      context.missing(_updateByMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('image_path')) {
      context.handle(_imagePathMeta,
          imagePath.isAcceptableOrUnknown(data['image_path']!, _imagePathMeta));
    }
    if (data.containsKey('is_active')) {
      context.handle(_isActiveMeta,
          isActive.isAcceptableOrUnknown(data['is_active']!, _isActiveMeta));
    }
    if (data.containsKey('has_approved_content')) {
      context.handle(
          _hasApprovedContentMeta,
          hasApprovedContent.isAcceptableOrUnknown(
              data['has_approved_content']!, _hasApprovedContentMeta));
    }
    if (data.containsKey('has_approved_i_t')) {
      context.handle(
          _hasApprovedITMeta,
          hasApprovedIT.isAcceptableOrUnknown(
              data['has_approved_i_t']!, _hasApprovedITMeta));
    }
    if (data.containsKey('is_selected')) {
      context.handle(
          _isSelectedMeta,
          isSelected.isAcceptableOrUnknown(
              data['is_selected']!, _isSelectedMeta));
    }
    if (data.containsKey('icon_path')) {
      context.handle(_iconPathMeta,
          iconPath.isAcceptableOrUnknown(data['icon_path']!, _iconPathMeta));
    }
    if (data.containsKey('video_u_r_l')) {
      context.handle(_videoURLMeta,
          videoURL.isAcceptableOrUnknown(data['video_u_r_l']!, _videoURLMeta));
    }
    if (data.containsKey('primary_color')) {
      context.handle(
          _primaryColorMeta,
          primaryColor.isAcceptableOrUnknown(
              data['primary_color']!, _primaryColorMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idLocal};
  @override
  LessonData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return LessonData.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $LessonTable createAlias(String alias) {
    return $LessonTable(_db, alias);
  }
}

class LessonFormData extends DataClass implements Insertable<LessonFormData> {
  final int id;
  final int idLocal;
  final int orderID;
  final DateTime createDate;
  final DateTime updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;
  final String iconPath;
  final bool isActive;
  final bool hasApprovedContent;
  final bool hasApprovedIT;
  final bool isSelected;
  final int formID;
  final int lessonID;
  LessonFormData(
      {required this.id,
      required this.idLocal,
      required this.orderID,
      required this.createDate,
      required this.updateDate,
      required this.createBy,
      required this.updateBy,
      required this.title,
      required this.description,
      required this.imagePath,
      required this.iconPath,
      required this.isActive,
      required this.hasApprovedContent,
      required this.hasApprovedIT,
      required this.isSelected,
      required this.formID,
      required this.lessonID});
  factory LessonFormData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return LessonFormData(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      idLocal: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id_local'])!,
      orderID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}order_i_d'])!,
      createDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_date'])!,
      updateDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_date'])!,
      createBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_by'])!,
      updateBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_by'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      imagePath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image_path'])!,
      iconPath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}icon_path'])!,
      isActive: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_active'])!,
      hasApprovedContent: const BoolType().mapFromDatabaseResponse(
          data['${effectivePrefix}has_approved_content'])!,
      hasApprovedIT: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}has_approved_i_t'])!,
      isSelected: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_selected'])!,
      formID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}form_i_d'])!,
      lessonID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}lesson_i_d'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['id_local'] = Variable<int>(idLocal);
    map['order_i_d'] = Variable<int>(orderID);
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    map['create_by'] = Variable<int>(createBy);
    map['update_by'] = Variable<int>(updateBy);
    map['title'] = Variable<String>(title);
    map['description'] = Variable<String>(description);
    map['image_path'] = Variable<String>(imagePath);
    map['icon_path'] = Variable<String>(iconPath);
    map['is_active'] = Variable<bool>(isActive);
    map['has_approved_content'] = Variable<bool>(hasApprovedContent);
    map['has_approved_i_t'] = Variable<bool>(hasApprovedIT);
    map['is_selected'] = Variable<bool>(isSelected);
    map['form_i_d'] = Variable<int>(formID);
    map['lesson_i_d'] = Variable<int>(lessonID);
    return map;
  }

  LessonFormCompanion toCompanion(bool nullToAbsent) {
    return LessonFormCompanion(
      id: Value(id),
      idLocal: Value(idLocal),
      orderID: Value(orderID),
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      createBy: Value(createBy),
      updateBy: Value(updateBy),
      title: Value(title),
      description: Value(description),
      imagePath: Value(imagePath),
      iconPath: Value(iconPath),
      isActive: Value(isActive),
      hasApprovedContent: Value(hasApprovedContent),
      hasApprovedIT: Value(hasApprovedIT),
      isSelected: Value(isSelected),
      formID: Value(formID),
      lessonID: Value(lessonID),
    );
  }

  factory LessonFormData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return LessonFormData(
      id: serializer.fromJson<int>(json['id']),
      idLocal: serializer.fromJson<int>(json['idLocal']),
      orderID: serializer.fromJson<int>(json['orderID']),
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      createBy: serializer.fromJson<int>(json['createBy']),
      updateBy: serializer.fromJson<int>(json['updateBy']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String>(json['description']),
      imagePath: serializer.fromJson<String>(json['imagePath']),
      iconPath: serializer.fromJson<String>(json['iconPath']),
      isActive: serializer.fromJson<bool>(json['isActive']),
      hasApprovedContent: serializer.fromJson<bool>(json['hasApprovedContent']),
      hasApprovedIT: serializer.fromJson<bool>(json['hasApprovedIT']),
      isSelected: serializer.fromJson<bool>(json['isSelected']),
      formID: serializer.fromJson<int>(json['formID']),
      lessonID: serializer.fromJson<int>(json['lessonID']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'idLocal': serializer.toJson<int>(idLocal),
      'orderID': serializer.toJson<int>(orderID),
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'createBy': serializer.toJson<int>(createBy),
      'updateBy': serializer.toJson<int>(updateBy),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String>(description),
      'imagePath': serializer.toJson<String>(imagePath),
      'iconPath': serializer.toJson<String>(iconPath),
      'isActive': serializer.toJson<bool>(isActive),
      'hasApprovedContent': serializer.toJson<bool>(hasApprovedContent),
      'hasApprovedIT': serializer.toJson<bool>(hasApprovedIT),
      'isSelected': serializer.toJson<bool>(isSelected),
      'formID': serializer.toJson<int>(formID),
      'lessonID': serializer.toJson<int>(lessonID),
    };
  }

  LessonFormData copyWith(
          {int? id,
          int? idLocal,
          int? orderID,
          DateTime? createDate,
          DateTime? updateDate,
          int? createBy,
          int? updateBy,
          String? title,
          String? description,
          String? imagePath,
          String? iconPath,
          bool? isActive,
          bool? hasApprovedContent,
          bool? hasApprovedIT,
          bool? isSelected,
          int? formID,
          int? lessonID}) =>
      LessonFormData(
        id: id ?? this.id,
        idLocal: idLocal ?? this.idLocal,
        orderID: orderID ?? this.orderID,
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        createBy: createBy ?? this.createBy,
        updateBy: updateBy ?? this.updateBy,
        title: title ?? this.title,
        description: description ?? this.description,
        imagePath: imagePath ?? this.imagePath,
        iconPath: iconPath ?? this.iconPath,
        isActive: isActive ?? this.isActive,
        hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
        hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
        isSelected: isSelected ?? this.isSelected,
        formID: formID ?? this.formID,
        lessonID: lessonID ?? this.lessonID,
      );
  @override
  String toString() {
    return (StringBuffer('LessonFormData(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('iconPath: $iconPath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('formID: $formID, ')
          ..write('lessonID: $lessonID')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          idLocal.hashCode,
          $mrjc(
              orderID.hashCode,
              $mrjc(
                  createDate.hashCode,
                  $mrjc(
                      updateDate.hashCode,
                      $mrjc(
                          createBy.hashCode,
                          $mrjc(
                              updateBy.hashCode,
                              $mrjc(
                                  title.hashCode,
                                  $mrjc(
                                      description.hashCode,
                                      $mrjc(
                                          imagePath.hashCode,
                                          $mrjc(
                                              iconPath.hashCode,
                                              $mrjc(
                                                  isActive.hashCode,
                                                  $mrjc(
                                                      hasApprovedContent
                                                          .hashCode,
                                                      $mrjc(
                                                          hasApprovedIT
                                                              .hashCode,
                                                          $mrjc(
                                                              isSelected
                                                                  .hashCode,
                                                              $mrjc(
                                                                  formID
                                                                      .hashCode,
                                                                  lessonID
                                                                      .hashCode)))))))))))))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is LessonFormData &&
          other.id == this.id &&
          other.idLocal == this.idLocal &&
          other.orderID == this.orderID &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.createBy == this.createBy &&
          other.updateBy == this.updateBy &&
          other.title == this.title &&
          other.description == this.description &&
          other.imagePath == this.imagePath &&
          other.iconPath == this.iconPath &&
          other.isActive == this.isActive &&
          other.hasApprovedContent == this.hasApprovedContent &&
          other.hasApprovedIT == this.hasApprovedIT &&
          other.isSelected == this.isSelected &&
          other.formID == this.formID &&
          other.lessonID == this.lessonID);
}

class LessonFormCompanion extends UpdateCompanion<LessonFormData> {
  final Value<int> id;
  final Value<int> idLocal;
  final Value<int> orderID;
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<int> createBy;
  final Value<int> updateBy;
  final Value<String> title;
  final Value<String> description;
  final Value<String> imagePath;
  final Value<String> iconPath;
  final Value<bool> isActive;
  final Value<bool> hasApprovedContent;
  final Value<bool> hasApprovedIT;
  final Value<bool> isSelected;
  final Value<int> formID;
  final Value<int> lessonID;
  const LessonFormCompanion({
    this.id = const Value.absent(),
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.createBy = const Value.absent(),
    this.updateBy = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.formID = const Value.absent(),
    this.lessonID = const Value.absent(),
  });
  LessonFormCompanion.insert({
    required int id,
    required int idLocal,
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    required int createBy,
    required int updateBy,
    required String title,
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    required int formID,
    required int lessonID,
  })  : id = Value(id),
        idLocal = Value(idLocal),
        createBy = Value(createBy),
        updateBy = Value(updateBy),
        title = Value(title),
        formID = Value(formID),
        lessonID = Value(lessonID);
  static Insertable<LessonFormData> custom({
    Expression<int>? id,
    Expression<int>? idLocal,
    Expression<int>? orderID,
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<int>? createBy,
    Expression<int>? updateBy,
    Expression<String>? title,
    Expression<String>? description,
    Expression<String>? imagePath,
    Expression<String>? iconPath,
    Expression<bool>? isActive,
    Expression<bool>? hasApprovedContent,
    Expression<bool>? hasApprovedIT,
    Expression<bool>? isSelected,
    Expression<int>? formID,
    Expression<int>? lessonID,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (idLocal != null) 'id_local': idLocal,
      if (orderID != null) 'order_i_d': orderID,
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (createBy != null) 'create_by': createBy,
      if (updateBy != null) 'update_by': updateBy,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (imagePath != null) 'image_path': imagePath,
      if (iconPath != null) 'icon_path': iconPath,
      if (isActive != null) 'is_active': isActive,
      if (hasApprovedContent != null)
        'has_approved_content': hasApprovedContent,
      if (hasApprovedIT != null) 'has_approved_i_t': hasApprovedIT,
      if (isSelected != null) 'is_selected': isSelected,
      if (formID != null) 'form_i_d': formID,
      if (lessonID != null) 'lesson_i_d': lessonID,
    });
  }

  LessonFormCompanion copyWith(
      {Value<int>? id,
      Value<int>? idLocal,
      Value<int>? orderID,
      Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<int>? createBy,
      Value<int>? updateBy,
      Value<String>? title,
      Value<String>? description,
      Value<String>? imagePath,
      Value<String>? iconPath,
      Value<bool>? isActive,
      Value<bool>? hasApprovedContent,
      Value<bool>? hasApprovedIT,
      Value<bool>? isSelected,
      Value<int>? formID,
      Value<int>? lessonID}) {
    return LessonFormCompanion(
      id: id ?? this.id,
      idLocal: idLocal ?? this.idLocal,
      orderID: orderID ?? this.orderID,
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      createBy: createBy ?? this.createBy,
      updateBy: updateBy ?? this.updateBy,
      title: title ?? this.title,
      description: description ?? this.description,
      imagePath: imagePath ?? this.imagePath,
      iconPath: iconPath ?? this.iconPath,
      isActive: isActive ?? this.isActive,
      hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
      hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
      isSelected: isSelected ?? this.isSelected,
      formID: formID ?? this.formID,
      lessonID: lessonID ?? this.lessonID,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (idLocal.present) {
      map['id_local'] = Variable<int>(idLocal.value);
    }
    if (orderID.present) {
      map['order_i_d'] = Variable<int>(orderID.value);
    }
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (createBy.present) {
      map['create_by'] = Variable<int>(createBy.value);
    }
    if (updateBy.present) {
      map['update_by'] = Variable<int>(updateBy.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (imagePath.present) {
      map['image_path'] = Variable<String>(imagePath.value);
    }
    if (iconPath.present) {
      map['icon_path'] = Variable<String>(iconPath.value);
    }
    if (isActive.present) {
      map['is_active'] = Variable<bool>(isActive.value);
    }
    if (hasApprovedContent.present) {
      map['has_approved_content'] = Variable<bool>(hasApprovedContent.value);
    }
    if (hasApprovedIT.present) {
      map['has_approved_i_t'] = Variable<bool>(hasApprovedIT.value);
    }
    if (isSelected.present) {
      map['is_selected'] = Variable<bool>(isSelected.value);
    }
    if (formID.present) {
      map['form_i_d'] = Variable<int>(formID.value);
    }
    if (lessonID.present) {
      map['lesson_i_d'] = Variable<int>(lessonID.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LessonFormCompanion(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('iconPath: $iconPath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('formID: $formID, ')
          ..write('lessonID: $lessonID')
          ..write(')'))
        .toString();
  }
}

class $LessonFormTable extends LessonForm
    with TableInfo<$LessonFormTable, LessonFormData> {
  final GeneratedDatabase _db;
  final String? _alias;
  $LessonFormTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _idLocalMeta = const VerificationMeta('idLocal');
  late final GeneratedColumn<int?> idLocal = GeneratedColumn<int?>(
      'id_local', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: true,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _orderIDMeta = const VerificationMeta('orderID');
  late final GeneratedColumn<int?> orderID = GeneratedColumn<int?>(
      'order_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _createDateMeta = const VerificationMeta('createDate');
  late final GeneratedColumn<DateTime?> createDate = GeneratedColumn<DateTime?>(
      'create_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _updateDateMeta = const VerificationMeta('updateDate');
  late final GeneratedColumn<DateTime?> updateDate = GeneratedColumn<DateTime?>(
      'update_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _createByMeta = const VerificationMeta('createBy');
  late final GeneratedColumn<int?> createBy = GeneratedColumn<int?>(
      'create_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _updateByMeta = const VerificationMeta('updateBy');
  late final GeneratedColumn<int?> updateBy = GeneratedColumn<int?>(
      'update_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, false,
      typeName: 'TEXT', requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _imagePathMeta = const VerificationMeta('imagePath');
  late final GeneratedColumn<String?> imagePath = GeneratedColumn<String?>(
      'image_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _iconPathMeta = const VerificationMeta('iconPath');
  late final GeneratedColumn<String?> iconPath = GeneratedColumn<String?>(
      'icon_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _isActiveMeta = const VerificationMeta('isActive');
  late final GeneratedColumn<bool?> isActive = GeneratedColumn<bool?>(
      'is_active', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_active IN (0, 1))',
      defaultValue: const Constant(true));
  final VerificationMeta _hasApprovedContentMeta =
      const VerificationMeta('hasApprovedContent');
  late final GeneratedColumn<bool?> hasApprovedContent = GeneratedColumn<bool?>(
      'has_approved_content', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_content IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _hasApprovedITMeta =
      const VerificationMeta('hasApprovedIT');
  late final GeneratedColumn<bool?> hasApprovedIT = GeneratedColumn<bool?>(
      'has_approved_i_t', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_i_t IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _isSelectedMeta = const VerificationMeta('isSelected');
  late final GeneratedColumn<bool?> isSelected = GeneratedColumn<bool?>(
      'is_selected', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_selected IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _formIDMeta = const VerificationMeta('formID');
  late final GeneratedColumn<int?> formID = GeneratedColumn<int?>(
      'form_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: true,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _lessonIDMeta = const VerificationMeta('lessonID');
  late final GeneratedColumn<int?> lessonID = GeneratedColumn<int?>(
      'lesson_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: true,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  @override
  List<GeneratedColumn> get $columns => [
        id,
        idLocal,
        orderID,
        createDate,
        updateDate,
        createBy,
        updateBy,
        title,
        description,
        imagePath,
        iconPath,
        isActive,
        hasApprovedContent,
        hasApprovedIT,
        isSelected,
        formID,
        lessonID
      ];
  @override
  String get aliasedName => _alias ?? 'lesson_form';
  @override
  String get actualTableName => 'lesson_form';
  @override
  VerificationContext validateIntegrity(Insertable<LessonFormData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('id_local')) {
      context.handle(_idLocalMeta,
          idLocal.isAcceptableOrUnknown(data['id_local']!, _idLocalMeta));
    } else if (isInserting) {
      context.missing(_idLocalMeta);
    }
    if (data.containsKey('order_i_d')) {
      context.handle(_orderIDMeta,
          orderID.isAcceptableOrUnknown(data['order_i_d']!, _orderIDMeta));
    }
    if (data.containsKey('create_date')) {
      context.handle(
          _createDateMeta,
          createDate.isAcceptableOrUnknown(
              data['create_date']!, _createDateMeta));
    }
    if (data.containsKey('update_date')) {
      context.handle(
          _updateDateMeta,
          updateDate.isAcceptableOrUnknown(
              data['update_date']!, _updateDateMeta));
    }
    if (data.containsKey('create_by')) {
      context.handle(_createByMeta,
          createBy.isAcceptableOrUnknown(data['create_by']!, _createByMeta));
    } else if (isInserting) {
      context.missing(_createByMeta);
    }
    if (data.containsKey('update_by')) {
      context.handle(_updateByMeta,
          updateBy.isAcceptableOrUnknown(data['update_by']!, _updateByMeta));
    } else if (isInserting) {
      context.missing(_updateByMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('image_path')) {
      context.handle(_imagePathMeta,
          imagePath.isAcceptableOrUnknown(data['image_path']!, _imagePathMeta));
    }
    if (data.containsKey('icon_path')) {
      context.handle(_iconPathMeta,
          iconPath.isAcceptableOrUnknown(data['icon_path']!, _iconPathMeta));
    }
    if (data.containsKey('is_active')) {
      context.handle(_isActiveMeta,
          isActive.isAcceptableOrUnknown(data['is_active']!, _isActiveMeta));
    }
    if (data.containsKey('has_approved_content')) {
      context.handle(
          _hasApprovedContentMeta,
          hasApprovedContent.isAcceptableOrUnknown(
              data['has_approved_content']!, _hasApprovedContentMeta));
    }
    if (data.containsKey('has_approved_i_t')) {
      context.handle(
          _hasApprovedITMeta,
          hasApprovedIT.isAcceptableOrUnknown(
              data['has_approved_i_t']!, _hasApprovedITMeta));
    }
    if (data.containsKey('is_selected')) {
      context.handle(
          _isSelectedMeta,
          isSelected.isAcceptableOrUnknown(
              data['is_selected']!, _isSelectedMeta));
    }
    if (data.containsKey('form_i_d')) {
      context.handle(_formIDMeta,
          formID.isAcceptableOrUnknown(data['form_i_d']!, _formIDMeta));
    } else if (isInserting) {
      context.missing(_formIDMeta);
    }
    if (data.containsKey('lesson_i_d')) {
      context.handle(_lessonIDMeta,
          lessonID.isAcceptableOrUnknown(data['lesson_i_d']!, _lessonIDMeta));
    } else if (isInserting) {
      context.missing(_lessonIDMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idLocal, formID, lessonID};
  @override
  LessonFormData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return LessonFormData.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $LessonFormTable createAlias(String alias) {
    return $LessonFormTable(_db, alias);
  }
}

class FormData extends DataClass implements Insertable<FormData> {
  final int id;
  final int idLocal;
  final int orderID;
  final DateTime createDate;
  final DateTime updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;
  final bool isActive;
  final bool hasApprovedContent;
  final bool hasApprovedIT;
  final bool isSelected;
  final String iconPath;
  final String videoURL;
  final int formType;
  FormData(
      {required this.id,
      required this.idLocal,
      required this.orderID,
      required this.createDate,
      required this.updateDate,
      required this.createBy,
      required this.updateBy,
      required this.title,
      required this.description,
      required this.imagePath,
      required this.isActive,
      required this.hasApprovedContent,
      required this.hasApprovedIT,
      required this.isSelected,
      required this.iconPath,
      required this.videoURL,
      required this.formType});
  factory FormData.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return FormData(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      idLocal: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id_local'])!,
      orderID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}order_i_d'])!,
      createDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_date'])!,
      updateDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_date'])!,
      createBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_by'])!,
      updateBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_by'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      imagePath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image_path'])!,
      isActive: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_active'])!,
      hasApprovedContent: const BoolType().mapFromDatabaseResponse(
          data['${effectivePrefix}has_approved_content'])!,
      hasApprovedIT: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}has_approved_i_t'])!,
      isSelected: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_selected'])!,
      iconPath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}icon_path'])!,
      videoURL: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}video_u_r_l'])!,
      formType: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}form_type'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['id_local'] = Variable<int>(idLocal);
    map['order_i_d'] = Variable<int>(orderID);
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    map['create_by'] = Variable<int>(createBy);
    map['update_by'] = Variable<int>(updateBy);
    map['title'] = Variable<String>(title);
    map['description'] = Variable<String>(description);
    map['image_path'] = Variable<String>(imagePath);
    map['is_active'] = Variable<bool>(isActive);
    map['has_approved_content'] = Variable<bool>(hasApprovedContent);
    map['has_approved_i_t'] = Variable<bool>(hasApprovedIT);
    map['is_selected'] = Variable<bool>(isSelected);
    map['icon_path'] = Variable<String>(iconPath);
    map['video_u_r_l'] = Variable<String>(videoURL);
    map['form_type'] = Variable<int>(formType);
    return map;
  }

  FormCompanion toCompanion(bool nullToAbsent) {
    return FormCompanion(
      id: Value(id),
      idLocal: Value(idLocal),
      orderID: Value(orderID),
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      createBy: Value(createBy),
      updateBy: Value(updateBy),
      title: Value(title),
      description: Value(description),
      imagePath: Value(imagePath),
      isActive: Value(isActive),
      hasApprovedContent: Value(hasApprovedContent),
      hasApprovedIT: Value(hasApprovedIT),
      isSelected: Value(isSelected),
      iconPath: Value(iconPath),
      videoURL: Value(videoURL),
      formType: Value(formType),
    );
  }

  factory FormData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return FormData(
      id: serializer.fromJson<int>(json['id']),
      idLocal: serializer.fromJson<int>(json['idLocal']),
      orderID: serializer.fromJson<int>(json['orderID']),
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      createBy: serializer.fromJson<int>(json['createBy']),
      updateBy: serializer.fromJson<int>(json['updateBy']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String>(json['description']),
      imagePath: serializer.fromJson<String>(json['imagePath']),
      isActive: serializer.fromJson<bool>(json['isActive']),
      hasApprovedContent: serializer.fromJson<bool>(json['hasApprovedContent']),
      hasApprovedIT: serializer.fromJson<bool>(json['hasApprovedIT']),
      isSelected: serializer.fromJson<bool>(json['isSelected']),
      iconPath: serializer.fromJson<String>(json['iconPath']),
      videoURL: serializer.fromJson<String>(json['videoURL']),
      formType: serializer.fromJson<int>(json['formType']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'idLocal': serializer.toJson<int>(idLocal),
      'orderID': serializer.toJson<int>(orderID),
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'createBy': serializer.toJson<int>(createBy),
      'updateBy': serializer.toJson<int>(updateBy),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String>(description),
      'imagePath': serializer.toJson<String>(imagePath),
      'isActive': serializer.toJson<bool>(isActive),
      'hasApprovedContent': serializer.toJson<bool>(hasApprovedContent),
      'hasApprovedIT': serializer.toJson<bool>(hasApprovedIT),
      'isSelected': serializer.toJson<bool>(isSelected),
      'iconPath': serializer.toJson<String>(iconPath),
      'videoURL': serializer.toJson<String>(videoURL),
      'formType': serializer.toJson<int>(formType),
    };
  }

  FormData copyWith(
          {int? id,
          int? idLocal,
          int? orderID,
          DateTime? createDate,
          DateTime? updateDate,
          int? createBy,
          int? updateBy,
          String? title,
          String? description,
          String? imagePath,
          bool? isActive,
          bool? hasApprovedContent,
          bool? hasApprovedIT,
          bool? isSelected,
          String? iconPath,
          String? videoURL,
          int? formType}) =>
      FormData(
        id: id ?? this.id,
        idLocal: idLocal ?? this.idLocal,
        orderID: orderID ?? this.orderID,
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        createBy: createBy ?? this.createBy,
        updateBy: updateBy ?? this.updateBy,
        title: title ?? this.title,
        description: description ?? this.description,
        imagePath: imagePath ?? this.imagePath,
        isActive: isActive ?? this.isActive,
        hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
        hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
        isSelected: isSelected ?? this.isSelected,
        iconPath: iconPath ?? this.iconPath,
        videoURL: videoURL ?? this.videoURL,
        formType: formType ?? this.formType,
      );
  @override
  String toString() {
    return (StringBuffer('FormData(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath, ')
          ..write('videoURL: $videoURL, ')
          ..write('formType: $formType')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          idLocal.hashCode,
          $mrjc(
              orderID.hashCode,
              $mrjc(
                  createDate.hashCode,
                  $mrjc(
                      updateDate.hashCode,
                      $mrjc(
                          createBy.hashCode,
                          $mrjc(
                              updateBy.hashCode,
                              $mrjc(
                                  title.hashCode,
                                  $mrjc(
                                      description.hashCode,
                                      $mrjc(
                                          imagePath.hashCode,
                                          $mrjc(
                                              isActive.hashCode,
                                              $mrjc(
                                                  hasApprovedContent.hashCode,
                                                  $mrjc(
                                                      hasApprovedIT.hashCode,
                                                      $mrjc(
                                                          isSelected.hashCode,
                                                          $mrjc(
                                                              iconPath.hashCode,
                                                              $mrjc(
                                                                  videoURL
                                                                      .hashCode,
                                                                  formType
                                                                      .hashCode)))))))))))))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FormData &&
          other.id == this.id &&
          other.idLocal == this.idLocal &&
          other.orderID == this.orderID &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.createBy == this.createBy &&
          other.updateBy == this.updateBy &&
          other.title == this.title &&
          other.description == this.description &&
          other.imagePath == this.imagePath &&
          other.isActive == this.isActive &&
          other.hasApprovedContent == this.hasApprovedContent &&
          other.hasApprovedIT == this.hasApprovedIT &&
          other.isSelected == this.isSelected &&
          other.iconPath == this.iconPath &&
          other.videoURL == this.videoURL &&
          other.formType == this.formType);
}

class FormCompanion extends UpdateCompanion<FormData> {
  final Value<int> id;
  final Value<int> idLocal;
  final Value<int> orderID;
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<int> createBy;
  final Value<int> updateBy;
  final Value<String> title;
  final Value<String> description;
  final Value<String> imagePath;
  final Value<bool> isActive;
  final Value<bool> hasApprovedContent;
  final Value<bool> hasApprovedIT;
  final Value<bool> isSelected;
  final Value<String> iconPath;
  final Value<String> videoURL;
  final Value<int> formType;
  const FormCompanion({
    this.id = const Value.absent(),
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.createBy = const Value.absent(),
    this.updateBy = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.videoURL = const Value.absent(),
    this.formType = const Value.absent(),
  });
  FormCompanion.insert({
    required int id,
    required int idLocal,
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    required int createBy,
    required int updateBy,
    required String title,
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
    this.iconPath = const Value.absent(),
    this.videoURL = const Value.absent(),
    required int formType,
  })  : id = Value(id),
        idLocal = Value(idLocal),
        createBy = Value(createBy),
        updateBy = Value(updateBy),
        title = Value(title),
        formType = Value(formType);
  static Insertable<FormData> custom({
    Expression<int>? id,
    Expression<int>? idLocal,
    Expression<int>? orderID,
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<int>? createBy,
    Expression<int>? updateBy,
    Expression<String>? title,
    Expression<String>? description,
    Expression<String>? imagePath,
    Expression<bool>? isActive,
    Expression<bool>? hasApprovedContent,
    Expression<bool>? hasApprovedIT,
    Expression<bool>? isSelected,
    Expression<String>? iconPath,
    Expression<String>? videoURL,
    Expression<int>? formType,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (idLocal != null) 'id_local': idLocal,
      if (orderID != null) 'order_i_d': orderID,
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (createBy != null) 'create_by': createBy,
      if (updateBy != null) 'update_by': updateBy,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (imagePath != null) 'image_path': imagePath,
      if (isActive != null) 'is_active': isActive,
      if (hasApprovedContent != null)
        'has_approved_content': hasApprovedContent,
      if (hasApprovedIT != null) 'has_approved_i_t': hasApprovedIT,
      if (isSelected != null) 'is_selected': isSelected,
      if (iconPath != null) 'icon_path': iconPath,
      if (videoURL != null) 'video_u_r_l': videoURL,
      if (formType != null) 'form_type': formType,
    });
  }

  FormCompanion copyWith(
      {Value<int>? id,
      Value<int>? idLocal,
      Value<int>? orderID,
      Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<int>? createBy,
      Value<int>? updateBy,
      Value<String>? title,
      Value<String>? description,
      Value<String>? imagePath,
      Value<bool>? isActive,
      Value<bool>? hasApprovedContent,
      Value<bool>? hasApprovedIT,
      Value<bool>? isSelected,
      Value<String>? iconPath,
      Value<String>? videoURL,
      Value<int>? formType}) {
    return FormCompanion(
      id: id ?? this.id,
      idLocal: idLocal ?? this.idLocal,
      orderID: orderID ?? this.orderID,
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      createBy: createBy ?? this.createBy,
      updateBy: updateBy ?? this.updateBy,
      title: title ?? this.title,
      description: description ?? this.description,
      imagePath: imagePath ?? this.imagePath,
      isActive: isActive ?? this.isActive,
      hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
      hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
      isSelected: isSelected ?? this.isSelected,
      iconPath: iconPath ?? this.iconPath,
      videoURL: videoURL ?? this.videoURL,
      formType: formType ?? this.formType,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (idLocal.present) {
      map['id_local'] = Variable<int>(idLocal.value);
    }
    if (orderID.present) {
      map['order_i_d'] = Variable<int>(orderID.value);
    }
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (createBy.present) {
      map['create_by'] = Variable<int>(createBy.value);
    }
    if (updateBy.present) {
      map['update_by'] = Variable<int>(updateBy.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (imagePath.present) {
      map['image_path'] = Variable<String>(imagePath.value);
    }
    if (isActive.present) {
      map['is_active'] = Variable<bool>(isActive.value);
    }
    if (hasApprovedContent.present) {
      map['has_approved_content'] = Variable<bool>(hasApprovedContent.value);
    }
    if (hasApprovedIT.present) {
      map['has_approved_i_t'] = Variable<bool>(hasApprovedIT.value);
    }
    if (isSelected.present) {
      map['is_selected'] = Variable<bool>(isSelected.value);
    }
    if (iconPath.present) {
      map['icon_path'] = Variable<String>(iconPath.value);
    }
    if (videoURL.present) {
      map['video_u_r_l'] = Variable<String>(videoURL.value);
    }
    if (formType.present) {
      map['form_type'] = Variable<int>(formType.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FormCompanion(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected, ')
          ..write('iconPath: $iconPath, ')
          ..write('videoURL: $videoURL, ')
          ..write('formType: $formType')
          ..write(')'))
        .toString();
  }
}

class $FormTable extends Form with TableInfo<$FormTable, FormData> {
  final GeneratedDatabase _db;
  final String? _alias;
  $FormTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _idLocalMeta = const VerificationMeta('idLocal');
  late final GeneratedColumn<int?> idLocal = GeneratedColumn<int?>(
      'id_local', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: true,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _orderIDMeta = const VerificationMeta('orderID');
  late final GeneratedColumn<int?> orderID = GeneratedColumn<int?>(
      'order_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _createDateMeta = const VerificationMeta('createDate');
  late final GeneratedColumn<DateTime?> createDate = GeneratedColumn<DateTime?>(
      'create_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _updateDateMeta = const VerificationMeta('updateDate');
  late final GeneratedColumn<DateTime?> updateDate = GeneratedColumn<DateTime?>(
      'update_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _createByMeta = const VerificationMeta('createBy');
  late final GeneratedColumn<int?> createBy = GeneratedColumn<int?>(
      'create_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _updateByMeta = const VerificationMeta('updateBy');
  late final GeneratedColumn<int?> updateBy = GeneratedColumn<int?>(
      'update_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, false,
      typeName: 'TEXT', requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _imagePathMeta = const VerificationMeta('imagePath');
  late final GeneratedColumn<String?> imagePath = GeneratedColumn<String?>(
      'image_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _isActiveMeta = const VerificationMeta('isActive');
  late final GeneratedColumn<bool?> isActive = GeneratedColumn<bool?>(
      'is_active', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_active IN (0, 1))',
      defaultValue: const Constant(true));
  final VerificationMeta _hasApprovedContentMeta =
      const VerificationMeta('hasApprovedContent');
  late final GeneratedColumn<bool?> hasApprovedContent = GeneratedColumn<bool?>(
      'has_approved_content', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_content IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _hasApprovedITMeta =
      const VerificationMeta('hasApprovedIT');
  late final GeneratedColumn<bool?> hasApprovedIT = GeneratedColumn<bool?>(
      'has_approved_i_t', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_i_t IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _isSelectedMeta = const VerificationMeta('isSelected');
  late final GeneratedColumn<bool?> isSelected = GeneratedColumn<bool?>(
      'is_selected', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_selected IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _iconPathMeta = const VerificationMeta('iconPath');
  late final GeneratedColumn<String?> iconPath = GeneratedColumn<String?>(
      'icon_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _videoURLMeta = const VerificationMeta('videoURL');
  late final GeneratedColumn<String?> videoURL = GeneratedColumn<String?>(
      'video_u_r_l', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _formTypeMeta = const VerificationMeta('formType');
  late final GeneratedColumn<int?> formType = GeneratedColumn<int?>(
      'form_type', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: true,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  @override
  List<GeneratedColumn> get $columns => [
        id,
        idLocal,
        orderID,
        createDate,
        updateDate,
        createBy,
        updateBy,
        title,
        description,
        imagePath,
        isActive,
        hasApprovedContent,
        hasApprovedIT,
        isSelected,
        iconPath,
        videoURL,
        formType
      ];
  @override
  String get aliasedName => _alias ?? 'form';
  @override
  String get actualTableName => 'form';
  @override
  VerificationContext validateIntegrity(Insertable<FormData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('id_local')) {
      context.handle(_idLocalMeta,
          idLocal.isAcceptableOrUnknown(data['id_local']!, _idLocalMeta));
    } else if (isInserting) {
      context.missing(_idLocalMeta);
    }
    if (data.containsKey('order_i_d')) {
      context.handle(_orderIDMeta,
          orderID.isAcceptableOrUnknown(data['order_i_d']!, _orderIDMeta));
    }
    if (data.containsKey('create_date')) {
      context.handle(
          _createDateMeta,
          createDate.isAcceptableOrUnknown(
              data['create_date']!, _createDateMeta));
    }
    if (data.containsKey('update_date')) {
      context.handle(
          _updateDateMeta,
          updateDate.isAcceptableOrUnknown(
              data['update_date']!, _updateDateMeta));
    }
    if (data.containsKey('create_by')) {
      context.handle(_createByMeta,
          createBy.isAcceptableOrUnknown(data['create_by']!, _createByMeta));
    } else if (isInserting) {
      context.missing(_createByMeta);
    }
    if (data.containsKey('update_by')) {
      context.handle(_updateByMeta,
          updateBy.isAcceptableOrUnknown(data['update_by']!, _updateByMeta));
    } else if (isInserting) {
      context.missing(_updateByMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('image_path')) {
      context.handle(_imagePathMeta,
          imagePath.isAcceptableOrUnknown(data['image_path']!, _imagePathMeta));
    }
    if (data.containsKey('is_active')) {
      context.handle(_isActiveMeta,
          isActive.isAcceptableOrUnknown(data['is_active']!, _isActiveMeta));
    }
    if (data.containsKey('has_approved_content')) {
      context.handle(
          _hasApprovedContentMeta,
          hasApprovedContent.isAcceptableOrUnknown(
              data['has_approved_content']!, _hasApprovedContentMeta));
    }
    if (data.containsKey('has_approved_i_t')) {
      context.handle(
          _hasApprovedITMeta,
          hasApprovedIT.isAcceptableOrUnknown(
              data['has_approved_i_t']!, _hasApprovedITMeta));
    }
    if (data.containsKey('is_selected')) {
      context.handle(
          _isSelectedMeta,
          isSelected.isAcceptableOrUnknown(
              data['is_selected']!, _isSelectedMeta));
    }
    if (data.containsKey('icon_path')) {
      context.handle(_iconPathMeta,
          iconPath.isAcceptableOrUnknown(data['icon_path']!, _iconPathMeta));
    }
    if (data.containsKey('video_u_r_l')) {
      context.handle(_videoURLMeta,
          videoURL.isAcceptableOrUnknown(data['video_u_r_l']!, _videoURLMeta));
    }
    if (data.containsKey('form_type')) {
      context.handle(_formTypeMeta,
          formType.isAcceptableOrUnknown(data['form_type']!, _formTypeMeta));
    } else if (isInserting) {
      context.missing(_formTypeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idLocal, formType};
  @override
  FormData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return FormData.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $FormTable createAlias(String alias) {
    return $FormTable(_db, alias);
  }
}

class QuestionData extends DataClass implements Insertable<QuestionData> {
  final int id;
  final int idLocal;
  final int orderID;
  final DateTime createDate;
  final DateTime updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;
  final bool isActive;
  final bool hasApprovedContent;
  final bool hasApprovedIT;
  final bool isSelected;
  QuestionData(
      {required this.id,
      required this.idLocal,
      required this.orderID,
      required this.createDate,
      required this.updateDate,
      required this.createBy,
      required this.updateBy,
      required this.title,
      required this.description,
      required this.imagePath,
      required this.isActive,
      required this.hasApprovedContent,
      required this.hasApprovedIT,
      required this.isSelected});
  factory QuestionData.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return QuestionData(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      idLocal: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id_local'])!,
      orderID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}order_i_d'])!,
      createDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_date'])!,
      updateDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_date'])!,
      createBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_by'])!,
      updateBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_by'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      imagePath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image_path'])!,
      isActive: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_active'])!,
      hasApprovedContent: const BoolType().mapFromDatabaseResponse(
          data['${effectivePrefix}has_approved_content'])!,
      hasApprovedIT: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}has_approved_i_t'])!,
      isSelected: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_selected'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['id_local'] = Variable<int>(idLocal);
    map['order_i_d'] = Variable<int>(orderID);
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    map['create_by'] = Variable<int>(createBy);
    map['update_by'] = Variable<int>(updateBy);
    map['title'] = Variable<String>(title);
    map['description'] = Variable<String>(description);
    map['image_path'] = Variable<String>(imagePath);
    map['is_active'] = Variable<bool>(isActive);
    map['has_approved_content'] = Variable<bool>(hasApprovedContent);
    map['has_approved_i_t'] = Variable<bool>(hasApprovedIT);
    map['is_selected'] = Variable<bool>(isSelected);
    return map;
  }

  QuestionCompanion toCompanion(bool nullToAbsent) {
    return QuestionCompanion(
      id: Value(id),
      idLocal: Value(idLocal),
      orderID: Value(orderID),
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      createBy: Value(createBy),
      updateBy: Value(updateBy),
      title: Value(title),
      description: Value(description),
      imagePath: Value(imagePath),
      isActive: Value(isActive),
      hasApprovedContent: Value(hasApprovedContent),
      hasApprovedIT: Value(hasApprovedIT),
      isSelected: Value(isSelected),
    );
  }

  factory QuestionData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return QuestionData(
      id: serializer.fromJson<int>(json['id']),
      idLocal: serializer.fromJson<int>(json['idLocal']),
      orderID: serializer.fromJson<int>(json['orderID']),
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      createBy: serializer.fromJson<int>(json['createBy']),
      updateBy: serializer.fromJson<int>(json['updateBy']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String>(json['description']),
      imagePath: serializer.fromJson<String>(json['imagePath']),
      isActive: serializer.fromJson<bool>(json['isActive']),
      hasApprovedContent: serializer.fromJson<bool>(json['hasApprovedContent']),
      hasApprovedIT: serializer.fromJson<bool>(json['hasApprovedIT']),
      isSelected: serializer.fromJson<bool>(json['isSelected']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'idLocal': serializer.toJson<int>(idLocal),
      'orderID': serializer.toJson<int>(orderID),
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'createBy': serializer.toJson<int>(createBy),
      'updateBy': serializer.toJson<int>(updateBy),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String>(description),
      'imagePath': serializer.toJson<String>(imagePath),
      'isActive': serializer.toJson<bool>(isActive),
      'hasApprovedContent': serializer.toJson<bool>(hasApprovedContent),
      'hasApprovedIT': serializer.toJson<bool>(hasApprovedIT),
      'isSelected': serializer.toJson<bool>(isSelected),
    };
  }

  QuestionData copyWith(
          {int? id,
          int? idLocal,
          int? orderID,
          DateTime? createDate,
          DateTime? updateDate,
          int? createBy,
          int? updateBy,
          String? title,
          String? description,
          String? imagePath,
          bool? isActive,
          bool? hasApprovedContent,
          bool? hasApprovedIT,
          bool? isSelected}) =>
      QuestionData(
        id: id ?? this.id,
        idLocal: idLocal ?? this.idLocal,
        orderID: orderID ?? this.orderID,
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        createBy: createBy ?? this.createBy,
        updateBy: updateBy ?? this.updateBy,
        title: title ?? this.title,
        description: description ?? this.description,
        imagePath: imagePath ?? this.imagePath,
        isActive: isActive ?? this.isActive,
        hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
        hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
        isSelected: isSelected ?? this.isSelected,
      );
  @override
  String toString() {
    return (StringBuffer('QuestionData(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          idLocal.hashCode,
          $mrjc(
              orderID.hashCode,
              $mrjc(
                  createDate.hashCode,
                  $mrjc(
                      updateDate.hashCode,
                      $mrjc(
                          createBy.hashCode,
                          $mrjc(
                              updateBy.hashCode,
                              $mrjc(
                                  title.hashCode,
                                  $mrjc(
                                      description.hashCode,
                                      $mrjc(
                                          imagePath.hashCode,
                                          $mrjc(
                                              isActive.hashCode,
                                              $mrjc(
                                                  hasApprovedContent.hashCode,
                                                  $mrjc(
                                                      hasApprovedIT.hashCode,
                                                      isSelected
                                                          .hashCode))))))))))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is QuestionData &&
          other.id == this.id &&
          other.idLocal == this.idLocal &&
          other.orderID == this.orderID &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.createBy == this.createBy &&
          other.updateBy == this.updateBy &&
          other.title == this.title &&
          other.description == this.description &&
          other.imagePath == this.imagePath &&
          other.isActive == this.isActive &&
          other.hasApprovedContent == this.hasApprovedContent &&
          other.hasApprovedIT == this.hasApprovedIT &&
          other.isSelected == this.isSelected);
}

class QuestionCompanion extends UpdateCompanion<QuestionData> {
  final Value<int> id;
  final Value<int> idLocal;
  final Value<int> orderID;
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<int> createBy;
  final Value<int> updateBy;
  final Value<String> title;
  final Value<String> description;
  final Value<String> imagePath;
  final Value<bool> isActive;
  final Value<bool> hasApprovedContent;
  final Value<bool> hasApprovedIT;
  final Value<bool> isSelected;
  const QuestionCompanion({
    this.id = const Value.absent(),
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.createBy = const Value.absent(),
    this.updateBy = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
  });
  QuestionCompanion.insert({
    required int id,
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    required int createBy,
    required int updateBy,
    required String title,
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
  })  : id = Value(id),
        createBy = Value(createBy),
        updateBy = Value(updateBy),
        title = Value(title);
  static Insertable<QuestionData> custom({
    Expression<int>? id,
    Expression<int>? idLocal,
    Expression<int>? orderID,
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<int>? createBy,
    Expression<int>? updateBy,
    Expression<String>? title,
    Expression<String>? description,
    Expression<String>? imagePath,
    Expression<bool>? isActive,
    Expression<bool>? hasApprovedContent,
    Expression<bool>? hasApprovedIT,
    Expression<bool>? isSelected,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (idLocal != null) 'id_local': idLocal,
      if (orderID != null) 'order_i_d': orderID,
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (createBy != null) 'create_by': createBy,
      if (updateBy != null) 'update_by': updateBy,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (imagePath != null) 'image_path': imagePath,
      if (isActive != null) 'is_active': isActive,
      if (hasApprovedContent != null)
        'has_approved_content': hasApprovedContent,
      if (hasApprovedIT != null) 'has_approved_i_t': hasApprovedIT,
      if (isSelected != null) 'is_selected': isSelected,
    });
  }

  QuestionCompanion copyWith(
      {Value<int>? id,
      Value<int>? idLocal,
      Value<int>? orderID,
      Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<int>? createBy,
      Value<int>? updateBy,
      Value<String>? title,
      Value<String>? description,
      Value<String>? imagePath,
      Value<bool>? isActive,
      Value<bool>? hasApprovedContent,
      Value<bool>? hasApprovedIT,
      Value<bool>? isSelected}) {
    return QuestionCompanion(
      id: id ?? this.id,
      idLocal: idLocal ?? this.idLocal,
      orderID: orderID ?? this.orderID,
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      createBy: createBy ?? this.createBy,
      updateBy: updateBy ?? this.updateBy,
      title: title ?? this.title,
      description: description ?? this.description,
      imagePath: imagePath ?? this.imagePath,
      isActive: isActive ?? this.isActive,
      hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
      hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
      isSelected: isSelected ?? this.isSelected,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (idLocal.present) {
      map['id_local'] = Variable<int>(idLocal.value);
    }
    if (orderID.present) {
      map['order_i_d'] = Variable<int>(orderID.value);
    }
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (createBy.present) {
      map['create_by'] = Variable<int>(createBy.value);
    }
    if (updateBy.present) {
      map['update_by'] = Variable<int>(updateBy.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (imagePath.present) {
      map['image_path'] = Variable<String>(imagePath.value);
    }
    if (isActive.present) {
      map['is_active'] = Variable<bool>(isActive.value);
    }
    if (hasApprovedContent.present) {
      map['has_approved_content'] = Variable<bool>(hasApprovedContent.value);
    }
    if (hasApprovedIT.present) {
      map['has_approved_i_t'] = Variable<bool>(hasApprovedIT.value);
    }
    if (isSelected.present) {
      map['is_selected'] = Variable<bool>(isSelected.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('QuestionCompanion(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected')
          ..write(')'))
        .toString();
  }
}

class $QuestionTable extends Question
    with TableInfo<$QuestionTable, QuestionData> {
  final GeneratedDatabase _db;
  final String? _alias;
  $QuestionTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _idLocalMeta = const VerificationMeta('idLocal');
  late final GeneratedColumn<int?> idLocal = GeneratedColumn<int?>(
      'id_local', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _orderIDMeta = const VerificationMeta('orderID');
  late final GeneratedColumn<int?> orderID = GeneratedColumn<int?>(
      'order_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _createDateMeta = const VerificationMeta('createDate');
  late final GeneratedColumn<DateTime?> createDate = GeneratedColumn<DateTime?>(
      'create_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _updateDateMeta = const VerificationMeta('updateDate');
  late final GeneratedColumn<DateTime?> updateDate = GeneratedColumn<DateTime?>(
      'update_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _createByMeta = const VerificationMeta('createBy');
  late final GeneratedColumn<int?> createBy = GeneratedColumn<int?>(
      'create_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _updateByMeta = const VerificationMeta('updateBy');
  late final GeneratedColumn<int?> updateBy = GeneratedColumn<int?>(
      'update_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, false,
      typeName: 'TEXT', requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _imagePathMeta = const VerificationMeta('imagePath');
  late final GeneratedColumn<String?> imagePath = GeneratedColumn<String?>(
      'image_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _isActiveMeta = const VerificationMeta('isActive');
  late final GeneratedColumn<bool?> isActive = GeneratedColumn<bool?>(
      'is_active', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_active IN (0, 1))',
      defaultValue: const Constant(true));
  final VerificationMeta _hasApprovedContentMeta =
      const VerificationMeta('hasApprovedContent');
  late final GeneratedColumn<bool?> hasApprovedContent = GeneratedColumn<bool?>(
      'has_approved_content', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_content IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _hasApprovedITMeta =
      const VerificationMeta('hasApprovedIT');
  late final GeneratedColumn<bool?> hasApprovedIT = GeneratedColumn<bool?>(
      'has_approved_i_t', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_i_t IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _isSelectedMeta = const VerificationMeta('isSelected');
  late final GeneratedColumn<bool?> isSelected = GeneratedColumn<bool?>(
      'is_selected', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_selected IN (0, 1))',
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        idLocal,
        orderID,
        createDate,
        updateDate,
        createBy,
        updateBy,
        title,
        description,
        imagePath,
        isActive,
        hasApprovedContent,
        hasApprovedIT,
        isSelected
      ];
  @override
  String get aliasedName => _alias ?? 'question';
  @override
  String get actualTableName => 'question';
  @override
  VerificationContext validateIntegrity(Insertable<QuestionData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('id_local')) {
      context.handle(_idLocalMeta,
          idLocal.isAcceptableOrUnknown(data['id_local']!, _idLocalMeta));
    }
    if (data.containsKey('order_i_d')) {
      context.handle(_orderIDMeta,
          orderID.isAcceptableOrUnknown(data['order_i_d']!, _orderIDMeta));
    }
    if (data.containsKey('create_date')) {
      context.handle(
          _createDateMeta,
          createDate.isAcceptableOrUnknown(
              data['create_date']!, _createDateMeta));
    }
    if (data.containsKey('update_date')) {
      context.handle(
          _updateDateMeta,
          updateDate.isAcceptableOrUnknown(
              data['update_date']!, _updateDateMeta));
    }
    if (data.containsKey('create_by')) {
      context.handle(_createByMeta,
          createBy.isAcceptableOrUnknown(data['create_by']!, _createByMeta));
    } else if (isInserting) {
      context.missing(_createByMeta);
    }
    if (data.containsKey('update_by')) {
      context.handle(_updateByMeta,
          updateBy.isAcceptableOrUnknown(data['update_by']!, _updateByMeta));
    } else if (isInserting) {
      context.missing(_updateByMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('image_path')) {
      context.handle(_imagePathMeta,
          imagePath.isAcceptableOrUnknown(data['image_path']!, _imagePathMeta));
    }
    if (data.containsKey('is_active')) {
      context.handle(_isActiveMeta,
          isActive.isAcceptableOrUnknown(data['is_active']!, _isActiveMeta));
    }
    if (data.containsKey('has_approved_content')) {
      context.handle(
          _hasApprovedContentMeta,
          hasApprovedContent.isAcceptableOrUnknown(
              data['has_approved_content']!, _hasApprovedContentMeta));
    }
    if (data.containsKey('has_approved_i_t')) {
      context.handle(
          _hasApprovedITMeta,
          hasApprovedIT.isAcceptableOrUnknown(
              data['has_approved_i_t']!, _hasApprovedITMeta));
    }
    if (data.containsKey('is_selected')) {
      context.handle(
          _isSelectedMeta,
          isSelected.isAcceptableOrUnknown(
              data['is_selected']!, _isSelectedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idLocal};
  @override
  QuestionData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return QuestionData.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $QuestionTable createAlias(String alias) {
    return $QuestionTable(_db, alias);
  }
}

class WordData extends DataClass implements Insertable<WordData> {
  final int id;
  final int idLocal;
  final int orderID;
  final DateTime createDate;
  final DateTime updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;
  final bool isActive;
  final bool hasApprovedContent;
  final bool hasApprovedIT;
  final bool isSelected;
  WordData(
      {required this.id,
      required this.idLocal,
      required this.orderID,
      required this.createDate,
      required this.updateDate,
      required this.createBy,
      required this.updateBy,
      required this.title,
      required this.description,
      required this.imagePath,
      required this.isActive,
      required this.hasApprovedContent,
      required this.hasApprovedIT,
      required this.isSelected});
  factory WordData.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return WordData(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      idLocal: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id_local'])!,
      orderID: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}order_i_d'])!,
      createDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_date'])!,
      updateDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_date'])!,
      createBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}create_by'])!,
      updateBy: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}update_by'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      imagePath: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image_path'])!,
      isActive: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_active'])!,
      hasApprovedContent: const BoolType().mapFromDatabaseResponse(
          data['${effectivePrefix}has_approved_content'])!,
      hasApprovedIT: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}has_approved_i_t'])!,
      isSelected: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_selected'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['id_local'] = Variable<int>(idLocal);
    map['order_i_d'] = Variable<int>(orderID);
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    map['create_by'] = Variable<int>(createBy);
    map['update_by'] = Variable<int>(updateBy);
    map['title'] = Variable<String>(title);
    map['description'] = Variable<String>(description);
    map['image_path'] = Variable<String>(imagePath);
    map['is_active'] = Variable<bool>(isActive);
    map['has_approved_content'] = Variable<bool>(hasApprovedContent);
    map['has_approved_i_t'] = Variable<bool>(hasApprovedIT);
    map['is_selected'] = Variable<bool>(isSelected);
    return map;
  }

  WordCompanion toCompanion(bool nullToAbsent) {
    return WordCompanion(
      id: Value(id),
      idLocal: Value(idLocal),
      orderID: Value(orderID),
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      createBy: Value(createBy),
      updateBy: Value(updateBy),
      title: Value(title),
      description: Value(description),
      imagePath: Value(imagePath),
      isActive: Value(isActive),
      hasApprovedContent: Value(hasApprovedContent),
      hasApprovedIT: Value(hasApprovedIT),
      isSelected: Value(isSelected),
    );
  }

  factory WordData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return WordData(
      id: serializer.fromJson<int>(json['id']),
      idLocal: serializer.fromJson<int>(json['idLocal']),
      orderID: serializer.fromJson<int>(json['orderID']),
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      createBy: serializer.fromJson<int>(json['createBy']),
      updateBy: serializer.fromJson<int>(json['updateBy']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String>(json['description']),
      imagePath: serializer.fromJson<String>(json['imagePath']),
      isActive: serializer.fromJson<bool>(json['isActive']),
      hasApprovedContent: serializer.fromJson<bool>(json['hasApprovedContent']),
      hasApprovedIT: serializer.fromJson<bool>(json['hasApprovedIT']),
      isSelected: serializer.fromJson<bool>(json['isSelected']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'idLocal': serializer.toJson<int>(idLocal),
      'orderID': serializer.toJson<int>(orderID),
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'createBy': serializer.toJson<int>(createBy),
      'updateBy': serializer.toJson<int>(updateBy),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String>(description),
      'imagePath': serializer.toJson<String>(imagePath),
      'isActive': serializer.toJson<bool>(isActive),
      'hasApprovedContent': serializer.toJson<bool>(hasApprovedContent),
      'hasApprovedIT': serializer.toJson<bool>(hasApprovedIT),
      'isSelected': serializer.toJson<bool>(isSelected),
    };
  }

  WordData copyWith(
          {int? id,
          int? idLocal,
          int? orderID,
          DateTime? createDate,
          DateTime? updateDate,
          int? createBy,
          int? updateBy,
          String? title,
          String? description,
          String? imagePath,
          bool? isActive,
          bool? hasApprovedContent,
          bool? hasApprovedIT,
          bool? isSelected}) =>
      WordData(
        id: id ?? this.id,
        idLocal: idLocal ?? this.idLocal,
        orderID: orderID ?? this.orderID,
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        createBy: createBy ?? this.createBy,
        updateBy: updateBy ?? this.updateBy,
        title: title ?? this.title,
        description: description ?? this.description,
        imagePath: imagePath ?? this.imagePath,
        isActive: isActive ?? this.isActive,
        hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
        hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
        isSelected: isSelected ?? this.isSelected,
      );
  @override
  String toString() {
    return (StringBuffer('WordData(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          idLocal.hashCode,
          $mrjc(
              orderID.hashCode,
              $mrjc(
                  createDate.hashCode,
                  $mrjc(
                      updateDate.hashCode,
                      $mrjc(
                          createBy.hashCode,
                          $mrjc(
                              updateBy.hashCode,
                              $mrjc(
                                  title.hashCode,
                                  $mrjc(
                                      description.hashCode,
                                      $mrjc(
                                          imagePath.hashCode,
                                          $mrjc(
                                              isActive.hashCode,
                                              $mrjc(
                                                  hasApprovedContent.hashCode,
                                                  $mrjc(
                                                      hasApprovedIT.hashCode,
                                                      isSelected
                                                          .hashCode))))))))))))));
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is WordData &&
          other.id == this.id &&
          other.idLocal == this.idLocal &&
          other.orderID == this.orderID &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.createBy == this.createBy &&
          other.updateBy == this.updateBy &&
          other.title == this.title &&
          other.description == this.description &&
          other.imagePath == this.imagePath &&
          other.isActive == this.isActive &&
          other.hasApprovedContent == this.hasApprovedContent &&
          other.hasApprovedIT == this.hasApprovedIT &&
          other.isSelected == this.isSelected);
}

class WordCompanion extends UpdateCompanion<WordData> {
  final Value<int> id;
  final Value<int> idLocal;
  final Value<int> orderID;
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<int> createBy;
  final Value<int> updateBy;
  final Value<String> title;
  final Value<String> description;
  final Value<String> imagePath;
  final Value<bool> isActive;
  final Value<bool> hasApprovedContent;
  final Value<bool> hasApprovedIT;
  final Value<bool> isSelected;
  const WordCompanion({
    this.id = const Value.absent(),
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.createBy = const Value.absent(),
    this.updateBy = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
  });
  WordCompanion.insert({
    required int id,
    this.idLocal = const Value.absent(),
    this.orderID = const Value.absent(),
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    required int createBy,
    required int updateBy,
    required String title,
    this.description = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.isActive = const Value.absent(),
    this.hasApprovedContent = const Value.absent(),
    this.hasApprovedIT = const Value.absent(),
    this.isSelected = const Value.absent(),
  })  : id = Value(id),
        createBy = Value(createBy),
        updateBy = Value(updateBy),
        title = Value(title);
  static Insertable<WordData> custom({
    Expression<int>? id,
    Expression<int>? idLocal,
    Expression<int>? orderID,
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<int>? createBy,
    Expression<int>? updateBy,
    Expression<String>? title,
    Expression<String>? description,
    Expression<String>? imagePath,
    Expression<bool>? isActive,
    Expression<bool>? hasApprovedContent,
    Expression<bool>? hasApprovedIT,
    Expression<bool>? isSelected,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (idLocal != null) 'id_local': idLocal,
      if (orderID != null) 'order_i_d': orderID,
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (createBy != null) 'create_by': createBy,
      if (updateBy != null) 'update_by': updateBy,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (imagePath != null) 'image_path': imagePath,
      if (isActive != null) 'is_active': isActive,
      if (hasApprovedContent != null)
        'has_approved_content': hasApprovedContent,
      if (hasApprovedIT != null) 'has_approved_i_t': hasApprovedIT,
      if (isSelected != null) 'is_selected': isSelected,
    });
  }

  WordCompanion copyWith(
      {Value<int>? id,
      Value<int>? idLocal,
      Value<int>? orderID,
      Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<int>? createBy,
      Value<int>? updateBy,
      Value<String>? title,
      Value<String>? description,
      Value<String>? imagePath,
      Value<bool>? isActive,
      Value<bool>? hasApprovedContent,
      Value<bool>? hasApprovedIT,
      Value<bool>? isSelected}) {
    return WordCompanion(
      id: id ?? this.id,
      idLocal: idLocal ?? this.idLocal,
      orderID: orderID ?? this.orderID,
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      createBy: createBy ?? this.createBy,
      updateBy: updateBy ?? this.updateBy,
      title: title ?? this.title,
      description: description ?? this.description,
      imagePath: imagePath ?? this.imagePath,
      isActive: isActive ?? this.isActive,
      hasApprovedContent: hasApprovedContent ?? this.hasApprovedContent,
      hasApprovedIT: hasApprovedIT ?? this.hasApprovedIT,
      isSelected: isSelected ?? this.isSelected,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (idLocal.present) {
      map['id_local'] = Variable<int>(idLocal.value);
    }
    if (orderID.present) {
      map['order_i_d'] = Variable<int>(orderID.value);
    }
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (createBy.present) {
      map['create_by'] = Variable<int>(createBy.value);
    }
    if (updateBy.present) {
      map['update_by'] = Variable<int>(updateBy.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (imagePath.present) {
      map['image_path'] = Variable<String>(imagePath.value);
    }
    if (isActive.present) {
      map['is_active'] = Variable<bool>(isActive.value);
    }
    if (hasApprovedContent.present) {
      map['has_approved_content'] = Variable<bool>(hasApprovedContent.value);
    }
    if (hasApprovedIT.present) {
      map['has_approved_i_t'] = Variable<bool>(hasApprovedIT.value);
    }
    if (isSelected.present) {
      map['is_selected'] = Variable<bool>(isSelected.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('WordCompanion(')
          ..write('id: $id, ')
          ..write('idLocal: $idLocal, ')
          ..write('orderID: $orderID, ')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('createBy: $createBy, ')
          ..write('updateBy: $updateBy, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('imagePath: $imagePath, ')
          ..write('isActive: $isActive, ')
          ..write('hasApprovedContent: $hasApprovedContent, ')
          ..write('hasApprovedIT: $hasApprovedIT, ')
          ..write('isSelected: $isSelected')
          ..write(')'))
        .toString();
  }
}

class $WordTable extends Word with TableInfo<$WordTable, WordData> {
  final GeneratedDatabase _db;
  final String? _alias;
  $WordTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _idLocalMeta = const VerificationMeta('idLocal');
  late final GeneratedColumn<int?> idLocal = GeneratedColumn<int?>(
      'id_local', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _orderIDMeta = const VerificationMeta('orderID');
  late final GeneratedColumn<int?> orderID = GeneratedColumn<int?>(
      'order_i_d', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _createDateMeta = const VerificationMeta('createDate');
  late final GeneratedColumn<DateTime?> createDate = GeneratedColumn<DateTime?>(
      'create_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _updateDateMeta = const VerificationMeta('updateDate');
  late final GeneratedColumn<DateTime?> updateDate = GeneratedColumn<DateTime?>(
      'update_date', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultValue: Constant(getNowDateTime()));
  final VerificationMeta _createByMeta = const VerificationMeta('createBy');
  late final GeneratedColumn<int?> createBy = GeneratedColumn<int?>(
      'create_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _updateByMeta = const VerificationMeta('updateBy');
  late final GeneratedColumn<int?> updateBy = GeneratedColumn<int?>(
      'update_by', aliasedName, false,
      typeName: 'INTEGER', requiredDuringInsert: true);
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, false,
      typeName: 'TEXT', requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _imagePathMeta = const VerificationMeta('imagePath');
  late final GeneratedColumn<String?> imagePath = GeneratedColumn<String?>(
      'image_path', aliasedName, false,
      typeName: 'TEXT',
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  final VerificationMeta _isActiveMeta = const VerificationMeta('isActive');
  late final GeneratedColumn<bool?> isActive = GeneratedColumn<bool?>(
      'is_active', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_active IN (0, 1))',
      defaultValue: const Constant(true));
  final VerificationMeta _hasApprovedContentMeta =
      const VerificationMeta('hasApprovedContent');
  late final GeneratedColumn<bool?> hasApprovedContent = GeneratedColumn<bool?>(
      'has_approved_content', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_content IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _hasApprovedITMeta =
      const VerificationMeta('hasApprovedIT');
  late final GeneratedColumn<bool?> hasApprovedIT = GeneratedColumn<bool?>(
      'has_approved_i_t', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (has_approved_i_t IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _isSelectedMeta = const VerificationMeta('isSelected');
  late final GeneratedColumn<bool?> isSelected = GeneratedColumn<bool?>(
      'is_selected', aliasedName, false,
      typeName: 'INTEGER',
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_selected IN (0, 1))',
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        idLocal,
        orderID,
        createDate,
        updateDate,
        createBy,
        updateBy,
        title,
        description,
        imagePath,
        isActive,
        hasApprovedContent,
        hasApprovedIT,
        isSelected
      ];
  @override
  String get aliasedName => _alias ?? 'word';
  @override
  String get actualTableName => 'word';
  @override
  VerificationContext validateIntegrity(Insertable<WordData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('id_local')) {
      context.handle(_idLocalMeta,
          idLocal.isAcceptableOrUnknown(data['id_local']!, _idLocalMeta));
    }
    if (data.containsKey('order_i_d')) {
      context.handle(_orderIDMeta,
          orderID.isAcceptableOrUnknown(data['order_i_d']!, _orderIDMeta));
    }
    if (data.containsKey('create_date')) {
      context.handle(
          _createDateMeta,
          createDate.isAcceptableOrUnknown(
              data['create_date']!, _createDateMeta));
    }
    if (data.containsKey('update_date')) {
      context.handle(
          _updateDateMeta,
          updateDate.isAcceptableOrUnknown(
              data['update_date']!, _updateDateMeta));
    }
    if (data.containsKey('create_by')) {
      context.handle(_createByMeta,
          createBy.isAcceptableOrUnknown(data['create_by']!, _createByMeta));
    } else if (isInserting) {
      context.missing(_createByMeta);
    }
    if (data.containsKey('update_by')) {
      context.handle(_updateByMeta,
          updateBy.isAcceptableOrUnknown(data['update_by']!, _updateByMeta));
    } else if (isInserting) {
      context.missing(_updateByMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('image_path')) {
      context.handle(_imagePathMeta,
          imagePath.isAcceptableOrUnknown(data['image_path']!, _imagePathMeta));
    }
    if (data.containsKey('is_active')) {
      context.handle(_isActiveMeta,
          isActive.isAcceptableOrUnknown(data['is_active']!, _isActiveMeta));
    }
    if (data.containsKey('has_approved_content')) {
      context.handle(
          _hasApprovedContentMeta,
          hasApprovedContent.isAcceptableOrUnknown(
              data['has_approved_content']!, _hasApprovedContentMeta));
    }
    if (data.containsKey('has_approved_i_t')) {
      context.handle(
          _hasApprovedITMeta,
          hasApprovedIT.isAcceptableOrUnknown(
              data['has_approved_i_t']!, _hasApprovedITMeta));
    }
    if (data.containsKey('is_selected')) {
      context.handle(
          _isSelectedMeta,
          isSelected.isAcceptableOrUnknown(
              data['is_selected']!, _isSelectedMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idLocal};
  @override
  WordData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return WordData.fromData(data, _db,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $WordTable createAlias(String alias) {
    return $WordTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  late final $LearningLangTable learningLang = $LearningLangTable(this);
  late final $CourseLevelTable courseLevel = $CourseLevelTable(this);
  late final $CourseTable course = $CourseTable(this);
  late final $LessonTable lesson = $LessonTable(this);
  late final $LessonFormTable lessonForm = $LessonFormTable(this);
  late final $FormTable form = $FormTable(this);
  late final $QuestionTable question = $QuestionTable(this);
  late final $WordTable word = $WordTable(this);
  late final LearningLangDao learningLangDao =
      LearningLangDao(this as AppDatabase);
  late final CourseDao courseDao = CourseDao(this as AppDatabase);
  late final WordDao wordDao = WordDao(this as AppDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        learningLang,
        courseLevel,
        course,
        lesson,
        lessonForm,
        form,
        question,
        word
      ];
}
