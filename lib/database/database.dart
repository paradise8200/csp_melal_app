import 'dart:io';
import 'package:flutter_language_app/database/daos/crs/course_dao.dart';
import 'package:flutter_language_app/database/tables/bas/learning_lang_table.dart';
import 'package:flutter_language_app/database/tables/cor/course_level_table.dart';
import 'package:flutter_language_app/database/tables/cor/course_table.dart';
import 'package:flutter_language_app/database/tables/lsn/lesson_table.dart';
import 'package:flutter_language_app/database/tables/lsn/part_lesson_table.dart';
import 'package:flutter_language_app/database/tables/lsn/part_table.dart';
import 'package:flutter_language_app/database/tables/qus/question_table.dart';
import 'package:flutter_language_app/database/tables/qus/word_table.dart';
//import 'package:moor/ffi.dart';
import 'package:moor/moor.dart';
import 'package:moor/moor_web.dart';

import 'daos/bas/learning_lang_dao.dart';
import 'package:flutter_language_app/utilities/date_time_controller.dart';

import 'package:path_provider/path_provider.dart' as paths;
import 'package:path/path.dart' as p;
import 'package:flutter/foundation.dart';

import 'daos/qus/word_dao.dart';

part 'database.g.dart';

/*
LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    final dbFolder = await paths.getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'cspMelal.sqlite'));
    return VmDatabase(file);
  });
}
*/


@UseMoor(tables: [
  LearningLang,
  CourseLevel,
  Course,
  Lesson,
  LessonForm,
  Form,
  Question,
  Word
], daos: [
  LearningLangDao,CourseDao,WordDao
  // BaseDao
])
class AppDatabase extends _$AppDatabase {
 //AppDatabase() : super(_openConnection());
 AppDatabase() : super(WebDatabase('app'));
  int get schemaVersion => 1;

}
