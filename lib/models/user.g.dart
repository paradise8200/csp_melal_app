// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    name: json['name'] as String,
    phone: json['phone'] as String,
    imageAvatar: json['imageAvatar'] as String,
  )
    ..id = json['id'] as int
    ..order = json['order'] as int
    ..createDate = json['createDate'] as String;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'order': instance.order,
      'createDate': instance.createDate,
      'name': instance.name,
      'phone': instance.phone,
      'imageAvatar': instance.imageAvatar,
    };
