
class ChoicePicModel {
  late final String name;
  late final String image;
  late bool accepting;
  late bool selected;

  ChoicePicModel(
      {required this.name, required this.image, this.accepting = false,this.selected = false});
}
