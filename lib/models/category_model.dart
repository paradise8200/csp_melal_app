import 'package:flutter/material.dart';

class CategoryModel{
  String backgroundImage;
  Color shadowColor;
  String title;
  String image;

  CategoryModel(this.backgroundImage,this.shadowColor,this.title,this.image);
}