import 'package:flutter_language_app/entities/bas/base_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable(includeIfNull: true)


class User extends BaseEntity {
  late String name;
  late String phone;
  late String imageAvatar;

  User({ this.name : "", this.phone: "", this.imageAvatar: ""}) : super(0, 0, '');


  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}

