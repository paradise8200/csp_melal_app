import 'dart:core';

import 'package:flutter_language_app/entities/bas/base_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'banner.g.dart';

@JsonSerializable()
class Banner extends BaseEntity {
  final int linkType;

  final int tableType;
  final int parentId;

  final String url;
  final String name;

  final int fileType;
  final String fileUrl;

  Banner(
      {required this.fileUrl,
      required this.fileType,
      required this.linkType,
      required this.parentId,
      required this.name,
      required this.tableType,
      required this.url,
      id,
      order,
      createDate})
      : super(id, order, createDate);

  factory Banner.fromJson(Map<String, dynamic> json) => _$BannerFromJson(json);

  Map<String, dynamic> toJson() => _$BannerToJson(this);
}
