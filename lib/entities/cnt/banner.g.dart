// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Banner _$BannerFromJson(Map<String, dynamic> json) {
  return Banner(
    fileUrl: json['fileUrl'] as String,
    fileType: json['fileType'] as int,
    linkType: json['linkType'] as int,
    parentId: json['parentId'] as int,
    name: json['name'] as String,
    tableType: json['tableType'] as int,
    url: json['url'] as String,
    id: json['id'],
    order: json['order'],
    createDate: json['createDate'],
  );
}

Map<String, dynamic> _$BannerToJson(Banner instance) => <String, dynamic>{
      'id': instance.id,
      'order': instance.order,
      'createDate': instance.createDate,
      'linkType': instance.linkType,
      'tableType': instance.tableType,
      'parentId': instance.parentId,
      'url': instance.url,
      'name': instance.name,
      'fileType': instance.fileType,
      'fileUrl': instance.fileUrl,
    };
