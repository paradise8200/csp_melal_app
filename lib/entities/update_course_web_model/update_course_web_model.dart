

import 'package:flutter_language_app/entities/safe_convert.dart';

class UpdateCourseRQM {
  final int ID;
  final String Title;
  final String Description;
  final String ImagePath;
  final int OrderID;
  final bool IsActive;
  final String IconPath;
  final String VideoURL;
  final int LanguageID;

  UpdateCourseRQM({
    this.ID = 0,
    this.Title = "",
    this.Description = "",
    this.ImagePath = "",
    this.OrderID = 0,
    this.IsActive = false,
    this.IconPath = "",
    this.VideoURL = "",
    this.LanguageID = 0,
  });

  factory UpdateCourseRQM.fromJson(Map<String, dynamic> json) => UpdateCourseRQM(
    ID: asT<int>(json, 'ID',defaultValue: 0),
    Title: asT<String>(json, 'Title',defaultValue: ''),
    Description: asT<String>(json, 'Description',defaultValue: ''),
    ImagePath: asT<String>(json, 'ImagePath',defaultValue: ''),
    OrderID: asT<int>(json, 'OrderID',defaultValue: 0),
    IsActive: asT<bool>(json, 'IsActive',defaultValue: false),
    IconPath: asT<String>(json, 'IconPath',defaultValue: ''),
    VideoURL: asT<String>(json, 'VideoURL',defaultValue: ''),
    LanguageID: asT<int>(json, 'LanguageID',defaultValue: 0),
  );

  Map<String, dynamic> toJson() => {
    'ID': this.ID,
    'Title': this.Title,
    'Description': this.Description,
    'ImagePath': this.ImagePath,
    'OrderID': this.OrderID,
    'IsActive': this.IsActive,
    'IconPath': this.IconPath,
    'VideoURL': this.VideoURL,
    'LanguageID': this.LanguageID,
  };
}

