

import 'package:flutter_language_app/entities/safe_convert.dart';

class SignInRQM {
  final String Username;
  final String Password;
  final bool RememberMe;

  SignInRQM({
    this.Username = "",
    this.Password = "",
    this.RememberMe = false,
  });

  factory SignInRQM.fromJson(Map<String, dynamic> json) => SignInRQM(
    Username: asT<String>(json, 'Username',defaultValue: ''),
    Password: asT<String>(json, 'Password',defaultValue: ''),
    RememberMe: asT<bool>(json, 'RememberMe',defaultValue:false),
  );

  Map<String, dynamic> toJson() => {
    'Username': this.Username,
    'Password': this.Password,
    'RememberMe': this.RememberMe,
  };
}

