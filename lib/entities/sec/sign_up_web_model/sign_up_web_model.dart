import 'package:flutter_language_app/entities/safe_convert.dart';

class SignUpRQM {
  final int ID;
  final String Email;
  final String PhoneNumber;
  final String Firstname;
  final String Lastname;
  final String Password;

  SignUpRQM({
    this.ID = 0,
    this.Email = "",
    this.PhoneNumber = "",
    this.Firstname = "",
    this.Lastname = "",
    this.Password = "",
  });

  factory SignUpRQM.fromJson(Map<String, dynamic> json) => SignUpRQM(
    ID: asT<int>(json, 'ID', defaultValue: 0),
    Email: asT<String>(json, 'Email',defaultValue: ''),
    PhoneNumber: asT<String>(json, 'PhoneNumber',defaultValue: ''),
    Firstname: asT<String>(json, 'Firstname',defaultValue: ''),
    Lastname: asT<String>(json, 'Lastname',defaultValue: ''),
    Password: asT<String>(json, 'Password',defaultValue: ''),
  );

  Map<String, dynamic> toJson() => {
    'ID': this.ID,
    'Email': this.Email,
    'PhoneNumber': this.PhoneNumber,
    'Firstname': this.Firstname,
    'Lastname': this.Lastname,
    'Password': this.Password,
  };
}

