import 'package:json_annotation/json_annotation.dart';

part 'base_rwm.g.dart';

//https://github.com/google/json_serializable.dart/tree/master/example/lib
//flutter packages pub run build_runner build
//flutter packages pub run build_runner build --delete-conflicting-outputs

@JsonSerializable(genericArgumentFactories: true)
class JsonResult<T extends BaseResponse> {
  int code;
  T data;
  String massage;
  dynamic error;

  JsonResult(this.code, this.data, this.error, this.massage);

  factory JsonResult.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$JsonResultFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(T Function(Object json) toJsonT) =>
      _$JsonResultToJson(this, toJsonT);
}

@JsonSerializable(genericArgumentFactories: true)
class LazyResult<R extends BaseResponse> {
  List<R> rows;
  int count;
  int pageIndex;
  int pageSize;
  String pageOrder;

  LazyResult(this.rows, this.count, this.pageIndex, this.pageSize, this.pageOrder);

  factory LazyResult.fromJson(
          Map<String, dynamic> json, R Function(Object? json) fromJsonR) =>
      _$LazyResultFromJson(json, fromJsonR);

  Map<String, dynamic> toJson(R Function(Object json) toJsonR) =>
      _$LazyResultToJson(this, toJsonR);
}

class BaseResponse {}

class BaseRequest {}
