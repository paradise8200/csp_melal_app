import 'package:json_annotation/json_annotation.dart';

part 'lazy_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class LazyModel {
  int categoryId;
  int pageIndex;
  int pageSize;
  String order;
  String orderby;
  String searchText;

  bool onlyInstock;
  bool sale;
  int maxPrice;
  int minPrice;

  LazyModel(this.pageIndex, this.categoryId, this.order, this.orderby,
      this.pageSize, this.searchText,
      {this.sale: false, this.onlyInstock: false, this.minPrice: 0, this.maxPrice: 1000000});

  factory LazyModel.fromJson(Map<String, dynamic> json) =>
      _$LazyModelFromJson(json);

  Map<String, dynamic> toJson() => _$LazyModelToJson(this);
}
