// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lazy_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LazyModel _$LazyModelFromJson(Map<String, dynamic> json) {
  return LazyModel(
    json['pageIndex'] as int,
    json['categoryId'] as int,
    json['order'] as String,
    json['orderby'] as String,
    json['pageSize'] as int,
    json['searchText'] as String,
    sale: json['sale'] as bool,
    onlyInstock: json['onlyInstock'] as bool,
    minPrice: json['minPrice'] as int,
    maxPrice: json['maxPrice'] as int,
  );
}

Map<String, dynamic> _$LazyModelToJson(LazyModel instance) => <String, dynamic>{
      'categoryId': instance.categoryId,
      'pageIndex': instance.pageIndex,
      'pageSize': instance.pageSize,
      'order': instance.order,
      'orderby': instance.orderby,
      'searchText': instance.searchText,
      'onlyInstock': instance.onlyInstock,
      'sale': instance.sale,
      'maxPrice': instance.maxPrice,
      'minPrice': instance.minPrice,
    };
