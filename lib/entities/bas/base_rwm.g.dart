// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_rwm.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonResult<T> _$JsonResultFromJson<T extends BaseResponse>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) {
  return JsonResult<T>(
    json['code'] as int,
    fromJsonT(json['data']),
    json['error'],
    json['massage'] as String,
  );
}

Map<String, dynamic> _$JsonResultToJson<T extends BaseResponse>(
  JsonResult<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'code': instance.code,
      'data': toJsonT(instance.data),
      'massage': instance.massage,
      'error': instance.error,
    };

LazyResult<R> _$LazyResultFromJson<R extends BaseResponse>(
  Map<String, dynamic> json,
  R Function(Object? json) fromJsonR,
) {
  return LazyResult<R>(
    (json['rows'] as List<dynamic>).map(fromJsonR).toList(),
    json['count'] as int,
    json['pageIndex'] as int,
    json['pageSize'] as int,
    json['pageOrder'] as String,
  );
}

Map<String, dynamic> _$LazyResultToJson<R extends BaseResponse>(
  LazyResult<R> instance,
  Object? Function(R value) toJsonR,
) =>
    <String, dynamic>{
      'rows': instance.rows.map(toJsonR).toList(),
      'count': instance.count,
      'pageIndex': instance.pageIndex,
      'pageSize': instance.pageSize,
      'pageOrder': instance.pageOrder,
    };
