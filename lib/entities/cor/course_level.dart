

import 'package:flutter_language_app/entities/safe_convert.dart';
class CourseLevel {
  final int ID;
  final int courseID;
  final int LevelID;


  CourseLevel({
    this.ID = 0,
    this.courseID = 0,
    this.LevelID = 0,

  });
  factory CourseLevel.fromJson(Map<String, dynamic> json) => CourseLevel(
    ID: asT<int>(json, 'ID',defaultValue: 0),
    courseID: asT<int>(json, 'courseID',defaultValue: 0),
    LevelID: asT<int>(json, 'LevelID',defaultValue: 0),

  );
  Map<String, dynamic> toJson() => {
    'ID': this.ID,
    'courseID  ': this.courseID  ,
    'LevelID  ': this.LevelID  ,
  };
}

