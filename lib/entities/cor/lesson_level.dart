

import 'package:flutter_language_app/entities/safe_convert.dart';
class LessonLevel {
  final int ID;
  final int CourseLevelID;
  final int LessosnID;


  LessonLevel({
    this.ID = 0,
    this.CourseLevelID = 0,
    this.LessosnID = 0,

  });
  factory LessonLevel.fromJson(Map<String, dynamic> json) => LessonLevel(
    ID: asT<int>(json, 'ID',defaultValue: 0),
    CourseLevelID: asT<int>(json, 'CourseLevelID',defaultValue: 0),
    LessosnID: asT<int>(json, 'LessosnID',defaultValue: 0),

  );
  Map<String, dynamic> toJson() => {
    'ID': this.ID,
    'CourseLevelID  ': this.CourseLevelID  ,
    'LessosnID  ': this.LessosnID  ,
  };
}

