

import 'package:flutter_language_app/entities/safe_convert.dart';


class Course {
  final int ID;
  final int orderID;
  final String createDate;
  final String updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;
  final bool isActive ;
  final String iconPath ;
  final String videoURL  ;
  final int languageID  ;

  Course({
    this.ID = 0,
    this.orderID = 0,
    this.createDate = "",
    this.updateDate = "",
    this.createBy = 0,
    this.updateBy = 0,
    this.title = "",
    this.description = "",
    this.imagePath = "",
    this.isActive  = false,
    this.iconPath  = "",
    this.videoURL   = "",
    this.languageID   = 0,
  });

  factory Course.fromJson(Map<String, dynamic> json) => Course(
    ID: asT<int>(json, 'ID',defaultValue: 0),
    orderID: asT<int>(json, 'orderID', defaultValue: 0),
    createDate: asT<String>(json, 'createDate',defaultValue: ''),
    updateDate: asT<String>(json, 'updateDate',defaultValue: ''),
    createBy: asT<int>(json, 'createBy',defaultValue: 0),
    updateBy: asT<int>(json, 'updateBy',defaultValue: 0),
    title: asT<String>(json, 'title',defaultValue: ''),
    description: asT<String>(json, 'description',defaultValue: ''),
    imagePath: asT<String>(json, 'imagePath',defaultValue: ''),
    isActive : asT<bool>(json, 'isActive ',defaultValue: false),
    iconPath : asT<String>(json, 'iconPath ',defaultValue: ''),
    videoURL  : asT<String>(json, 'videoURL  ',defaultValue: ''),
    languageID  : asT<int>(json, 'languageID  ',defaultValue: 0),
  );

  Map<String, dynamic> toJson() => {
    'ID': this.ID,
    'orderID': this.orderID,
    'createDate': this.createDate,
    'updateDate': this.updateDate,
    'createBy': this.createBy,
    'updateBy': this.updateBy,
    'title': this.title,
    'description': this.description,
    'imagePath': this.imagePath,
    'isActive ': this.isActive ,
    'iconPath ': this.iconPath ,
    'videoURL  ': this.videoURL  ,
    'languageID  ': this.languageID  ,
  };
}

