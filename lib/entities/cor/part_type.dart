

import 'package:flutter_language_app/entities/safe_convert.dart';
class PartType {
  final int ID;
  final String title;



  PartType({
    this.ID = 0,
    this.title = '',



  });
  factory PartType.fromJson(Map<String, dynamic> json) => PartType(
    ID: asT<int>(json, 'ID',defaultValue: 0),
    title: asT<String>(json, 'title',defaultValue: ''),



  );
  Map<String, dynamic> toJson() => {
    'ID': this.ID,
    'title  ': this.title  ,


  };
}

