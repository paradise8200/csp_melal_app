

import 'package:flutter_language_app/entities/safe_convert.dart';
class PartSession {
  final int ID;
  final int partID;

  final int lessonID;


  PartSession({
    this.ID = 0,
    this.partID = 0,
    this.lessonID = 0,


  });
  factory PartSession.fromJson(Map<String, dynamic> json) => PartSession(
    ID: asT<int>(json, 'ID',defaultValue: 0),
    partID: asT<int>(json, 'partID',defaultValue: 0),
    lessonID: asT<int>(json, 'lessonID',defaultValue: 0),


  );
  Map<String, dynamic> toJson() => {
    'ID': this.ID,
    'partID': this.partID,
    'lessonID  ': this.lessonID  ,

  };
}

