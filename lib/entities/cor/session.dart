

import 'package:flutter_language_app/entities/safe_convert.dart';
class Session {
  final int ID;
  final String title;
  final String description;
  final int lessonID;


  Session({
    this.ID = 0,
    this.title = '',
    this.description = '',
    this.lessonID = 0,


  });
  factory Session.fromJson(Map<String, dynamic> json) => Session(
    ID: asT<int>(json, 'ID',defaultValue: 0),
    title: asT<String>(json, 'title',defaultValue: ''),
    description: asT<String>(json, 'description',defaultValue: ''),
    lessonID: asT<int>(json, 'lessonID',defaultValue: 0),


  );
  Map<String, dynamic> toJson() => {
    'ID': this.ID,
    'title  ': this.title  ,
    'description  ': this.description  ,
    'lessonID  ': this.lessonID  ,

  };
}

