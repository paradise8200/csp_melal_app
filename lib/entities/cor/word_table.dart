

import 'package:flutter_language_app/entities/safe_convert.dart';


class WordTable {
  final int ID;
  final int orderID;
  final String createDate;
  final String updateDate;
  final int createBy;
  final int updateBy;
  final String title;
  final String description;
  final String imagePath;


  WordTable({
    this.ID = 0,
    this.orderID = 0,
    this.createDate = "",
    this.updateDate = "",
    this.createBy = 0,
    this.updateBy = 0,
    this.title = "",
    this.description = "",
    this.imagePath = "",

  });

  factory WordTable.fromJson(Map<String, dynamic> json) => WordTable(
    ID: asT<int>(json, 'ID',defaultValue: 0),
    orderID: asT<int>(json, 'orderID', defaultValue: 0),
    createDate: asT<String>(json, 'createDate',defaultValue: ''),
    updateDate: asT<String>(json, 'updateDate',defaultValue: ''),
    createBy: asT<int>(json, 'createBy',defaultValue: 0),
    updateBy: asT<int>(json, 'updateBy',defaultValue: 0),
    title: asT<String>(json, 'title',defaultValue: ''),
    description: asT<String>(json, 'description',defaultValue: ''),
    imagePath: asT<String>(json, 'imagePath',defaultValue: ''),

  );

  Map<String, dynamic> toJson() => {
    'ID': this.ID,
    'orderID': this.orderID,
    'createDate': this.createDate,
    'updateDate': this.updateDate,
    'createBy': this.createBy,
    'updateBy': this.updateBy,
    'title': this.title,
    'description': this.description,
    'imagePath': this.imagePath,

  };
}

