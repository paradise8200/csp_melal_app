import 'package:flutter/cupertino.dart';
import 'package:flutter_language_app/fakeData.dart';
import 'package:flutter_language_app/models/choice_pic_quiz_model.dart';
import 'package:flutter_language_app/models/matching_quiz_model.dart';

class QuizRepository {

  static List<MatchingQuizModel> getMatchingQuiz() {
    return matchingQuiz();
  }

  static List<ChoicePicModel> getChoicePicQuiz() {
    return choicePicQuiz();
  }
  static List<WordModel> getAnswerWord() {
    return wordModel();
  }


}
