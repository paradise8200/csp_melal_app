// import 'package:audioplayers/audio_cache.dart';
// import 'package:audioplayers/audioplayers.dart';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/fakeData.dart';
import 'package:flutter_language_app/models/MultiQuestion.dart';
import 'package:flutter_language_app/theme/dimens.dart';
import 'package:flutter_language_app/theme/text_widgets.dart';
import 'package:flutter_language_app/widgets/action_widgets.dart';
import 'package:flutter_language_app/widgets/image_widget.dart';
import 'package:flutter_language_app/widgets/progress_button.dart';
import 'package:flutter_svg/svg.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

class SelectWordPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SelectWordPageState();
}

class SelectWordPageState extends State<SelectWordPage> {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  int nbSteps = 4;

  int selectedStep = 0;
  // Future<AudioPlayer> playWin() async {
  //   AudioCache cache = new AudioCache();
  //
  //   return await cache.play("win.mp3");
  // }

  // Future<AudioPlayer> playLoss() async {
  //   AudioCache cache = new AudioCache();
  //
  //   return await cache.play("wrong.mp3");
  // }

  @override
  Widget build(BuildContext context) {

    var theme=Theme.of(context);
    MultiQuestion question = multiQuestion();
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(

        appBar: AppBar(


          elevation: 0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(standardSize(context)),
                  topRight: Radius.circular(standardSize(context)))),
          backgroundColor: theme.backgroundColor,
          centerTitle: true,
          title: Text(
            "لغات",
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Color(0xff474747),
                ),
          ),
          // actions: [
          //   IconButton(
          //     splashRadius: standardSize(context) / 1.2,
          //     splashColor: Colors.grey.shade300,
          //     icon: Icon(
          //       CupertinoIcons.clear,
          //       color: Color(0xff474747),
          //       size: mediumSize(context),
          //     ),
          //     onPressed: () {
          //       Navigator.pop(context);
          //     },
          //   ),
          // ],
          leading:
              // selectedStep == 0 ? SizedBox() :
              Container(
                margin: EdgeInsets.only(right: mediumSize(context)),
                child: backIcon(context),
              ),
        ),
        key: key,
        body: Stack(
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: standardSize(context),
                  vertical: xxSmallSize(context)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Container(
                    margin: EdgeInsets.only(bottom: xxSmallSize(context)),
                    child: StepProgressIndicator(
                      totalSteps: nbSteps,
                      currentStep: 3,
                      unselectedColor: Color(0xffEAEAEA),
                      roundedEdges: Radius.circular(smallSize(context)),
                      selectedGradientColor: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          Color(0xff3428ea),
                          Color(0xffa573ff),
                        ],
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.symmetric(vertical: mediumSize(context)),
                    child: Text(
                      "کدام کلمه به معنای دختر است ؟" ,
                      textAlign: TextAlign.center,
                      style: theme.textTheme.bodyText1!.copyWith(
                          color: theme.accentColor,
                          fontSize: standardSize(context) / 1.3),
                    ),
                  ),
                  GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: question.words.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                    itemBuilder: (context, index) => QuestionCard(question, index),
                  ),

                ],
              ),
            ),
            Container(
                margin: EdgeInsets.all(standardSize(context)),
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: progressButton(context, "ادامه", false,
                            ()
                               async {
                                if (question.selectedAnswer == question.answerCorrect) {
                                  // await playWin();

                                  // widget.controller.animateToPage(
                                  //   widget.controller.page!.toInt() + 1,
                                  //   duration: Duration(milliseconds: 200),
                                  //   curve: Curves.easeInQuad,
                                  // );

                                  print("dorost");
                                } else {
                                  // await playLoss();
                                  setState(() {
                                    question.selectedAnswer = "";
                                  });
                                  key.currentState!.showSnackBar(SnackBar(
                                      elevation: 3,
                                      margin: EdgeInsets.only(
                                          bottom: xlargeSize(context),
                                          right: standardSize(context),
                                          left: standardSize(context)),
                                      behavior: SnackBarBehavior.floating,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(12)),
                                      backgroundColor: Colors.redAccent,
                                      content: Text(
                                        "اشتباه گفتی ! دوباره امتحان کن!",
                                        textAlign: TextAlign.right,
                                        textDirection: TextDirection.rtl,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1!
                                            .copyWith(color: Colors.white),
                                      )));
                                  print("ghalat");
                                }
                              },


    )))

          ],
        ),
      ),
    );
  }
}

class QuestionCard extends StatefulWidget {
  final MultiQuestion _multiQuestion;
  int index;

  QuestionCard(this._multiQuestion, this.index);

  @override
  State<StatefulWidget> createState() => QuestionCardState();
}

class QuestionCardState extends State<QuestionCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(smallSize(context)),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(smallSize(context)),
          color: Color(0xfff3f4f9),
        border:widget._multiQuestion.isSelected==true? GradientBorder.uniform(
            width: 2,
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.0, 1.0],
            colors: [
              Color(0xff3428ea),
              Color(0xffa573ff),
            ],
          ),

          ):
       Border.all(color: Colors.transparent)
    ),
      child: Material(

        color: Colors.transparent,
        child: Ink(

          decoration: BoxDecoration(borderRadius: BorderRadius.circular(mediumSize(context))),
          child: InkWell(

            splashColor: Colors.grey.shade200,
            borderRadius: BorderRadius.circular(mediumSize(context)),

            onTap: () {
              setState(() {
                widget._multiQuestion.selectedAnswer =
                widget._multiQuestion.words[widget.index];
                widget._multiQuestion.isSelected = false;
              });
            },
            child: Column(
              children: [
                Container(
                  height: 80,
                  width: 80,
                  margin: EdgeInsets.only(top: smallSize(context)),
                  child: imageWidget(
                    widget._multiQuestion.images[widget.index],
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                    margin: EdgeInsets.only(top: smallSize(context)),
                    child: headline3(
                        context, widget._multiQuestion.words[widget.index]))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
class GradientBorder extends Border {
  final Gradient borderGradient;
  final double width;

  const GradientBorder({this.width = 0.0, required this.borderGradient})
      : assert(borderGradient != null),
        super();

  @override
  void paint(Canvas canvas, Rect rect,
      {TextDirection? textDirection,
        BoxShape shape = BoxShape.rectangle,
      BorderRadius? borderRadius}) {
    if (isUniform) {
      switch (shape) {
        case BoxShape.circle:
          assert(borderRadius == null,
          'A borderRadius can only be given for rectangular boxes.');
          this._paintGradientBorderWithCircle(canvas, rect);
          break;
        case BoxShape.rectangle:
          if (borderRadius != null) {
            this._paintGradientBorderWithRadius(canvas, rect, borderRadius);
            return;
          }
          this._paintGradientBorderWithRectangle(canvas, rect);
          break;
      }
      return;
    }
  }

  void _paintGradientBorderWithRadius(
      Canvas canvas, Rect rect, BorderRadius borderRadius) {
    final Paint paint = Paint();
    final RRect outer = borderRadius.toRRect(rect);

    paint.shader = borderGradient.createShader(outer.outerRect);

    if (width == 0.0) {
      paint
        ..style = PaintingStyle.stroke
        ..strokeWidth = 0.0;
      canvas.drawRRect(outer, paint);
    } else {
      final RRect inner = outer.deflate(width);
      canvas.drawDRRect(outer, inner, paint);
    }
  }

  void _paintGradientBorderWithCircle(Canvas canvas, Rect rect) {
    final double radius = (rect.shortestSide - width) / 2.0;
    final Paint paint = Paint();
    paint
      ..strokeWidth = width
      ..style = PaintingStyle.stroke
      ..shader = borderGradient
          .createShader(Rect.fromCircle(center: rect.center, radius: radius));

    canvas.drawCircle(rect.center, radius, paint);
  }

  void _paintGradientBorderWithRectangle(Canvas canvas, Rect rect) {
    final Paint paint = Paint();
    paint
      ..strokeWidth = width
      ..shader = borderGradient.createShader(rect)
      ..style = PaintingStyle.stroke;

    canvas.drawRect(rect.deflate(width / 2.0), paint);
  }

  factory GradientBorder.uniform({
    Gradient gradient = const LinearGradient(colors: [Colors.red]),
    double width = 4.0,
  }) {
    return GradientBorder._fromUniform(gradient, width);
  }

  const GradientBorder._fromUniform(Gradient gradient, double width)
      : assert(gradient != null),
        assert(width >= 0.0),
        borderGradient = gradient,
        width = width;
}
