import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget imageWidget(String image, {BoxFit fit: BoxFit.cover}) {
  return Image.network(
    image,
    fit: fit,
    // loadingBuilder: (context, child, loadingProgress) => Container(
    //     child: Center(
    //       child: CupertinoActivityIndicator(),
    //     ),
    //   ),
    // errorBuilder: (context, error, stackTrace) => Center(
    //   child: Container(
    //     child: Icon(Icons.broken_image),
    //   ),
    // ),
  );

  // return CachedNetworkImage(
  //   fit: fit,
  //   fadeInCurve: Curves.easeInCubic,
  //   fadeInDuration: Duration(milliseconds: 200),
  //   imageUrl: image,
  //   placeholder: (context, url) => Container(
  //     child: Center(
  //       child: CupertinoActivityIndicator(),
  //     ),
  //   ),
  //
  //   errorWidget: (context, url, error) => Center(
  //       child: Container(
  //     child: Icon(Icons.broken_image),
  //   )),
  // );
}
