// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_language_app/theme/colors.dart';
// import 'package:flutter_language_app/theme/dimens.dart';
//
// Widget progressButton(BuildContext context, String text, bool isProgress,
//     VoidCallback onClickAction,
//     {Color color: AppColors.primaryColor}) {
//   return ElevatedButton(
//     onPressed: onClickAction,
//     style: ElevatedButton.styleFrom(
//         primary: color,
//         padding: EdgeInsets.all(smallSize(context)),
//         shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(standardSize(context)))),
//     child: isProgress
//         ? CupertinoActivityIndicator()
//         : Text(
//             text,
//             style: TextStyle(
//                 fontSize: fullWidth(context) > 600
//                     ? fullWidth(context) / 33
//                     : fullWidth(context) / 23,
//                 color: Colors.white,
//                 fontWeight: FontWeight.w600),
//           ),
//   );
// }
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_language_app/theme/dimens.dart';

Widget progressButton(
    BuildContext context, String text, bool isProgress, VoidCallback onClickAction,
    {Color? color}) {
  return Directionality(
    textDirection: TextDirection.rtl,
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(mediumSize(context)),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0.0, 1.0],
          colors: [
            Color(0xff3428ea),
            Color(0xffa573ff),
          ],
        ),
      ),
      width: fullWidth(context),
      child: ElevatedButton(
        onPressed: isProgress ? null : onClickAction,
        style: ElevatedButton.styleFrom(
          elevation: 0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(mediumSize(context))),
          primary: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical:  mediumSize(context))
        ),
        child: isProgress
            ? CupertinoActivityIndicator(animating: true,)
            : Text(
          text,
          style: TextStyle(
              fontSize: fullWidth(context) > 600
                  ? fullWidth(context) / 35
                  : fullWidth(context) / 25,
              color: Colors.white,
              fontWeight: FontWeight.w600),
        ),
      ),
    ),
  );
}

